﻿using DG.Tweening;
using HighlightPlus;
using Pvr_UnitySDKAPI;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameObjectIns : MonoBehaviour
{

    #region 变量

    public static GameObjectIns Instance;


    #region GameObject
    

    [HideInInspector]
    public GameObject arrowRotation_CDG;
    [HideInInspector]
    public GameObject arrowRotation_R;
    [HideInInspector]
    public GameObject arrowRotation_L;
    
    /// <summary>
    /// 充电枪紧急停止按钮 左
    /// </summary>
    [HideInInspector]
    public GameObject CDQ_JJTZ_L_arrow;
    /// <summary>
    /// 充电枪紧急停止按钮 右
    /// </summary>
    [HideInInspector]
    public GameObject CDQ_JJTZ_R_arrow;
    /// <summary>
    /// 悬臂左紧急停止
    /// </summary>
    [HideInInspector]
    public GameObject XuanBi_OringeBuL;
    /// <summary>
    /// 悬臂右紧急停止
    /// </summary>
    [HideInInspector]
    public GameObject XuanBi_OringeBuR;
    /// <summary>
    /// 左手射线
    /// </summary>
    ///
    [HideInInspector]
    public GameObject rayParent_L;
    [HideInInspector]
    public GameObject L_Ray;
    [HideInInspector]
    public GameObject L_StartRayPos;
    [HideInInspector]
    public GameObject L_Sphere;
    [HideInInspector]
    public GameObject CDGJJTZBtn;
    /// <summary>
    /// 右手射线
    /// </summary>
    /// 
    [HideInInspector]
    public GameObject rayParent_R;
    [HideInInspector]
    public GameObject R_Ray;
    [HideInInspector]
    public GameObject R_StartRayPos;
    [HideInInspector]
    public GameObject R_Sphere;
    /// <summary>
    /// 通讯超时中止图片
    /// </summary>
    [HideInInspector]
    public GameObject RechargeAbnormalTermination;
    /// <summary>
    /// 绳子
    /// </summary>
    /// 
    [HideInInspector]
    public GameObject Obi_Gameobject;
    /// <summary>
    /// 充电枪B充电线
    /// </summary>
    /// 
    [HideInInspector]
    public GameObject XuanBi_Wire_R;
    /// <summary>
    /// 充电枪A充电线
    /// </summary>
    /// 
    [HideInInspector]
    public GameObject XuanBi_Wire_L;
    /// <summary>
    /// 车载监控
    /// </summary>
    /// 
    [HideInInspector]
    public GameObject RawImage;
    /// <summary>
    /// 车载监控相机
    /// </summary>
    /// 
    [HideInInspector]
    public GameObject RawImage_Camera;

    /// <summary>
    /// 箭头链表
    /// </summary>
    /// 

    public List<GameObject> ArrowList;
    /// <summary>
    /// 测试模式水
    /// </summary>
    [HideInInspector]
    public GameObject TestModel_Water_Plane;
    /// <summary>
    /// 充电枪集控屏309空闲中
    /// </summary>
    /// 
    [HideInInspector]
    public GameObject CDQ_309_KXZ;
    /// <summary>
    /// 充电枪集控屏309充电中
    /// </summary>
    /// 
    [HideInInspector]
    public GameObject CDQ_309_CDZ;
    /// <summary>
    /// 充电枪310空闲中
    /// </summary>
    /// 
    [HideInInspector]
    public GameObject CDQ_310_KXZ;
    /// <summary>
    /// 充电枪310充电中
    /// </summary>
    /// 
    [HideInInspector]
    public GameObject CDQ_310_CDZ; 
    /// <summary>
    /// 充电口箭头
    /// </summary>
    /// 
    [HideInInspector]
    public GameObject CDK_Door_arrow;
    /// <summary>
    /// 测试模式充电弓箭头
    /// </summary>
    /// 
    [HideInInspector]
    public GameObject TestCDG_arrow;
    /// <summary>
    /// 测试模式充电弓箭头1
    /// </summary>
    /// 
    [HideInInspector]
    public GameObject TestCDG_arrow1;

    ///// <summary>
    ///// 左手射线
    ///// </summary>
    //public SpriteRenderer L_Hand_Ray;
    ///// <summary>
    ///// 右手射线
    ///// </summary>
    ///// 
    //public SpriteRenderer R_Hand_Ray;
    ///// <summary>
    ///// 左手碰撞点
    ///// </summary>
    //public SpriteRenderer L_Hand_Dot;
    ///// <summary>
    ///// 右手碰撞点
    ///// </summary>
    //public SpriteRenderer R_Hand_Dot;



    /// <summary>
    /// 充电枪集控屏按钮父物体
    /// </summary>
    /// 
    [HideInInspector]
    public GameObject CDQ_JiKongTai_Btn_Parent;
    /// <summary>
    /// 充电枪集控屏充电信息父物体
    /// </summary>
    /// 
    [HideInInspector]
    public GameObject CDQ_JiKongTai_CDXX_Parent;
    /// <summary>
    /// 玩家左手mesh
    /// </summary>
    /// 
    [HideInInspector]
    public GameObject Play_L_Hand_Mesh;
    /// <summary>
    /// 玩家右手mesh
    /// </summary>
    /// 
    [HideInInspector]
    public GameObject Play_R_Hand_Mesh;
    /// <summary>
    /// 充电枪A箭头
    /// </summary>
    /// 
    [HideInInspector]
    public GameObject CDQA_arrow;
    /// <summary>
    /// 充电枪b箭头
    /// </summary>
    /// 
    [HideInInspector]
    public GameObject CDQB_arrow;
    /// <summary>
    /// 充电枪A扳机
    /// </summary>
    /// 
    [HideInInspector]
    public GameObject XuanBi_GunButtoonA;

    /// <summary>
    /// 充电枪B扳机
    /// </summary>
    /// 
    [HideInInspector]
    public GameObject XuanBi_GunButtoonB;
    /// <summary>
    /// 玩家
    /// </summary>
    /// 
    [HideInInspector]
    public GameObject Player;
    /// <summary>
    /// 充电枪父物体
    /// </summary>
    /// 
    [HideInInspector]
    private List<GameObject> ParentList = new List<GameObject>();
    /// <summary>
    /// 车
    /// </summary>
    /// 
    [HideInInspector]
    public GameObject CityBuswithInterior;
    /// <summary>
    /// 车左前轮
    /// </summary>
    /// 
    [HideInInspector]
    public GameObject Wheel_FL;
    /// <summary>
    /// 车右前轮
    /// </summary>
    /// 
    [HideInInspector]
    public GameObject Wheel_FR;
    /// <summary>
    /// 车左后轮
    /// </summary>
    /// 
    [HideInInspector]
    public GameObject Wheel_RL;
    /// <summary>
    /// 车右后轮
    /// </summary>
    /// 
    [HideInInspector]
    public GameObject Wheel_RR;
    /// <summary>
    /// 选择模式Canvas
    /// </summary>
    /// 
    [HideInInspector]
    public GameObject Canvas;
    /// <summary>
    /// 选择充电类型Canvas
    /// </summary>
    /// 
    [HideInInspector]
    public GameObject Canvas1;
    /// <summary>
    /// 充电弓充电触摸屏
    /// </summary>
    /// 
    [HideInInspector]
    public GameObject Canvas2;
    /// <summary>
    /// 车手刹
    /// </summary>
    /// 
    [HideInInspector]
    public GameObject ShouSha001;
    /// <summary>
    /// 车钥匙
    /// </summary>
    /// 
    [HideInInspector]
    public GameObject YaoShi;
    /// <summary>
    /// 充电弓
    /// </summary>
    /// 
    [HideInInspector]
    public GameObject CDG_ani;
    /// <summary>
    /// 充电弓1
    /// </summary>
    /// 
    [HideInInspector]
    public GameObject CDG_ani1;
    /// <summary>
    /// Player左手模型
    /// </summary>
    /// 
    [HideInInspector]
    public GameObject vr_cartoon_hand_prefab_Left;
    /// <summary>
    /// player右手模型
    /// </summary>
    /// 
    [HideInInspector]
    public GameObject vr_cartoon_hand_prefab_Right;
    /// <summary>
    /// 一键充电失败
    /// </summary>
    /// 
    [HideInInspector]
    public GameObject AKeyToStartHit_Image; 
    /// <summary>
    /// 充电弓正在下降提示
    /// </summary>
    /// 
    [HideInInspector]
    public GameObject RechargeDown_Image; 
    /// <summary>
    /// 充电弓降落成功提示
    /// </summary>
    /// 
    [HideInInspector]
    public GameObject RechargeDownYes_Image; 
    /// <summary>
    /// 启动充电成功提示
    /// </summary>
    /// 
    [HideInInspector]
    public GameObject RechargeYes_Image;
    /// <summary>
    /// 充电结束 充电弓上升提示
    /// </summary>
    [HideInInspector]
    public GameObject RechargeOver_Image;
    /// <summary>
    /// 充电详情界面
    /// </summary>
    [HideInInspector]
    public GameObject RechargeState_Image;
    ///// <summary>
    ///// 充电弓镜头
    ///// </summary>
    //[HideInInspector]
    //public GameObject RawImage_Camera0;
    ///// <summary>
    ///// 车轮镜头
    ///// </summary>
    //[HideInInspector]
    //public GameObject RawImage_Camera1;
    /// <summary>
    /// 车充电枪的充电口
    /// </summary>
    [HideInInspector]
    public GameObject chargeDoor;
    /// <summary>
    ///  不允许降弓
    /// </summary>
    [HideInInspector]
    public GameObject NoDeclineCDG_Img;
    /// <summary>
    /// 是否移动车辆界面
    /// </summary>
    [HideInInspector]
    public GameObject IsMoveVehicle_Img;
    /// <summary>
    /// 充电枪B
    /// </summary>
    [HideInInspector]
    public GameObject CDQ_B;
    /// <summary>
    /// 充电枪A
    /// </summary>
    [HideInInspector]
    public GameObject CDQ_A;
    /// <summary>
    /// xuanbiA父物体
    /// </summary>
    [HideInInspector]
    public GameObject XuanBi_GunL_A_Parent;
    /// <summary>
    ///悬臂B父物体
    /// </summary>
    [HideInInspector]
    public GameObject XuanBi_GunL_B_Parent;
    /// <summary>
    /// 车A父物体
    /// </summary>
    [HideInInspector]
    public GameObject Che_A_Parent;
    /// <summary>
    /// 车B父物体
    /// </summary>
    [HideInInspector]
    public GameObject Che_B_Parent;
    /// <summary>
    /// 充电枪B身体
    /// </summary>
    [HideInInspector]
    public GameObject CDQ_B_Body;
    /// <summary>
    /// 充电枪A身体
    /// </summary>
    [HideInInspector]
    public GameObject CDQ_A_Body;
    /// <summary>
    /// 异物
    /// </summary>
    [HideInInspector]
    public GameObject YiWu;
    /// <summary>
    /// 通过提示
    /// </summary>
    [HideInInspector]
    public GameObject PassTraining_Image;
    ///// <summary>
    ///// 充电绿色按钮左
    ///// </summary>
    //public GameObject XuanBi_GreenBuL;
    ///// <summary>
    ///// 充电绿色按钮右
    ///// </summary>
    //public GameObject XuanBi_GreenBuR;
    ///// <summary>
    ///// 插枪黄色按钮左
    ///// </summary>
    //public GameObject XuanBi_YellowBuL;
    ///// <summary>
    ///// 插枪黄色按钮右
    ///// </summary>
    //public GameObject XuanBi_YellowBuR;
    /// <summary>
    /// 左充电枪结束按钮
    /// </summary>
    public GameObject XuanBi_RedBuL1;
    /// <summary>
    /// 右充电枪结束按钮
    /// </summary>
    public GameObject XuanBi_RedBuR1;
    /// <summary>
    /// 玩家提示Canvas
    /// </summary>
    [HideInInspector]
    public GameObject PlayerHint_Canvas;
    /// <summary>
    /// 减速带父物体  
    /// </summary>
    [HideInInspector]
    public GameObject JianShuDaiParent;
    /// <summary>
    /// 培训模式水
    /// </summary>
    [HideInInspector]
    public GameObject Water_Plane;
    /// <summary>
    /// 充电弓编号箭头
    /// </summary>
    [HideInInspector]
    public GameObject CDG_arrow;
    /// <summary>
    /// 充电弓降落箭头
    /// </summary>
    [HideInInspector]
    public GameObject CDGJL_arrow;
    /// <summary>
    /// 挡位N
    /// </summary>
    [HideInInspector]
    public GameObject DangWei_N_GameObject;
    /// <summary>
    /// 显示屏背景无结束充电
    /// </summary>
    [HideInInspector]
    public GameObject BG_Image0;
    /// <summary>
    /// 显示屏背景有结束充电
    /// </summary>
    [HideInInspector]
    public GameObject BG_Image1;
    /// <summary>
    /// 二号充电弓编号Image
    /// </summary>
    [HideInInspector]
    public GameObject TwoRecharge_Img;
    /// <summary>
    /// 结束停电提示Image
    /// </summary>
    [HideInInspector]
    public GameObject RechargeOverHint_Image;
    /// <summary>
    /// 十米二车型提示
    /// </summary>
    [HideInInspector]
    public GameObject Che_10_2m_Hint;
    /// <summary>
    /// 考试模式停车位置提示
    /// </summary>
    [HideInInspector]
    public GameObject Che_Test_Hint;
    //public GameObject Che_6_5m_Hint;

    #endregion

    #region UI
    /// <summary>
    /// 确认充电弓上升button
    /// </summary>
    [HideInInspector]
    public Button CDGSS_Over_Button;
    /// <summary>
    /// 确认充电弓上升button
    /// </summary>
    [HideInInspector]
    public Button CDGSS_Over_Button1;

    /// <summary>
    /// 培训 模式Btn
    /// </summary>
    [HideInInspector]
    public Button TrainingBtn;
    /// <summary>
    /// 测试 模式btn
    /// </summary>
    [HideInInspector]
    public Button TestBtn;
    /// <summary>
    /// 培训-充电弓模式
    /// </summary>
    [HideInInspector]
    public Button ChargingBowBtn;
    /// <summary>
    /// 培训-充电枪模式
    /// </summary>
    [HideInInspector]
    public Button EVChargerBtn;
    /// <summary>
    /// 返回主页
    /// </summary>
    [HideInInspector]
    public Button StartGameBtn;
    /// <summary>
    /// 一键充电失败
    /// </summary>
    [HideInInspector]
    public Button AKeyToStartFail_Btn;
    /// <summary>
    /// 001充电弓
    /// </summary>
    [HideInInspector]
    public Button OneRecharge_Btn;
    /// <summary>
    /// 002充电弓
    /// </summary>
    [HideInInspector]
    public Button TwoRecharge_Btn;
    /// <summary>
    /// 003充电弓
    /// </summary>
    [HideInInspector]
    public Button ThreeRecharge_Btn;
    /// <summary>
    /// 002充电弓
    /// </summary>
    [HideInInspector]
    public Button FourRecharge_Btn;
    /// <summary>
    /// 充电弓降落按钮
    /// </summary>
    [HideInInspector]
    public Button RechargeDown_Btn;
    /// <summary>
    /// 充电弓上升按钮
    /// </summary>
    [HideInInspector]
    public Button RechargeUp_Btn;
    [HideInInspector]
    public Button RechargeUp_Btn1;
    /// <summary>
    /// 开始充电按钮
    /// </summary>
    [HideInInspector]
    public Button StartRecharge_Btn;
    /// <summary>
    /// 结束充电按钮
    /// </summary>
    [HideInInspector]
    public Button OverRecharge_Btn;
    /// <summary>
    /// 一键充电结束充电按钮
    /// </summary>
    [HideInInspector]
    public GameObject ATS_OverRecharge_Btn;
    /// <summary>
    /// 主页按钮
    /// </summary>
    [HideInInspector]
    public Button Homepage_Btn;
    /// <summary>
    /// 一键充电按钮
    /// </summary>
    [HideInInspector]
    public Button AKeyToStart_Button;
    /// <summary>
    /// 调度信息
    /// </summary>
    [HideInInspector]
    public Button Urgency_Button;
    /// <summary>
    /// 车触控屏显示时间
    /// </summary>
    [HideInInspector]
    public Text Time_Text;
    /// <summary>
    /// 电量显示百分比
    /// </summary>
    [HideInInspector]
    public Text Percentage_Text;
    /// <summary>
    /// 充电时间
    /// </summary>
    [HideInInspector]
    public Text Duration_Time;
    /// <summary>
    /// 玩家提示
    /// </summary>
    [HideInInspector]
    public Text PlayerHint_Text;

    /// <summary>
    /// 转场图片
    /// </summary>
    [HideInInspector]
    public Image CutTo_Image;
    #endregion

    #region Bool
    /// <summary>
    /// 是否注意事项1
    /// </summary>
    [HideInInspector]
    public bool IsZYSXOne;
    /// <summary>
    /// 是否下巴士
    /// </summary>
    [HideInInspector]
    public bool IsDownBus;
    /// <summary>
    /// 是否关闭了充电口门
    /// </summary>
    [HideInInspector]
    public bool IsOverCDK_Door;
    /// <summary>
    /// 是否去了集控屏
    /// </summary>
    [HideInInspector]
    public bool IsGoJKP;
    /// <summary>
    /// 是否拿枪
    /// </summary>
    [HideInInspector]
    public string IsNaQiang;
    /// <summary>
    /// 是否播放插枪声音提示
    /// </summary>
    [HideInInspector]
    public bool IsChaQiangAudioHint;
    /// <summary>
    /// 是否正在播放声音
    /// </summary>
    [HideInInspector]
    public bool IsPlayAudio;
    /// <summary>
    /// 是否正在播放动画
    /// </summary>
    [HideInInspector]
    public bool IsPlayAni;
    /// <summary>
    /// 充电枪A是否充电
    /// </summary>
    [HideInInspector]
    public bool CDQAIsCD;
    /// <summary>
    /// 充电枪B是否充电
    /// </summary>
    [HideInInspector]
    public bool CDQBIsCD;
    /// <summary>
    /// 充电枪A是否充电了
    /// </summary>
    [HideInInspector]
    public bool CDQAIsCDLe;
    /// <summary>
    /// 充电枪B是否充电了
    /// </summary>
    [HideInInspector]
    public bool CDQBIsCDLe;
    //public bool CDQBFuWeiLe;
    //public bool CDQAFuWeiLe;
    /// <summary>
    /// 是否充电
    /// </summary>
    [HideInInspector]
    public bool IsRecharge;
    /// <summary>
    /// 是否充电三十秒
    /// </summary>
    [HideInInspector]
    public bool IsRecharge30;


    /// <summary>
    /// 充电三十秒计时器
    /// </summary>
    [HideInInspector]
    public float Recharge30Time;
    /// <summary>
    /// 车充电口打开
    /// </summary>
    [HideInInspector]
    public bool IschargeDoor;

    /// <summary>
    /// 手刹是否关闭
    /// </summary>
    [HideInInspector]
    public bool IsShouSha;
    /// <summary>
    /// 钥匙是否关闭
    /// </summary>
    [HideInInspector]
    public bool IsYaoShi;
    /// <summary>
    /// 判定手充电枪的向量是否小于这个距离
    /// </summary>
    [HideInInspector]
    public float Test_JvLi;
    [HideInInspector]
    public float Test_JvLi1;
    /// <summary>
    /// 充电弓是否降落了
    /// </summary>
    [HideInInspector]
    public bool CDGIsDown;
    /// <summary>
    /// 判定悬臂A里面有没有充电枪
    /// </summary>
    [HideInInspector]
    public bool CDQ_XuanBi_A;
    /// <summary>
    /// 判定悬臂B里面有没有充电枪
    /// </summary>
    [HideInInspector]
    public bool CDQ_XuanBi_B;
    /// <summary>
    /// 判定车A里面有没有充电枪
    /// </summary>
    [HideInInspector]
    public bool CDQ_Che_A;
    /// <summary>
    /// 判定车B里面有没有充电枪
    /// </summary>
    [HideInInspector]
    public bool CDQ_Che_B;
    /// <summary>
    /// 判定是否测试模式
    /// </summary>
    [HideInInspector]
    public bool IsTest;
    /// <summary>
    /// 测试模式是否失败了
    /// </summary>
    [HideInInspector]
    public bool IsTestFalse;
    /// <summary>
    /// 是否挂了空挡
    /// </summary>
    [HideInInspector]
    public bool DangWei_N;



    #endregion

    #region MyRegion

    [HideInInspector]
    public int IsCDGCDQ;

    /// <summary>
    /// 充电弓火花
    /// </summary>
    [HideInInspector]
    public ParticleSystem LavaSparks03_coll;
    /// <summary>
    /// 充电枪火花——L
    /// </summary>
    [HideInInspector]
    public ParticleSystem LavaSparks03_coll_L;
    // 0 未选择模式 1 测验模式 2测验模式充电中
    [HideInInspector]
    public int IsModel;
    /// <summary>
    /// 手刹初始位置
    /// </summary>
    [HideInInspector]
    public Vector3 ShoushaStartPos;
    /// <summary>
    /// 手刹停止位置
    /// </summary>
    [HideInInspector]
    private Vector3 ShoushaStopPos;
    /// <summary>
    /// 受撒谎初始位置
    /// </summary>
    [HideInInspector]
    public Vector3 ShouShaStartPos;
    /// <summary>
    /// 玩家初始旋转
    /// </summary>
    [HideInInspector]
    public  Quaternion PlayerStartRotation = Quaternion.Euler(0f, 127f, 0f);
    /// <summary>
    /// 玩家充电枪旋转
    /// </summary>
    [HideInInspector]
    public  Quaternion PlayerCDQRotation = Quaternion.Euler(0f, -180f, 0f);
    /// <summary>
    /// 玩家充电枪旋转1
    /// </summary>
    [HideInInspector]
    public Quaternion PlayerCDQRotation1 = Quaternion.Euler(0f, 180f, 0f);
    /// <summary>
    /// 手刹初始旋转
    /// </summary>
    [HideInInspector]
    public Quaternion ShouShaStartRotate = Quaternion.Euler(-93.462f, 0f, 0f);
    /// <summary>
    /// 充电时间
    /// </summary>
    [HideInInspector]
    public float RechargeTime;
    /// <summary>
    /// 充电枪B上的手
    /// </summary>
    [HideInInspector]
    public GameObject B_HandVR;
    /// <summary>
    /// 充电枪A上的手
    /// </summary>
    [HideInInspector]
    public GameObject A_HandVR;
    /// <summary>
    /// 悬臂左黄灯
    /// </summary>
    [HideInInspector]
    public Material LeftYellowLight;
    /// <summary>
    /// 悬臂右黄灯
    /// </summary>
    [HideInInspector]
    public Material RightYellowLight;
    /// <summary>
    /// 悬臂左绿灯
    /// </summary>
    [HideInInspector]
    public Material LeftGreenLight;
    /// <summary>
    /// 悬臂右绿灯
    /// </summary>
    [HideInInspector]
    public Material RightGreenLight;
    /// <summary>
    /// 充电弓左绿灯
    /// </summary>
    [HideInInspector]
    public Material LeftCDGgreenLight;
    /// <summary>
    /// 充电弓右红灯
    /// </summary>
    [HideInInspector]
    public Material RightCDRedLight;



    #endregion
    #endregion

   

    private void Awake()
    {
       
        //CDQBFuWeiLe = false;
        //CDQAFuWeiLe = false;
        IsPlayAudio = false;
        CDQAIsCD = false;
        CDQBIsCD = false;
        IsTestFalse = false;
        IsZYSXOne = true;
        LeftYellowLight = GameObject.Find("XuanBi_YellowBuR1").GetComponent<MeshRenderer>().material;
        RightYellowLight = GameObject.Find("XuanBi_YellowBuL1").GetComponent<MeshRenderer>().material;
        LeftGreenLight = GameObject.Find("XuanBi_GreenBuR1").GetComponent<MeshRenderer>().material;
        RightGreenLight = GameObject.Find("XuanBi_GreenBuL1").GetComponent<MeshRenderer>().material;  
        LeftCDGgreenLight = GameObject.Find("AnNiu_A").GetComponent<MeshRenderer>().material;
        RightCDRedLight = GameObject.Find("AnNiu_B").GetComponent<MeshRenderer>().material;
        
       


        SetCDQ_CDD(false, LeftYellowLight);
        SetCDQ_CDD(false, LeftGreenLight);
        SetCDQ_CDD(false, RightYellowLight);
        SetCDQ_CDD(false, RightGreenLight);

        IsRecharge30 = false;
        DangWei_N = false;
        Instance = this;
        IsTest = false;
        Instance = this;
        CDQ_Che_A = false;
        CDQ_Che_B = false;
        CDQ_XuanBi_A = true;
        CDQ_XuanBi_B = true;
        IsModel = 0;
        IsCDGCDQ = -1;
        //RechargeTime = 0;
        CDGIsDown = false;
        IsShouSha = false;
        IsYaoShi = true;
        IschargeDoor = true;
        IsRecharge = false;
        Test_JvLi = 0.1f;
        Test_JvLi1 = 0.1f;
        ShoushaStopPos = new Vector3(-0.01098232f, 0.014881f, 0.047517f);
        LavaSparks03_coll = GameObject.Find("LavaSparks03_coll").GetComponent<ParticleSystem>();
        LavaSparks03_coll_L = GameObject.Find("LavaSparks03_coll_L").GetComponent<ParticleSystem>();
        Player = GameObject.Find("Player");
        ShouSha001 = GameObject.Find("ShouSha001");
        YaoShi = GameObject.Find("YaoShi");
        vr_cartoon_hand_prefab_Left = GameObject.Find("vr_cartoon_hand_prefab_Left");
        vr_cartoon_hand_prefab_Right = GameObject.Find("vr_cartoon_hand_prefab_Right");
        CityBuswithInterior = GameObject.Find("CityBuswithInterior");
        TrainingBtn = GameObject.Find("TrainingBtn").GetComponent<Button>();
        TestBtn = GameObject.Find("TestBtn").GetComponent<Button>();
        ChargingBowBtn = GameObject.Find("ChargingBowBtn").GetComponent<Button>();
        EVChargerBtn = GameObject.Find("EVChargerBtn").GetComponent<Button>();
        AKeyToStart_Button = GameObject.Find("AKeyToStart_Button").GetComponent<Button>();
        AKeyToStartFail_Btn = GameObject.Find("AKeyToStartFail_Btn").GetComponent<Button>(); 
        OneRecharge_Btn = GameObject.Find("OneRecharge_Btn").GetComponent<Button>();
        TwoRecharge_Btn = GameObject.Find("TwoRecharge_Btn").GetComponent<Button>();
        RechargeDown_Btn = GameObject.Find("RechargeDown_Btn").GetComponent<Button>();
        RechargeUp_Btn = GameObject.Find("RechargeUp_Btn").GetComponent<Button>(); 
        RechargeUp_Btn1 = GameObject.Find("RechargeUp_Btn1").GetComponent<Button>();
        StartRecharge_Btn = GameObject.Find("StartRecharge_Btn").GetComponent<Button>(); 
        OverRecharge_Btn = GameObject.Find("OverRecharge_Btn").GetComponent<Button>(); 
        Homepage_Btn = GameObject.Find("Homepage_Btn").GetComponent<Button>(); 
        Urgency_Button = GameObject.Find("Urgency_Button").GetComponent<Button>(); 
         ThreeRecharge_Btn = GameObject.Find("ThreeRecharge_Btn").GetComponent<Button>();
        StartGameBtn = GameObject.Find("StartGameBtn").GetComponent<Button>(); 
         FourRecharge_Btn = GameObject.Find("FourRecharge_Btn").GetComponent<Button>();
        CDGSS_Over_Button = GameObject.Find("CDGSS_Over_Button").GetComponent<Button>();
        CDGSS_Over_Button1 = GameObject.Find("CDGSS_Over_Button1").GetComponent<Button>();

        Time_Text = GameObject.Find("Time_Text").GetComponent<Text>(); 
        Percentage_Text = GameObject.Find("Percentage_Text").GetComponent<Text>(); 
         Duration_Time = GameObject.Find("Duration_Time").GetComponent<Text>();
        PlayerHint_Text = GameObject.Find("PlayerHint_Text").GetComponent<Text>(); 
         CutTo_Image = GameObject.Find("CutTo_Image").GetComponent<Image>();
        B_HandVR = GameObject.Find("B_HandVR");
        A_HandVR = GameObject.Find("A_HandVR");
        Canvas = GameObject.Find("Canvas");
        Canvas1 = GameObject.Find("Canvas1"); 
        Canvas2 = GameObject.Find("Canvas2");
        CDG_ani = GameObject.Find("CDG_ani"); 
        CDG_ani1 = GameObject.Find("CDG_ani1"); 
        chargeDoor = GameObject.Find("chargeDoor");
        CDQ_A = GameObject.Find("XuanBi_GunL_A");
        CDQ_B = GameObject.Find("XuanBi_GunL_B"); 
         YiWu = GameObject.Find("YiWu");
        DangWei_N_GameObject = GameObject.Find("DangWei_N");
        Play_L_Hand_Mesh = GameObject.Find("Play_L_Hand_Mesh"); 
         Play_R_Hand_Mesh = GameObject.Find("Play_R_Hand_Mesh");
        CDQA_arrow = GameObject.Find("CDGA_arrow"); 
         CDQB_arrow = GameObject.Find("CDGB_arrow");
        XuanBi_GunButtoonB = GameObject.Find("XuanBi_GunButtoonR"); 
         XuanBi_GunButtoonA = GameObject.Find("XuanBi_GunButtoonL"); 
         CDK_Door_arrow = GameObject.Find("CDK_Door_arrow"); 
         TestModel_Water_Plane = GameObject.Find("TestModel_Water_Plane");
        TestCDG_arrow = GameObject.Find("TestCDG_arrow"); 
         TestCDG_arrow1 = GameObject.Find("TestCDG_arrow1"); 
         ATS_OverRecharge_Btn = GameObject.Find("ATS_OverRecharge_Btn");
        RechargeAbnormalTermination = GameObject.Find("RechargeAbnormalTermination");
        XuanBi_OringeBuL = GameObject.Find("XuanBi_OringeBuL1");
        XuanBi_OringeBuR = GameObject.Find("XuanBi_OringeBuR1");
        IsMoveVehicle_Img = GameObject.Find("IsMoveVehicle_Img");
        RechargeState_Image = GameObject.Find("RechargeState_Image");
        AKeyToStartHit_Image = GameObject.Find("AKeyToStartHit_Image"); 
        RechargeDown_Image = GameObject.Find("RechargeDown_Image"); 
        RechargeDownYes_Image = GameObject.Find("RechargeDownYes_Image"); 
        RechargeYes_Image = GameObject.Find("RechargeYes_Image"); 
        RechargeOver_Image = GameObject.Find("RechargeOver_Image");
        //RawImage_Camera0 = GameObject.Find("RawImage_Camera0"); 
        //RawImage_Camera1 = GameObject.Find("RawImage_Camera1"); 
        NoDeclineCDG_Img = GameObject.Find("NoDeclineCDG_Img");
        XuanBi_GunL_A_Parent = GameObject.Find("XuanBi_GunL_A_Parent"); 
        XuanBi_GunL_B_Parent = GameObject.Find("XuanBi_GunL_B_Parent");
        Che_A_Parent = GameObject.Find("Che_A_Parent");
        Che_B_Parent = GameObject.Find("Che_B_Parent");
        CDQ_A_Body = GameObject.Find("XuanBi_GunL_A_Body"); 
        CDQ_B_Body = GameObject.Find("XuanBi_GunL_B_Body"); 
         PassTraining_Image = GameObject.Find("PassTraining_Image"); 
         Che_Test_Hint = GameObject.Find("Che_Test_Hint"); 
         CDGJJTZBtn = GameObject.Find("CDGJJTZBtn");
        CDQ_JJTZ_L_arrow = GameObject.Find("CDQ_JJTZ_L_arrow"); 
         CDQ_JJTZ_R_arrow = GameObject.Find("CDQ_JJTZ_R_arrow"); 
         arrowRotation_R = GameObject.Find("arrowRotation_R");
        arrowRotation_L = GameObject.Find("arrowRotation_L");
        arrowRotation_CDG = GameObject.Find("arrowRotation_CDG");
        XuanBi_RedBuL1 = GameObject.Find("XuanBi_RedBuL1");
        XuanBi_RedBuR1 = GameObject.Find("XuanBi_RedBuR1");
        rayParent_L = GameObject.Find("Neo2_L");
        L_Ray = GameObject.Find("L_Ray");
        L_StartRayPos = GameObject.Find("L_StartRayPos");
        L_Sphere = GameObject.Find("L_Sphere");

        rayParent_R = GameObject.Find("Neo2_R");
        R_Ray = GameObject.Find("R_Ray");
        R_StartRayPos = GameObject.Find("R_StartRayPos");
        R_Sphere = GameObject.Find("SphereR");

        Obi_Gameobject = GameObject.Find("Obi_GameObject");
        XuanBi_Wire_R = GameObject.Find("XuanBi_Wire_R");
        XuanBi_Wire_L = GameObject.Find("XuanBi_Wire_L");
        RawImage = GameObject.Find("RawImage");
        RawImage_Camera = GameObject.Find("RawImage_Camera");

        //L_Hand_Ray = GameObject.Find("PvrController0").transform.Find("ray_alpha").transform.GetComponent<SpriteRenderer>();
        //R_Hand_Ray = GameObject.Find("PvrController1").transform.Find("ray_alpha").transform.GetComponent<SpriteRenderer>();
        //L_Hand_Dot = GameObject.Find("PvrController0").transform.Find("dot").transform.GetComponent<SpriteRenderer>();
        //R_Hand_Dot = GameObject.Find("PvrController1").transform.Find("dot").transform.GetComponent<SpriteRenderer>();

        CDQ_309_KXZ = GameObject.Find("CDQ_309_KXZ").gameObject;
        CDQ_309_CDZ = GameObject.Find("CDQ_309_CDZ").gameObject;
        CDQ_310_KXZ = GameObject.Find("CDQ_310_KXZ").gameObject;
        CDQ_310_CDZ = GameObject.Find("CDQ_310_CDZ").gameObject;
        CDQ_JiKongTai_Btn_Parent = GameObject.Find("CDQ_JiKongTai_Btn_Parent").gameObject;
        CDQ_JiKongTai_CDXX_Parent = GameObject.Find("CDQ_JiKongTai_CDXX_Parent").gameObject;

        PlayerHint_Canvas = GameObject.Find("PlayerHint_Canvas"); 
         JianShuDaiParent = GameObject.Find("JianShuDaiParent");  
         Water_Plane = GameObject.Find("Water_Plane");
          CDG_arrow = GameObject.Find("CDG_arrow");
        CDGJL_arrow = GameObject.Find("CDGJL_arrow");
        TwoRecharge_Img = GameObject.Find("TwoRecharge_Img");
        TwoRecharge_Img.transform.GetChild(1).gameObject.SetActive(false);
        BG_Image0 = GameObject.Find("BG_Image0"); 
         BG_Image1 = GameObject.Find("BG_Image1");
        //Che_6_5m_Hint = GameObject.Find("Che_6_5m_Hint");
        Che_10_2m_Hint = GameObject.Find("Che_10_2m_Hint");
        RechargeOverHint_Image = GameObject.Find("RechargeOverHint_Image");
        ShoushaStartPos = ShouSha001.transform.localPosition;
        Wheel_FL = CityBuswithInterior.transform.Find("Wheel_FL").transform.gameObject;
        Wheel_FR = CityBuswithInterior.transform.Find("Wheel_FR").transform.gameObject;
        Wheel_RL = CityBuswithInterior.transform.Find("Wheel_RL").transform.gameObject;
        Wheel_RR = CityBuswithInterior.transform.Find("Wheel_RR").transform.gameObject;
        ShoushaStartPos = ShouSha001.transform.localPosition;
        ShouShaStartPos = ShouSha001.transform.localPosition;
        ParentList.Add(Che_A_Parent);
        ParentList.Add(Che_B_Parent);
        ParentList.Add(XuanBi_GunL_A_Parent);
        ParentList.Add(XuanBi_GunL_B_Parent);
        //A_HandVR.gameObject.SetActive(false);
        //B_HandVR.gameObject.SetActive(false);

        A_HandVR.transform.GetChild(0).gameObject.SetActive(false);
        A_HandVR.transform.GetChild(1).gameObject.SetActive(false);
        B_HandVR.transform.GetChild(0).gameObject.SetActive(false);
        B_HandVR.transform.GetChild(1).gameObject.SetActive(false);

        CDQ_309_CDZ.transform.GetChild(2).transform.gameObject.SetActive(false);
        //CDQ_310_CDZ.transform.GetChild(2).transform.gameObject.SetActive(false);
        CDQ_JiKongTai_CDXX_Parent.SetActive(false);
        RechargeOverHint_Image.SetActive(false);
        BG_Image1.SetActive(false);
        IsMoveVehicle_Img.SetActive(false);
        Urgency_Button.gameObject.SetActive(false);
        StartRecharge_Btn.transform.gameObject.SetActive(false);
        RechargeDown_Btn.transform.gameObject.SetActive(false);
        RechargeUp_Btn.transform.gameObject.SetActive(false);
        RechargeUp_Btn1.transform.gameObject.SetActive(false);
        vr_cartoon_hand_prefab_Left.transform.gameObject.SetActive(false);
        vr_cartoon_hand_prefab_Right.transform.gameObject.SetActive(false);
        AKeyToStartFail_Btn.transform.gameObject.SetActive(false);
        OneRecharge_Btn.gameObject.SetActive(false); 
        TwoRecharge_Btn.gameObject.SetActive(false);
        OverRecharge_Btn.gameObject.SetActive(false);
        RechargeDownYes_Image.SetActive(false);
        RechargeDown_Image.SetActive(false); 
        AKeyToStartHit_Image.SetActive(false);
        RechargeYes_Image.SetActive(false);
        RechargeOver_Image.SetActive(false);
        RechargeState_Image.gameObject.SetActive(false);
        //RawImage_Camera1.SetActive(false);
        NoDeclineCDG_Img.SetActive(false); 
        PassTraining_Image.SetActive(false); 
        CDG_arrow.SetActive(false);
        CDGJL_arrow.SetActive(false);
        TwoRecharge_Img.SetActive(false);
        PlayerHint_Canvas.SetActive(false);
        Canvas1.SetActive(false);
        Canvas2.SetActive(false);
    }
    public void SetPlayerStartValue()
    {
        AKeyToStart_Button.transform.GetChild(0).transform.GetChild(0).GetComponent<MeshRenderer>().enabled = false;
        arrowRotation_CDG.SetActive(false);
        arrowRotation_R.SetActive(false);
        arrowRotation_L.SetActive(false);
        CDQ_JJTZ_R_arrow.SetActive(false);
        CDQ_JJTZ_L_arrow.SetActive(false);
        SetRawImage(false);
        SetObi(false);
        RechargeAbnormalTermination.SetActive(false);
        IsZYSXOne = true;
        CDGSS_Over_Button.gameObject.SetActive(false);
        CDGSS_Over_Button1.gameObject.SetActive(false);
        LavaSparks03_coll_L.Stop();
        TestCDG_arrow.SetActive(false);
        TestCDG_arrow1.SetActive(false);
        TestModel_Water_Plane.SetActive(false);
        YiWu.SetActive(false);
        Che_Test_Hint.SetActive(false);
        Che_Test_Hint.transform.localPosition = new Vector3(-23f, -0.6f, -37.398f);
        //CDQBFuWeiLe = false;
        //CDQAFuWeiLe = false;
        SetHight(XuanBi_GunButtoonA, false);
        SetHight(XuanBi_GunButtoonB, false);
        SetHight(XuanBi_OringeBuL, false);
        SetHight(XuanBi_OringeBuR, false);
        CDQB_arrow.SetActive(false); 
        CDQA_arrow.SetActive(false);
        CDK_Door_arrow.SetActive(false);
        CDQAIsCD = false;
        CDQBIsCD = false;
       
        SetCDQ_CDD(false, LeftYellowLight);
        SetCDQ_CDD(false, LeftGreenLight);
        SetCDQ_CDD(false, RightYellowLight);
        SetCDQ_CDD(false, RightGreenLight);

        //A_HandVR.gameObject.SetActive(false);
        //B_HandVR.gameObject.SetActive(false);
        A_HandVR.transform.GetChild(0).gameObject.SetActive(false);
        A_HandVR.transform.GetChild(1).gameObject.SetActive(false);
        B_HandVR.transform.GetChild(0).gameObject.SetActive(false);
        B_HandVR.transform.GetChild(1).gameObject.SetActive(false);
        A_HandVR.GetComponent<Animator>().SetBool("IsHold", false);
        A_HandVR.GetComponent<Animator>().SetBool("IsPine", false);
        B_HandVR.GetComponent<Animator>().SetBool("IsHold", false);
        B_HandVR.GetComponent<Animator>().SetBool("IsPine", false);
        IsRecharge30 = false;
        DangWei_N = false;
        IsTest = false;
        Instance = this;
        IsTestFalse = false;
        CDQ_Che_A = false;
        CDQ_Che_B = false;
        CDQ_XuanBi_A = true;
        CDQ_XuanBi_B = true;
        IsModel = 0;
        //RechargeTime = 0;
        CDGIsDown = false;
        IsShouSha = false;
        IsYaoShi = true;
        IschargeDoor = true;
        IsRecharge = false;
        vr_cartoon_hand_prefab_Left.transform.gameObject.SetActive(false);
        vr_cartoon_hand_prefab_Right.transform.gameObject.SetActive(false);
        Player.transform.SetParent(null);
        Player.transform.localPosition = PlayerPosition.PlayerStartPos;
        Player.transform.localRotation = PlayerStartRotation;
        CityBuswithInterior.transform.position = PlayerPosition.CityBuswithInteriorStartPos;
        Canvas.SetActive(true);
        TrainingBtn.gameObject.SetActive(true);
        TestBtn.gameObject.SetActive(true);
        Canvas1.gameObject.SetActive(false);
        if (IsShouSha)
        {
            CloseShouSha(false, 0f);
        }
        if (IsYaoShi)
        {
            CloseYaoShi(false, 0f);
        }
        RechargeOverHint_Image.gameObject.SetActive(false);
        Canvas2.SetActive(true);
        SetC2ChildFalse();
        AKeyToStart_Button.gameObject.SetActive(true);
        IsModel = 0;
        IsCDGCDQ = -1;
        //RechargeTime = 0;  
        if (IschargeDoor == false)
        {
            IschargeDoor = true;
            DOTweenRotate(chargeDoor.transform.gameObject, new Vector3(0f, 180f, 0f), 0f, true);
        }
        CDGAniUP();
    }

    public void SetRawImage(bool b )
    {
        if (b)
        {
            RawImage.SetActive(true);
            RawImage_Camera.SetActive(true);
        }
        else
        {
            RawImage.SetActive(false);
            RawImage_Camera.SetActive(false);
        }
    }

    public void SetObi(bool b)
    {
        if (b)
        {
            Obi_Gameobject.SetActive(true);
            XuanBi_Wire_L.SetActive(false);
            XuanBi_Wire_R.SetActive(false);
        }
        else
        {
            Obi_Gameobject.SetActive(false);
            XuanBi_Wire_L.SetActive(true);
            XuanBi_Wire_R.SetActive(true);
        }
    }

    /// <summary>
    /// 隐藏所有箭头
    /// </summary>
    public void SetArrowListFalst()
    {
        if (ArrowList.Count >0)
        {
            for (int i = 0; i < ArrowList.Count; i++)
            {
                ArrowList[i].transform.GetComponent<MeshRenderer>().enabled = false;
            }
        }
    }
    /// <summary>
    /// 显示所有箭头
    /// </summary>
    public void SetArrowListTrue()
    {
        if (ArrowList.Count > 0)
        {
            for (int i = 0; i < ArrowList.Count; i++)
            {
                ArrowList[i].transform.GetComponent<MeshRenderer>().enabled = true;
            }
        }
    }

    public Tweener TwenerMove;
    public Tweener TwenerRotate;
    public void DOTweenRotate(GameObject go,Vector3 v3, float Time ,bool b)
    {
        if (b)
        {
            TwenerRotate = go.transform.DOLocalRotate(v3, Time, RotateMode.WorldAxisAdd);
        }
        else
        {
            TwenerRotate = go.transform.DOLocalRotate(v3, Time, RotateMode.LocalAxisAdd);
        }
    }

    public void DOTweenMove(GameObject go, Vector3 v3, float Time)
    {
        TwenerMove = go.transform.DOLocalMove(v3, Time);
    }
    /// <summary>
    /// 充电弓下降动画
    /// </summary>
    public void CDGAniDown()
    {
        CDG_ani1.transform.GetComponent<Animator>().SetBool("IsUp", false);
        CDG_ani1.transform.GetComponent<Animator>().SetBool("IsDown", true);
        CDG_ani1.transform.GetComponent<Animator>().SetBool("IsCDGBreak", false);
        CDGIsDown = true;
    }
    /// <summary>
    /// 充电弓上升动画
    /// </summary>
    public void CDGAniUP()
    {
        CDG_ani1.transform.GetComponent<Animator>().SetBool("IsDown", false);
        CDG_ani1.transform.GetComponent<Animator>().SetBool("IsUp", true);
        CDG_ani1.transform.GetComponent<Animator>().SetBool("IsCDGBreak", false);
        CDGIsDown = false;
    }
    /// <summary>
    /// 充电弓变形动画
    /// </summary>
    public void CDGAniBreak()
    {
        CDG_ani1.transform.GetComponent<Animator>().SetBool("IsDown", false);
        CDG_ani1.transform.GetComponent<Animator>().SetBool("IsUp", false);
        CDG_ani1.transform.GetComponent<Animator>().SetBool("IsCDGBreak", true);
        CDGIsDown = false;
    }

    //关闭手刹
    public void CloseShouSha(bool b,float f)
    {
        if (/*GameObjectIns.Instance.ShouSha001.transform.eulerAngles.x < 290f*/b)
        {
            PlayerAudio.Instance.PlayBtnAudio();
            IsShouSha = true;
            DOTweenMove(ShouSha001, ShoushaStopPos, f);
            DOTweenRotate(ShouSha001, new Vector3(-32f, 0f, 0f), f, false);
        }
        else
        {
            IsShouSha = false;
            DOTweenMove(ShouSha001, ShoushaStartPos, f);
            DOTweenRotate(ShouSha001, new Vector3(32f, 0f, 0f), f, false);
        }
        IsYaoShiShouShaOver();
    }

    //关闭钥匙
    public void CloseYaoShi(bool b,float f)
    {
        if (/*GameObjectIns.Instance.YaoShi.transform.eulerAngles.x > 90f*/b)
        {
            IsYaoShi = false;
            PlayerAudio.Instance.PlayBtnAudio();
            DOTweenRotate(YaoShi, new Vector3(90f, 0f, 0f), f, false);
        }
        else
        {
            IsYaoShi = true;
            DOTweenRotate(YaoShi, new Vector3(-90f, 0f, 0f), f, false);
        }

        IsYaoShiShouShaOver();
    }

    public void SetAKeyToStart_ButtonCancelInvoke()
    {
        SetCutTo(1f, 0f, 2f); 
        CancelInvoke("AwaitCdqHint");
        CancelInvoke("SetCDQCutTo");
    }
    /// <summary>
    /// 判断手刹钥匙是否关闭
    /// </summary>
    public void IsYaoShiShouShaOver()
    {
        if (DangWei_N)
        {
            DOTweenMove(DangWei_N_GameObject, PlayerPosition.DangWeiN_StopV3, 1f);
            DangWei_N_GameObject.transform.DOLocalMove(PlayerPosition.DangWeiN_StopV3, 1f).OnComplete(()=> {
                DangWei_N_GameObject.transform.DOLocalMove(PlayerPosition.DangWeiN_StartV3, 1f);
            });
        }
        else
        {
            DangWei_N_GameObject.transform.localPosition = PlayerPosition.DangWeiN_StartV3;
        }
        AKeyToStart_Button.transform.GetChild(0).transform.GetChild(0).GetComponent<MeshRenderer>().enabled = false;
        if (IsTest)
        {
            if (IsCDGCDQ == -1)
            {
                print("未选择充电模式");
            }
            else if (IsCDGCDQ == 0)
            {
                if (IsTestFalse)
                {

                    if (DangWei_N == false)
                    {
                        PlayDangWeiNAudio();
                        SetHight(DangWei_N_GameObject, true);
                    }

                    if (IsShouSha && DangWei_N)
                    {
                        print("充电弓模式");
                        AKeyToStart_Button.transform.GetChild(0).transform.GetChild(0).GetComponent<MeshRenderer>().enabled = true;
                        CancelInvoke("PlayDangWeiNAudio");
                        print("15_2使用手柄点击屏幕上的一键开启按钮可以自动完成充电");
                        PlayerAudio.Instance.PlayAudioClipTest(15);

                        PlayerHint_Canvas.gameObject.SetActive(true);
                        PlayerHint_Text.text = "请使用vr手柄点击驾驶室上方屏幕";
                    }
                    else
                    {
                        print("未拉手刹或挂空挡");
                    }
                }
            }
            else if (IsCDGCDQ == 1)
            {
                if (IsTestFalse)
                {
                    if (IsShouSha == false)
                    {
                        print("手刹未关闭");
                        SetHight(ShouSha001, true);
                        SetHight(DangWei_N_GameObject, false);
                        SetHight(YaoShi, false);
                    }
                    else if (DangWei_N == false)
                    {
                        PlayDangWeiNAudio();
                        SetHight(DangWei_N_GameObject, true);
                        SetHight(YaoShi, false);
                    }
                    else if (IsYaoShi)
                    {
                        print("1     1");
                        PlayYaoShiAudio();
                        SetHight(YaoShi, true);
                    }

                    if (IsYaoShi == false && IsShouSha && DangWei_N)
                    {
                        PlayerHint_Canvas.gameObject.SetActive(true);
                        PlayerHint_Text.text = "请下车进行充电操作";

                        Invoke("AwaitCdqHint", 3f);
                    }
                    else
                    {
                        print("钥匙或者手刹未关闭");
                    }
                }
                else
                {
                    if (IsShouSha && DangWei_N && IsYaoShi == false)
                    {
                        print("下车");
                        PlayerHint_Canvas.gameObject.SetActive(true);
                        PlayerHint_Text.text = "请下车进行充电操作";


                        Invoke("AwaitCdqHint", 3f);
                    }
                }
            }
        }
        else
        {
            if (IsCDGCDQ == -1)
            {
                print("未选择充电模式");
            }
            else if (IsCDGCDQ == 0)
            {
                print(IsShouSha + "   " + DangWei_N);

                //if (IsShouSha == false)
                //{
                //    print("手刹未关闭");
                //    SetHight(ShouSha001, true);
                //}
                if (DangWei_N == false)
                {
                    PlayDangWeiNAudio();
                    SetHight(DangWei_N_GameObject, true);
                }

                if (IsShouSha && DangWei_N)
                {
                    print("充电弓模式");
                    AKeyToStart_Button.transform.GetChild(0).transform.GetChild(0).GetComponent<MeshRenderer>().enabled = true;

                    CancelInvoke("PlayDangWeiNAudio");
                    print("15_2使用手柄点击屏幕上的一键开启按钮可以自动完成充电");
                    PlayerAudio.Instance.PlayAudioClipTest(15);

                    PlayerHint_Canvas.gameObject.SetActive(true);
                    PlayerHint_Text.text = "请使用vr手柄点击驾驶室上方屏幕";
                }
                else
                {
                    print("未拉手刹或挂空挡");
                }
            }
            else if (IsCDGCDQ == 1)
            {
                if (IsShouSha == false)
                {
                    print("手刹未关闭");
                    SetHight(ShouSha001, true);
                }
                else if (DangWei_N == false)
                {
                    PlayDangWeiNAudio();
                    SetHight(DangWei_N_GameObject, true);
                }
                else if (IsYaoShi)
                {
                    print("1     2");
                    PlayYaoShiAudio();
                    SetHight(YaoShi, true);
                }

                if (IsYaoShi == false && IsShouSha && DangWei_N)
                {
                    PlayerHint_Canvas.gameObject.SetActive(true);
                    PlayerHint_Text.text = "请下车进行充电操作";

                    Invoke("AwaitCdqHint", 3f);
                }
                else
                {
                    print("钥匙或者手刹未关闭");
                }
            }
        }
    }
    /// <summary>
    /// 重复播放挡位声音
    /// </summary>
    private void PlayDangWeiNAudio()
    {
        PlayerHint_Text.text = "请将手柄靠近空挡";
        print("31请挂空挡！");
        PlayerAudio.Instance.PlayAudioClipTest(31);
        Invoke("PlayDangWeiNAudio", 4);
    }
    /// <summary>
    /// 重复播放手刹声音
    /// </summary>
    public void PlayLaShouShaAudio()
    {
        print("33请拉起手刹！");
        PlayerAudio.Instance.PlayAudioClipTest(33);
        Invoke("PlayLaShouShaAudio", 7f);
    }
    /// <summary>
    /// 重复播放钥匙声音
    /// </summary>
    public void PlayYaoShiAudio()
    {
        Instance.PlayerHint_Text.text = "请将手柄靠近钥匙";
        print("钥匙未关闭");
        PlayerAudio.Instance.PlayAudioClipTest(32);
        Invoke("PlayYaoShiAudio", 5f);
    }
    /// <summary>
    /// 设置关闭重复播放挡位、手刹、钥匙的声音
    /// </summary>
    public void SetCancelInvoke()
    {
        CancelInvoke("PlayLaShouShaAudio");
        CancelInvoke("PlayDangWeiNAudio");
        CancelInvoke("PlayYaoShiAudio");
    }
    /// <summary>
    /// 充电枪关灯转场动画
    /// </summary>
    private void AwaitCdqHint()
    {
        print("关灯");
        SetCutTo(0f, 1f, 1.5f);
        Invoke("SetCDQCutTo",1.5f);
    }
    /// <summary>
    /// 设置充电枪开灯动画
    /// </summary>
    private void SetCDQCutTo()
    {
        IsDownBus = false;
        print("开灯");
        SetCutTo(1f, 0f, 1.5f);
        print("充电枪模式");
        print("钥匙手刹已关闭");
        if (!IsTest || IsTestFalse)
        {
            print("27请把手柄靠近充电口舱门可以将其打开");
            PlayerAudio.Instance.PlayAudioClipTest(27);
            SetHight(chargeDoor.transform.GetChild(0).transform.gameObject, true);

            CDK_Door_arrow.SetActive(true);
        }
        //vr_cartoon_hand_prefab_Left.transform.GetChild(1).transform.gameObject.SetActive(false);
        //vr_cartoon_hand_prefab_Right.transform.GetChild(1).transform.gameObject.SetActive(false);

        //vr_cartoon_hand_prefab_Left.transform.parent.transform.GetComponent<MeshRenderer>().enabled = true;
        //vr_cartoon_hand_prefab_Right.transform.parent.transform.GetComponent<MeshRenderer>().enabled = true;
        PlayerHint_Canvas.SetActive(false);

        CityBuswithInterior.transform.position = PlayerPosition.CityBuswithInteriorCDQPos;
        Player.transform.position = PlayerPosition.PlayerCDQXuanBiPos;
        print(Player.transform.eulerAngles);
        //Player.transform.localRotation = PlayerCDQRotation1;
        Player.transform.DOBlendableLocalRotateBy(Vector3.zero, 0f);
        print(Player.transform.eulerAngles);
    }
    /// <summary>
    /// 设置充电弓结束提示面板 判断是提示面板是考试还是继续充电枪考试还是结束充电枪的培训
    /// </summary>
    public void SetCDGCanvasHint1()
    {
        if (IsTest)
        {
            if (IsTestFalse == false)
            {
                print("充电弓测验合格 即将测验充电枪模式");
                PlayerHint_Canvas.gameObject.SetActive(true);
                PlayerHint_Text.text = "充电弓测验合格 即将测验充电枪模式";
            }
            else
            {
                PlayerHint_Canvas.gameObject.SetActive(true);
                PlayerHint_Text.text = "充电弓培训结束 即将返回测验充电弓模式";
                print("48充电弓测试开始，请先完成停车操作");
                PlayerAudio.Instance.PlayAudioClipTest(48);
                Invoke("CDGGameOverHint", 5f);
            }
        }
        else
        {
            print("充电弓结束提示");
            PlayerHint_Canvas.gameObject.SetActive(true);
            PlayerHint_Text.text = "充电弓培训完成 即将返回";
        }
        Invoke("CDGGameOverHint", 5f);
    }
    /// <summary>
    /// 充电弓结束转场动画
    /// </summary>
    public void CDGGameOverHint()
    {
        SetCutTo(0f, 1f, 1.6f);
        Invoke("CDGGameOver", 1.6f);
    }
    /// <summary>
    /// 充电弓结束提示面板 判断是回到考试还是继续充电枪考试还是结束充电枪的培训
    /// </summary>
    private void CDGGameOver()
    {
        //SceneM();
        //LeftCDGgreenLight.DisableKeyword("_EMISSION");
        RightCDRedLight.DisableKeyword("_EMISSION");
        SetHandModel(false);
        if (IsTest)
        {
            if (IsTestFalse)
            {
                PlayerHint_Text.text = "开始测验充电弓模式";
                print("开始测验充电弓模式");
                CDGAniDownOverSetHandTrueInvoke();
                vr_cartoon_hand_prefab_Left.AddComponent<LeftShouCdgTrigger>();
                vr_cartoon_hand_prefab_Right.AddComponent<RightShouCdgTrigger>();
            }
            else
            {
                PlayerHint_Text.text = "充电弓考试完成，下面进入充电枪操作流程考试";
                print("42充电弓考试完成，下面进入充电枪操作流程考试");
                PlayerAudio.Instance.PlayAudioClipTest(42);

                vr_cartoon_hand_prefab_Left.AddComponent<Left_CDQ_Test_Model>();
                vr_cartoon_hand_prefab_Right.AddComponent<Right_CDQ_Test_Model>();

                Destroy(vr_cartoon_hand_prefab_Left.transform.GetComponent<LeftShouCdgTrigger>());
                Destroy(vr_cartoon_hand_prefab_Right.transform.GetComponent<RightShouCdgTrigger>());

            }

        }
        else
        {
            if (transform.GetComponent<TrainingModel1>() == null)
            {
                transform.gameObject.AddComponent<TrainingModel1>();
            }
        }
        SetCDGValue();
        SetRawImage(false);
        Destroy(vr_cartoon_hand_prefab_Left.transform.GetComponent<LeftShouShaTrigger>());
        Destroy(vr_cartoon_hand_prefab_Right.transform.GetComponent<RightYaoShiTrigger>());
    }
    /// <summary>
    /// 充电枪结束 判断是考试模式还是考试失败还是培训模式
    /// </summary>
    public void CDQGameOver1()
    {
        PlayerHint_Canvas.SetActive(true);
        PlayerHint_Text.text = "充电枪培训完成 即将返回";
        //考试模式 并且 考试失败 重新培训结束后播放“49充电枪测试开始，请先完成停车操作”
        if (IsTest)
        {
            if (IsTestFalse)
            {
                PlayerHint_Text.text = "充电枪培训完成 即将返回重新考试";
                print("49充电枪测试开始，请先完成停车操作");
                PlayerAudio.Instance.PlayAudioClipTest(49);
            }
            else
            {
                PlayerHint_Text.text = "恭喜您 考试通过！";
                print("43恭喜您，考试通过！");
                PlayerAudio.Instance.PlayAudioClipTest(43);
            }
            
        }
        Invoke("AwaitCDQGameOver", 5f);
    }
    /// <summary>
    /// 关灯转场动画
    /// </summary>
    private void AwaitCDQGameOver()
    {
        SetCutTo(0f, 1f, 1.5f);
        Invoke("SetCdqGameOver", 1.5f);
    }
    /// <summary>
    /// 设置充电枪结束是返回考试还是返回培训还是通过考试
    /// </summary>
    private void SetCdqGameOver()
    {
        DOTweenRotate(chargeDoor, new Vector3(0f, 180f, 0f), 2f, true);
        SetCDQ_CDD(false, LeftYellowLight);
        SetCDQ_CDD(false, LeftGreenLight);
        SetCDQ_CDD(false, RightYellowLight);
        SetCDQ_CDD(false, RightGreenLight);
        if (IsTest)
        {
            if (IsTestFalse)
            {
                print("充电枪培训结束 即将返回测验充电枪模式");
                PlayerHint_Canvas.gameObject.SetActive(true);
                PlayerHint_Text.text = "充电枪培训结束 即将返回测验充电枪模式";

                vr_cartoon_hand_prefab_Left.AddComponent<Left_CDQ_Test_Model>();
                vr_cartoon_hand_prefab_Right.AddComponent<Right_CDQ_Test_Model>();

                IsTestFalse = false;
                SetObi(false);
                Destroy(vr_cartoon_hand_prefab_Right.GetComponent<RightShouTrigger>());
                Destroy(vr_cartoon_hand_prefab_Left.GetComponent<LeftShouTrigger>());
            }
            else
            {
                if (TestValue.PlayID == 10000)
                {
                    SceneManager.LoadScene("Login");
                    return;
                }
                SetObi(false);
                Destroy(vr_cartoon_hand_prefab_Left.transform.GetComponent<Left_CDQ_Test_Model>());
                Destroy(vr_cartoon_hand_prefab_Right.transform.GetComponent<Right_CDQ_Test_Model>());
                //Destroy(vr_cartoon_hand_prefab_Right.transform.GetComponent<RightShouTrigger>());

                TestValue.PlayScore = 100;
                GameObject.Find("TestValue_Manager").GetComponent<PostData>().Logout();
                
                TestValue.PlayID = 0;
                TestValue.PlayScore = 0;
                SceneManager.LoadScene("Login");
            }
        }
        else
        {
            SetObi(false);
            Destroy(vr_cartoon_hand_prefab_Left.transform.GetComponent<LeftShouTrigger>());
            Destroy(vr_cartoon_hand_prefab_Right.transform.GetComponent<RightShouTrigger>());
            if (transform.gameObject.GetComponent<TrainingModel1>() == null)
            {
                transform.gameObject.AddComponent<TrainingModel1>();
            }
        }
      
    }

    public void SetInvokeToParkingLotScene()
    {
        Invoke("ToParkingLotScene", 2f);
    }

    private void ToParkingLotScene()
    {
        SceneManager.LoadScene("ParkingLotScene");
    }

    /// <summary>
    /// 设置充电弓初始值
    /// </summary>
    public void SetCDGValue()
    {
        CancelInvoke();
        CancelInvoke("PlayLaShouShaAudio");
        if (IsShouSha)
        {
            CloseShouSha(false, 0.01f);
        }
        DangWei_N = false;
        IsRecharge = false;
        IsRecharge30 = false;
        CDGAniUP();
        SetC2ChildFalse();
        AKeyToStart_Button.gameObject.SetActive(true);
        CDGIsDown = false;
        CDGJL_arrow.SetActive(false);
        CDG_arrow.SetActive(false);



        //TwoRecharge_Btn.transform.GetChild(3).gameObject.SetActive(true);
        //BG_Image0.SetActive(true);
        //BG_Image1.SetActive(false);
        //IsModel = 0;
    }
    private int RechargeSpeed=2;

    private bool HandHint;
    // Update is called once per frame
    private void Update()
    {
        if (Pvr_ControllerManager.GetControllerConnectionState(0) != 1 || Pvr_ControllerManager.GetControllerConnectionState(1) != 1)
        {
            HandHint = true;
            PlayerHint_Canvas.SetActive(true);
            PlayerHint_Text.text = "请先确认左右手柄连接成功";
        }
        else
        {
            if (HandHint)
            {
                HandHint = false;
                PlayerHint_Canvas.SetActive(false);
            }
        }
        

        Time_Text.text = DateTime.Now.Hour.ToString()+":"+ DateTime.Now.Minute.ToString();
        if (IsRecharge)
        {
            if (!IsTest||IsTestFalse)
            {
                RechargeSpeed = 2;
            }
            else
            {
                RechargeSpeed = 4;
            }
            RechargeTime += Time.deltaTime* RechargeSpeed;
            if (RechargeTime<=100f)
            {
                if (RechargeTime>=30 && IsRecharge30==false )
                {
                    if (IsTest == false || IsTestFalse)
                    {
                        IsRecharge30 = true;
                        SetC2ChildFalse();
                        Homepage_Btn.transform.GetChild(0).transform.gameObject.SetActive(false);
                        Homepage_Btn.gameObject.SetActive(true);
                        RechargeState_Image.gameObject.SetActive(true);
                        OverRecharge_Btn.gameObject.SetActive(true);
                        BG_Image0.gameObject.SetActive(false);
                        BG_Image1.gameObject.SetActive(true);
                        print("12_2充电信息包括电池电量百分比（SOC），已充电量，充电功率，充电电流，充电电压。充电过程中，如果需要返回上一级菜单可点击左上角小房子按钮，如果需要提前结束充电，可点击终端面板右下角的“结束充电”按钮结束充电。");
                        PlayerAudio.Instance.PlayAudioClipTest(12);
                    }
                    else
                    {
                        print("测验模式");
                        IsRecharge30 = true;
                        SetC2ChildFalse();
                        Homepage_Btn.gameObject.SetActive(true);
                        RechargeState_Image.gameObject.SetActive(true);
                        OverRecharge_Btn.gameObject.SetActive(true);
                        BG_Image0.gameObject.SetActive(false);
                        BG_Image1.gameObject.SetActive(true);
                    }
                   
                }
                else if (IsRecharge30 == false)
                {
                    Recharge30Time -= Time.deltaTime * RechargeSpeed;
                    int R30 = (int)Recharge30Time;

                    RechargeYes_Image.transform.GetChild(0).GetComponent<Text>().text = R30.ToString();
                }

                if (IsModel == 1 && RechargeTime>5f)
                {
                    //OverRecharge_Btn.transform.parent.gameObject.SetActive(false);
                    //Urgency_Button.gameObject.SetActive(true);
                    SetC2ChildFalse();

                    RechargeYes_Image.SetActive(true);
                    Urgency_Button.gameObject.gameObject.SetActive(true);
                    IsMoveVehicle_Img.SetActive(true);

                    IsModel = 2;
                }

                Percentage_Text.text = ((int)RechargeTime).ToString()+"%";
                if (RechargeTime<60f)
                {
                    Duration_Time.text ="0h:"+ RechargeTime.ToString()+"m";
                }
                else
                {
                    Duration_Time.text = "1h:" + (RechargeTime-60f).ToString() + "m";
                }
            }
            else
            {
                IsRecharge = false;
                print("充电100%    充满电");
                CDGAniUP();
                CDGJL_arrow.SetActive(true);
                
                if (IsTest)
                {
                    if (IsTestFalse)
                    {
                        print("13充电结束后，充电弓自动上升，可通过监控或者下车查看充电弓是否上升成功");
                        PlayerAudio.Instance.PlayAudioClipTest(13);
                        Invoke("RechargeUPOver", 10f);
                    }
                    else
                    {
                        print("46充电完成");
                        PlayerAudio.Instance.PlayAudioClipTest(46);
                        Invoke("CDGAniDownOverSetHandTrue", 10f);
                        Invoke("CDGAniUpOver1", 7f);

                    }
                }
                else
                {
                    print("13充电结束后，充电弓自动上升，可通过监控或者下车查看充电弓是否上升成功");
                    PlayerAudio.Instance.PlayAudioClipTest(13);
                    Invoke("RechargeUPOver", 10f);
                }
               
                SetC2ChildFalse();
                RechargeOver_Image.gameObject.SetActive(true);
                RechargeOver_Image.transform.GetChild(0).gameObject.SetActive(true);
                RechargeOver_Image.transform.GetChild(1).gameObject.SetActive(false);

                BG_Image0.SetActive(true);
                BG_Image1.SetActive(false);
                
                vr_cartoon_hand_prefab_Left.SetActive(false);
                vr_cartoon_hand_prefab_Left.transform.parent.GetComponent<MeshRenderer>().enabled = true;
            }
        }
        if (vr_cartoon_hand_prefab_Left.activeInHierarchy == false)
        {
            if (Pvr_UnitySDKAPI.Controller.UPvr_GetKeyDown(0, Pvr_KeyCode.TRIGGER)||Input.GetKeyDown(KeyCode.L))
            {
                if (L_Ray.activeInHierarchy == false)
                {
                    RayManager.Instance.L_RayName = "lena";
                    rayParent_L.transform.GetComponent<Ray_L>().enabled = true;
                    L_Ray.SetActive(true);
                    L_StartRayPos.SetActive(true);
                    L_Sphere.SetActive(true);

                    rayParent_R.transform.GetComponent<Ray_R>().enabled = false;
                    R_Ray.SetActive(false);
                    R_StartRayPos.SetActive(false);
                    R_Sphere.SetActive(false);
                }
            }
        }
        else
        {
            rayParent_L.transform.GetComponent<Ray_L>().enabled = false;
            L_Ray.SetActive(false);
            L_StartRayPos.SetActive(false);
            L_Sphere.SetActive(false);
            RayManager.Instance.L_RayName = "lfalse";
        }

        if (vr_cartoon_hand_prefab_Right.activeInHierarchy == false)
        {
            if (Pvr_UnitySDKAPI.Controller.UPvr_GetKeyDown(1, Pvr_KeyCode.TRIGGER) || Input.GetKeyDown(KeyCode.N))
            {
                if (R_Ray.activeInHierarchy == false)
                {
                    RayManager.Instance.R_RayName = "rena";
                    rayParent_R.transform.GetComponent<Ray_R>().enabled = true;
                    R_Ray.SetActive(true);
                    R_StartRayPos.SetActive(true);
                    R_Sphere.SetActive(true);

                    rayParent_L.transform.GetComponent<Ray_L>().enabled = false;
                    L_Ray.SetActive(false);
                    L_StartRayPos.SetActive(false);
                    L_Sphere.SetActive(false);
                }
            }
        }
        else
        {
            rayParent_R.transform.GetComponent<Ray_R>().enabled = false;
            R_Ray.SetActive(false);
            R_StartRayPos.SetActive(false);
            R_Sphere.SetActive(false);
            RayManager.Instance.R_RayName = "rfalse";
        }

        if (IsStarAKeyToStartt)
        {
            if (StarAKeyToStarttTime<=100f)
            {
                StarAKeyToStarttTime += Time.deltaTime*10;
                Percentage_Text.text = ((int)StarAKeyToStarttTime).ToString()+"%";

                if (StarAKeyToStarttTime < 60f)
                {
                    Duration_Time.text = "0h:" + StarAKeyToStarttTime.ToString() + "m";
                }
                else
                {
                    Duration_Time.text = "1h:" + (StarAKeyToStarttTime - 60f).ToString() + "m";
                }
            }
            else
            {
                StarAKeyToStartCDOver();
            }
        }
    }

    /// <summary>
    /// 关闭所有车内的触摸屏按钮
    /// </summary>
    public void SetC2ChildFalse()
    {
        for (int i = 0; i < AKeyToStart_Button.transform.parent.transform.childCount; i++)
        {
            AKeyToStart_Button.transform.parent.GetChild(i).gameObject.SetActive(false);
        }
    }
    /// <summary>
    /// 设置提示面板文字
    /// </summary>
    /// <param name="s"></param>
    public void SetPlayerHint_Canvas_Test(string s)
    {
        PlayerHint_Text.text = s;
    }
    /// <summary>
    /// 转场动画 fromValue = 1 toValue= 0  开灯 else 关灯
    /// </summary>
    /// <param name="fromValue">开始值</param>
    /// <param name="toValue">结束值</param>
    /// <param name="duration">时间</param>
    public void SetCutTo(float fromValue, float toValue, float duration)
    {
        print("1111  "+fromValue +"  "+toValue +"  "+duration);
        Color temColor = CutTo_Image.color;
        temColor.a = fromValue;
        Tweener tweener = DOTween.ToAlpha(() => temColor, x => temColor = x, toValue, duration);
        tweener.onUpdate = () => { CutTo_Image.color = temColor; };
        //tweener.onComplete = () =>
        //{
        //    SetCutTo(toValue, fromValue, duration);
        //};
    }
    /// <summary>
    /// 设置高光物体
    /// </summary>
    /// <param name="go"></param>
    /// <param name="b"></param>
    public void SetHight(GameObject go,bool b)
    {
        go.GetComponent<HighlightEffect>().highlighted = b;
    }
    /// <summary>
    /// 色湖之手柄震动
    /// </summary>
    /// <param name="hand">哪只手0 1</param>
    /// <param name="time">时间（毫秒）</param>
    /// <param name="strength">震动强度0-1</param>
    public void VibateController(int hand, int time, float strength)
    {
        Pvr_UnitySDKAPI.Controller.UPvr_VibrateNeo2Controller(strength, time, hand);
        if (hand == 0)
        {
            print("左手柄震动" + transform.name);
        }
        if (hand == 1)
        {
            print("右手柄震动" + transform.name);
        }
    }
    public void SetIsDownBus()
    {
        IsDownBus = false;
    }




    #region 充电枪培训模式

    /// <summary>
    /// 设置充电枪初始的值
    /// </summary>
    public void SetCDQStartValue()
    {
        SetCancelInvoke();
        RayManager.Instance.L_RayName = "lCDQmodel";
        RayManager.Instance.R_RayName = "rCDQmodel";

        LeftYellowLight.DisableKeyword("_EMISSION");
        RightYellowLight.DisableKeyword("_EMISSION");
        LeftGreenLight.DisableKeyword("_EMISSION");
        RightGreenLight.DisableKeyword("_EMISSION");

        SetCDQ_CDD(false, LeftYellowLight);
        SetCDQ_CDD(false, LeftGreenLight);
        SetCDQ_CDD(false, RightYellowLight);
        SetCDQ_CDD(false, RightGreenLight);

        //设置集控台的界面
        CDQ_JiKongTai_Btn_Parent.SetActive(true);
        CDQ_JiKongTai_CDXX_Parent.SetActive(false);
        //关闭充电枪 AB结束按键
        CDQB_arrow.SetActive(false);
        CDQA_arrow.SetActive(false);
        //设置充电枪AB还没有充过电
        CDQAIsCDLe = false;
        CDQBIsCDLe = false;
        //充电枪是否已经插进去了一个 是的话播放提示语音
        IsChaQiangAudioHint = false;
        //设置充电枪在哪里 
        CDQ_Che_A = false;
        CDQ_Che_B = false;
        CDQ_XuanBi_A = true;
        CDQ_XuanBi_B = true;
        //关闭充电枪按键高亮
        SetHight(XuanBi_GunButtoonB, false);
        SetHight(XuanBi_GunButtoonA, false);
        //关闭挡位钥匙手刹充电门高亮
        SetHight(chargeDoor.transform.GetChild(0).transform.gameObject, false);
        SetHight(DangWei_N_GameObject.gameObject, false);
        SetHight(YaoShi.transform.gameObject, false);
        SetHight(ShouSha001.transform.gameObject, false);
        //隐藏玩家提示的面板
        PlayerHint_Canvas.SetActive(false);
        //充电枪A B是否充电设置false
        CDQAIsCD = false;
        CDQBIsCD = false;
        //打开充电弓摄像头 关闭轮胎摄像头
        //RawImage_Camera0.SetActive(true);
        //RawImage_Camera1.SetActive(false);

        //关闭钥匙 手刹 挡位
        if (IsYaoShi == false)
        {
            CloseYaoShi(false, 0f);
        }
        if (IsShouSha)
        {
            CloseShouSha(false, 0f);
        }
        DangWei_N = false;

        //关闭充电门
        if (IschargeDoor == false)
        {
            Instance.IschargeDoor = true;
            Instance.DOTweenRotate(chargeDoor.transform.gameObject, new Vector3(0f, 180f, 0f), 0f, true);
        }

        //设置充电枪父物体位置旋转
        CDQ_A.transform.parent = GameObject.Find("XuanBi_GunL_A_Parent").transform;
        CDQ_B.transform.parent = GameObject.Find("XuanBi_GunL_B_Parent").transform;

        CDQ_A.transform.localEulerAngles = new Vector3(0f, 0f, 0f);
        CDQ_B.transform.localEulerAngles = new Vector3(0f, 0f, 0f);

        CDQ_A.transform.localPosition = Vector3.zero;
        CDQ_B.transform.localPosition = Vector3.zero;

        Canvas1.SetActive(true);
    }

    public void CDQGameOver()
    {
        Player.transform.GetChild(0).transform.name = "00";
        Invoke("CDQToJJTZZC0", 2f);
    }
    /// <summary>
    /// 充电枪紧急停止转场
    /// </summary>
    private void CDQToJJTZZC0()
    {

        SetCutTo(0f, 1f, 2f);
        Invoke("SetShengZi", 2f);
    }

    private void SetShengZi()
    {
        CDQ_A.transform.parent = Che_B_Parent.transform;
        CDQ_B.transform.parent = Che_A_Parent.transform;

        CDQ_A.transform.localPosition = Vector3.zero;
        CDQ_B.transform.localPosition = Vector3.zero;

        CDQ_A.transform.localEulerAngles = new Vector3(0f, 0f, 90f);
        CDQ_B.transform.localEulerAngles = new Vector3(0f, 0f, 90f);


        SetCDQ_CDD(true, LeftYellowLight);
        SetCDQ_CDD(true, LeftGreenLight);
        SetCDQ_CDD(true, RightYellowLight);
        SetCDQ_CDD(true, RightGreenLight);

        DOTweenRotate(chargeDoor, new Vector3(0f, -180f, 0f), 0f, true);


        CDQ_JJTZ_L_arrow.SetActive(true);
        CDQ_JJTZ_R_arrow.SetActive(true);
        SetHight(XuanBi_OringeBuL, true);
        SetHight(XuanBi_OringeBuR, true);

        Invoke("CDQToJJTZZC1", 2f);
    }

    /// <summary>
    /// 充电枪注意事项1
    /// </summary>
    private void CDQToJJTZZC1()
    {
        PlayerHint_Canvas.SetActive(true);
        PlayerHint_Text.text = "注意事项";
        print("47滋滋的电流声音效_爱给网_aigei_com");
        PlayerAudio.Instance.PlayCDQDL();
        LavaSparks03_coll_L.Play();

        Che_A_Parent.transform.GetChild(0).GetComponent<MeshCollider>().enabled = false;
        CDQ_B.transform.Find("XuanBi_GunBody").GetComponent<MeshCollider>().enabled = false;
        CDQ_B.transform.Find("XuanBi_GunL_B_Body").GetComponent<MeshCollider>().enabled = false;

        SetCutTo(1f, 0f, 2f);
        Invoke("CDQModel_BusCatchFire", 3f);
    }
    /// <summary>
    /// 充电枪注意事项1语音
    /// </summary>
    private void CDQModel_BusCatchFire()
    {
        PlayerHint_Text.text = "一 车辆着火\n请立即按下充电枪终端上紧急停止按钮";
        print("55注意事项一 车辆着火，请立即按下充电枪终端上紧急停止按钮");
        PlayerAudio.Instance.PlayAudioClipTest(55);
        Player.transform.GetChild(0).transform.name = "22";
    }
    /// <summary>
    /// 充电枪紧急停止按钮
    /// </summary>
    public void CDQ_XuanBi_OringeBu()
    {

        Che_A_Parent.transform.GetChild(0).GetComponent<MeshCollider>().enabled = true;
        CDQ_B.transform.Find("XuanBi_GunBody").GetComponent<MeshCollider>().enabled = true;
        CDQ_B.transform.Find("XuanBi_GunL_B_Body").GetComponent<MeshCollider>().enabled = true;

        PlayerAudio.Instance.StopDL();
        LavaSparks03_coll_L.Stop();

        SetCDQ_CDD(false, LeftYellowLight);
        SetCDQ_CDD(false, LeftGreenLight);
        SetCDQ_CDD(false, RightYellowLight);
        SetCDQ_CDD(false, RightGreenLight);
        //XuanBi_OringeBuL.transform.parent.transform.localPosition = Vector3.zero;
        DOTweenMove(XuanBi_OringeBuL.transform.parent.gameObject, Vector3.zero, 1f);
        SetHight(XuanBi_OringeBuL, false);
        SetHight(XuanBi_OringeBuR, false);

        PlayerHint_Text.text = "也可以在车载屏幕端结束充电或集控屏进行断电";
        print("56也可以在车载屏幕端结束充电或集控屏进行断电");
        PlayerAudio.Instance.PlayAudioClipTest(56);
        Invoke("CDQToJJTZZC2", 6f);
    }


    private void CDQToJJTZZC2()
    {
        SetCutTo(0f, 1f, 2f);
        Invoke("CDQToJJTZZC3", 2f);
    }
    /// <summary>
    /// 充电枪注意事项2
    /// </summary>
    private void CDQToJJTZZC3()
    {
        SetCutTo(1f, 0f, 2f);

        arrowRotation_R.SetActive(true);
        arrowRotation_L.SetActive(true);

        SetHight(XuanBi_OringeBuL, true);
        SetHight(XuanBi_OringeBuR, true);
        Player.transform.GetChild(0).transform.name = "33";
        PlayerHint_Canvas.SetActive(true);
        PlayerHint_Text.text = "二 若充电枪充电失败\n请检查紧急停止按钮是否被按下";
        print("57注意事项二，若充电枪充电失败，请检查紧急停止按钮是否被按下");
        PlayerAudio.Instance.PlayAudioClipTest(57);
    }
    /// <summary>
    /// 充电枪紧急停止按钮
    /// </summary>
    public void CDQ_XuanBi_OringeBu1()
    {
        arrowRotation_R.SetActive(false);
        arrowRotation_L.SetActive(false);

        SetCDQ_CDD(true, LeftYellowLight);
        SetCDQ_CDD(true, LeftGreenLight);
        SetCDQ_CDD(true, RightYellowLight);
        SetCDQ_CDD(true, RightGreenLight);
        //XuanBi_OringeBuL.transform.parent.transform.localPosition = new Vector3(0f, -0.027f,0f);
        DOTweenMove(XuanBi_OringeBuL.transform.parent.gameObject, new Vector3(0f, -0.027f, 0f), 1f);

        SetHight(XuanBi_OringeBuL, false);
        SetHight(XuanBi_OringeBuR, false);
        Player.transform.GetChild(0).transform.name = "44";
        PlayerHint_Canvas.SetActive(false);
        Invoke("CDQGameOver1", 2f);
    }

    #endregion



    #region 充电枪培训

    /// <summary>
    /// 关闭车充电口门
    /// </summary>
    public void IsOverDoor()
    {
        if (CDQ_XuanBi_A && CDQ_XuanBi_B && CDQAIsCDLe && CDQBIsCDLe)
        {
            SetHight(chargeDoor.transform.GetChild(0).transform.gameObject, true);
            print("22请关闭车上充电口舱门");
            PlayerAudio.Instance.PlayAudioClipTest(22);
        }
    }

    public void SetXuanBi_RedBu_Ani(bool b)
    {
        if (b)
        {
            XuanBi_RedBuL1.transform.DOLocalMove(PlayerPosition.XuanBi_RedBuL1_StopV3, 1f).OnComplete(SetXuanBi_RedBuL_Ani);
        }
        else
        {
            XuanBi_RedBuR1.transform.DOLocalMove(PlayerPosition.XuanBi_RedBuR1_StopV3, 1f).OnComplete(SetXuanBi_RedBuR_Ani);
        }
    }
    private void SetXuanBi_RedBuL_Ani()
    {
        XuanBi_RedBuL1.transform.DOLocalMove(PlayerPosition.XuanBi_RedBuL1_StartV3, 1f);
    }
    private void SetXuanBi_RedBuR_Ani()
    {
        XuanBi_RedBuR1.transform.DOLocalMove(PlayerPosition.XuanBi_RedBuR1_StartV3, 1f);
    }

    public void Set_CDQ_CancelInvoke()
    {
        //CancelInvoke("DengHit");
        //CancelInvoke("CDQ_CDZ_AudioHint");
        //CancelInvoke("ChongDianZhongAudioHint");
        CancelInvoke("JieShuAudioHint");
        CancelInvoke("PlayJKPAudio");
        CancelInvoke("playMeiQuJkpAudio");
       CancelInvoke("CDQZiDongJieShuChongDian");
    }

    /// <summary>
    /// 充电灯语音提示23插枪完成后。。。
    /// </summary>
    public void DengHit()
    {
        PlayerHint_Canvas.SetActive(false);
        if (IsChaQiangAudioHint == false)
        {
            IsChaQiangAudioHint = true;
        }
        if (CDQ_Che_A || CDQ_Che_B)
        {
            Invoke("Play15sChaQiang", 15);
        }

        if (CDQ_Che_A && CDQ_Che_B)
        {
            CancelInvoke("Play15sChaQiang");
            print("23插枪完成后，观察充电终端上插枪充电指示灯，插枪后黄灯常亮表示插枪成功，绿灯常亮，表示正在充电中");
            PlayerAudio.Instance.PlayAudioClipTest(23);

            Invoke("CDQ_CDZ_AudioHint", 14f);
        }
    }
    private void Play15sChaQiang()
    {
        print("58请在15秒内，完成另一把充电枪的插枪操作");
        PlayerAudio.Instance.PlayAudioClipTest(58);
    }

    /// <summary>
    /// 判断是否播放充电中语音
    /// </summary>
    public void CDQ_CDZ_AudioHint()
    {
        print(CDQ_Che_A + " " + CDQ_Che_B);
        if (CDQ_Che_A && CDQ_Che_B)
        {
            Invoke("ChongDianZhongAudioHint", 1f);
        }
        else
        {
            CancelInvoke("ChongDianZhongAudioHint");
        }
    }

    /// <summary>
    /// 语音34充电过程中。。。-PlayJKPAudio
    /// </summary>
    public void ChongDianZhongAudioHint()
    {
        SetCDQ_CDD(true, LeftGreenLight);
        SetCDQ_CDD(true, RightGreenLight);
        CDQAIsCDLe = true;
        CDQBIsCDLe = true;
        CDQAIsCD = true;
        CDQBIsCD = true;
        print("25充电中，请等待");
        PlayerAudio.Instance.PlayAudioClipTest(25);
        Invoke("JieShuAudioHint", 5f);
    }
    
    /// <summary>
    /// 集控屏语音
    /// </summary>
    public void JieShuAudioHint()
    {
        print("34充电过程中可以通过集控屏查看充电信息，或者结束充电");
        PlayerAudio.Instance.PlayAudioClipTest(34);
        Invoke("PlayJKPAudio", 7f);
    }
    
    /// <summary>
    /// XY面板提示 并显示手模型 - playMeiQuJkpAudio
    /// </summary>
    public void PlayJKPAudio()
    {
        PlayerHint_Canvas.SetActive(true);
        PlayerHint_Text.text = "按左手手柄Y键可以查看集控屏，按左手手柄上的X键返回原地";

        vr_cartoon_hand_prefab_Left.SetActive(false);
        vr_cartoon_hand_prefab_Left.transform.parent.GetComponent<MeshRenderer>().enabled = true;

        Invoke("playMeiQuJkpAudio", 10f);
    }

    /// <summary>
    /// 显示手模型 并显示结束按钮提示箭头
    /// </summary>
    public void playMeiQuJkpAudio()
    {
        vr_cartoon_hand_prefab_Left.SetActive(true);
        vr_cartoon_hand_prefab_Left.transform.parent.GetComponent<MeshRenderer>().enabled = false;

        IsGoJKP = true;

        if (CDQAIsCD)
        {
            CDQA_arrow.SetActive(true);
        }
        if (CDQBIsCD)
        {
            CDQB_arrow.SetActive(true);
        }

        PlayerHint_Canvas.SetActive(false);
        print("21按充电桩上的结束复位按钮，可以使充电枪提前结束充电。结束充电后，请将充电枪依次插回充电枪终端");
        PlayerAudio.Instance.PlayAudioClipTest(21);
        Invoke("CDQZiDongJieShuChongDian", 20f);
    }

    public void CDQZiDongJieShuChongDian()
    {
        print("60充电完成，请将充电枪依次插回充电终端");
        PlayerAudio.Instance.PlayAudioClipTest(60);

        PlayerHint_Canvas.SetActive(false);
        CDQBIsCD = false;
        CDQAIsCD = false;

        SetCDQ_CDD(false, RightYellowLight);
        SetCDQ_CDD(false, RightGreenLight);
        SetCDQ_CDD(false, LeftYellowLight);
        SetCDQ_CDD(false, LeftGreenLight);

        CDQB_arrow.SetActive(false);
        CDQA_arrow.SetActive(false);
        SetHight(XuanBi_GunButtoonB, true);
        SetHight(XuanBi_GunButtoonA, true);
    }

    /// <summary>
    /// 亮灯
    /// </summary>
    /// <param name="b"></param>
    /// <param name="m"></param>
    public void SetCDQ_CDD(bool b, Material m)
    {
        if (b)
        {
            m.EnableKeyword("_EMISSION");
        }
        else
        {
            m.DisableKeyword("_EMISSION");
        }
    }

    #endregion



    #region 充电枪考试
    /// <summary>
    /// 设置充电枪动画
    /// </summary>
    /// <param name="hand">哪只手 0 1</param>
    /// <param name="HandVr">充电枪上的手模型</param>
    /// <param name="End">true 播放拿枪动画 false 取消拿枪</param>
    public void SetCDQHeadAni_Enter(GameObject HandVr, bool End, int hand)
    {
        if (End)
        {
            HandVr.transform.GetComponent<Animator>().SetBool("IsHold", true);
            HandVr.transform.GetComponent<Animator>().SetBool("IsPine", false);
            HandVr.transform.GetChild(0).transform.gameObject.SetActive(true);
            HandVr.transform.GetChild(1).transform.gameObject.SetActive(true);
            Play_L_Hand_Mesh.SetActive(false);
            Play_R_Hand_Mesh.SetActive(false);
        }
        else
        {
            PlayerAudio.Instance.PlayBtnAudio();
            VibateController(hand, 100, 1);

            HandVr.transform.GetComponent<Animator>().SetBool("IsHold", false);
            HandVr.transform.GetComponent<Animator>().SetBool("IsPine", true);
            HandVr.transform.GetChild(0).transform.gameObject.SetActive(false);
            HandVr.transform.GetChild(1).transform.gameObject.SetActive(false);
            Play_L_Hand_Mesh.SetActive(true);
            Play_R_Hand_Mesh.SetActive(true);
        }
    }
    /// <summary>
    /// 设置考试模式充电枪火花的播放暂停
    /// </summary>
    /// <param name="b"></param>
    public void SetTestModelCDQHuoHua(bool b)
    {
        if (b)
        {
            LavaSparks03_coll_L.Play();
        }
        else
        {
            LavaSparks03_coll_L.Stop();
        }
    }
    /// <summary>
    /// 充电枪集控屏返回声音
    /// </summary>
    public void CDQJKPReturnAudio()
    {
        if (CDQAIsCD == false && CDQBIsCD == false)
        {
            PlayerHint_Canvas.SetActive(true);
            PlayerHint_Text.text = "按左手手柄Y键可以查看集控屏，按左手手柄上的X键返回原地";
            if (!IsTest || IsTestFalse)
            {
                print("45请返回充电舱门口拔下充电枪并且插回原位");
                PlayerAudio.Instance.PlayAudioClipTest(45);
            }
            
        }
    }
    /// <summary>
    /// 手模型的延时显示
    /// </summary>
    /// <param name="b">b=true 延时显示手模型 false 取消延时显示手模型</param>
    /// <param name="time">延时时间</param>
    public void CDGAniDownOverSetHandTrue(bool b, float time)
    {
        if (b)
        {
            Invoke("CDGAniDownOverSetHandTrueInvoke", time);
        }
        else
        {
            CancelInvoke("CDGAniDownOverSetHandTrueInvoke");
        }
    }
    /// <summary>
    /// 充电弓上升动画结束设置手模型显示
    /// </summary>
    private void CDGAniDownOverSetHandTrueInvoke()
    {
        vr_cartoon_hand_prefab_Left.gameObject.SetActive(true);
        vr_cartoon_hand_prefab_Left.transform.parent.GetComponent<MeshRenderer>().enabled = false;
    }
    /// <summary>
    /// 设置 手柄 和 手模型 的显示
    /// </summary>
    /// <param name="SetVRHandTure"></param>
    public void SetHandModel(bool SetVRHandTure)
    {
        if (SetVRHandTure)
        {
            vr_cartoon_hand_prefab_Left.SetActive(true);
            vr_cartoon_hand_prefab_Right.SetActive(true);

            vr_cartoon_hand_prefab_Left.transform.parent.GetComponent<MeshRenderer>().enabled = false;
            vr_cartoon_hand_prefab_Right.transform.parent.GetComponent<MeshRenderer>().enabled = false;

            //R_Hand_Dot.enabled = false;
            //R_Hand_Ray.enabled = false;
            //L_Hand_Dot.enabled = false;
            //L_Hand_Ray.enabled = false;
        }
        else
        {
            vr_cartoon_hand_prefab_Left.SetActive(false);
            vr_cartoon_hand_prefab_Right.SetActive(false);

            vr_cartoon_hand_prefab_Left.transform.parent.GetComponent<MeshRenderer>().enabled = true;
            vr_cartoon_hand_prefab_Right.transform.parent.GetComponent<MeshRenderer>().enabled = true;


            rayParent_L.transform.GetComponent<Ray_L>().enabled = false;
            L_Ray.SetActive(false);
            L_StartRayPos.SetActive(false);
            L_Sphere.SetActive(false);

            rayParent_R.transform.GetComponent<Ray_R>().enabled = true;
            R_Ray.SetActive(true);
            R_StartRayPos.SetActive(true);
            R_Sphere.SetActive(true);
        }
    }
    /// <summary>
    /// 充电枪充电结束提示语音
    /// </summary>
    /// <param name="b"></param>
    public void SetCDQ_CD_OverAudio(bool b)
    {
        if (b)
        {
            print("准备充电了");
            Invoke("ChongDianZhongAudioHint1", 6f);
            Invoke("CDQ_CD_OverAudio", 21f);
        }
        else
        {
            CancelInvoke("ChongDianZhongAudioHint1");
            CancelInvoke("CDQ_CD_OverAudio");
        }
    }
    /// <summary>
    /// 充电枪充电完成语音
    /// </summary>
    private void CDQ_CD_OverAudio()
    {
        print("46充电完成");
        PlayerAudio.Instance.PlayAudioClipTest(46);
        vr_cartoon_hand_prefab_Left.SetActive(true);
        vr_cartoon_hand_prefab_Left.transform.parent.GetComponent<MeshRenderer>().enabled = false;

        SetCDQ_CD_OverAudio(false);

        PlayerHint_Canvas.SetActive(false);
        CDQBIsCD = false;
        CDQAIsCD = false;

        SetCDQ_CDD(false, RightYellowLight);
        SetCDQ_CDD(false, RightGreenLight);
        SetCDQ_CDD(false, LeftYellowLight);
        SetCDQ_CDD(false, LeftGreenLight);

        CDQ_JiKongTai_Btn_Parent.SetActive(true);
        CDQ_JiKongTai_CDXX_Parent.SetActive(false);
        CDQ_309_KXZ.SetActive(true);
        CDQ_309_CDZ.SetActive(false);
        CDQ_310_KXZ.SetActive(true);
        CDQ_310_CDZ.SetActive(false);
        CDQ_309_CDZ.transform.parent.transform.GetChild(2).GetComponent<Text>().text = "309号直流";
        CDQ_310_CDZ.transform.parent.transform.GetChild(2).GetComponent<Text>().text = "310号直流";
    }
    /// <summary>
    /// 充电中声音提示
    /// </summary>
    public void ChongDianZhongAudioHint1()
    {
        CDQAIsCDLe = true;
        CDQBIsCDLe = true;
        SetCDQ_CDD(true, LeftGreenLight);
        SetCDQ_CDD(true, RightGreenLight);
        print("25充电中，请等待");
        PlayerAudio.Instance.PlayAudioClipTest(25);
        vr_cartoon_hand_prefab_Left.SetActive(false);
        vr_cartoon_hand_prefab_Left.transform.parent.GetComponent<MeshRenderer>().enabled = true;
    }

    public void Is_CDQ_15s()
    {
        IsTestFalse = true;
        PlayerHint_Canvas.SetActive(true);
        PlayerHint_Text.text = "双枪插入间隔超过15秒\n测验失败 请重新培训后考试";
        Invoke("SetCutTo0", 5f);
    }
    /// <summary>
    /// 转场
    /// </summary>
    public void SetCutTo0()
    {
        //关灯
        SetCutTo(0f, 1f, 2f);
        Invoke("CDQ_TestModelToTrainingModel", 2f);
    }
    /// <summary>
    /// 充电枪测试失败重新培训
    /// </summary>
    private void CDQ_TestModelToTrainingModel()
    {
        PlayerAudio.Instance.StopAudio();
        PlayerAudio.Instance.StopDL();

        Che_A_Parent.transform.GetChild(0).GetComponent<MeshCollider>().enabled = true;
        CDQ_B.transform.Find("XuanBi_GunBody").GetComponent<MeshCollider>().enabled = true;
        CDQ_B.transform.Find("XuanBi_GunL_B_Body").GetComponent<MeshCollider>().enabled = true;
        LavaSparks03_coll_L.Stop();

        SetTestModelCDQHuoHua(false);
        PlayerHint_Canvas.SetActive(false);

        print("Add 1");
        vr_cartoon_hand_prefab_Left.AddComponent<LeftShouTrigger>();
        vr_cartoon_hand_prefab_Right.AddComponent<RightShouTrigger>();

        SetObi(false);
        Destroy(vr_cartoon_hand_prefab_Left.transform.GetComponent<Left_CDQ_Test_Model>());
        Destroy(vr_cartoon_hand_prefab_Right.transform.GetComponent<Right_CDQ_Test_Model>());
    }

    #endregion



    #region 充电弓培训模式


    /// <summary>
    /// 充电弓上升结束信息面板
    /// </summary>
    public void RechargeUPOver()
    {
        if (IsTest == false || IsTestFalse)
        {

            PlayerHint_Canvas.SetActive(true);
            PlayerHint_Text.text = "用左手手柄射线点击监控上方的确认按钮，或者按左手手柄上的Y键下车查看，按X键回到车内，确认充电弓已上升，然后再移车";


            print("14充电弓上升完成，界面显示充电报告信息");
            PlayerAudio.Instance.PlayAudioClipTest(14);
        }

        CDGJL_arrow.SetActive(false);
        SetC2ChildFalse();


        RechargeOverHint_Image.gameObject.SetActive(true);
        RechargeOverHint_Image.transform.GetChild(0).GetComponent<Text>().text = ((int)RechargeTime).ToString() + "%";
        if (Player.transform.localPosition.x > -26f)
        {
            CDGSS_Over_Button.gameObject.SetActive(true);
            rayParent_L.transform.GetComponent<Ray_L>().enabled = true;
            L_Ray.SetActive(true);
            L_StartRayPos.SetActive(true);
            L_Sphere.SetActive(true);

            //SetHandModel(false);
            PlayerHint_Canvas.SetActive(true);
            PlayerHint_Text.text = "用左手手柄射线点击监控上方的确认按钮，或者按左手手柄上的Y键下车查看，按X键回到车内，确认充电弓已上升，然后再移车";
        }
        Player.transform.GetChild(0).transform.gameObject.name = "0";
    }
    /// <summary>
    /// 充电弓模式 上巴士 到注意事项
    /// </summary>
    public void CDG_UpBusToZYSX()
    {
        Invoke("SetCDGCanvasHint", 2f);
    }

    /// <summary>
    /// 判断是否考试模式
    /// </summary>
    public void SetCDGCanvasHint()
    {
        RechargeYes_Image.transform.GetChild(0).GetComponent<Text>().text = "";
        PlayerHint_Canvas.SetActive(false);
        Player.transform.GetChild(0).name = "cdg";
        SetCutTo(0f, 1f, 2f);
        Invoke("BusCatchFireHint", 2f);
    }
    /// <summary>
    /// 巴士着火提示 注意事项1
    /// </summary>
    private void BusCatchFireHint()
    {
        CDGSS_Over_Button1.gameObject.SetActive(false);

        PlayerHint_Canvas.SetActive(true);
        PlayerHint_Text.text = "注意事项";


        print("47滋滋的电流声音效_爱给网_aigei_com");
        PlayerAudio.Instance.PlayCDGDL();
        LavaSparks03_coll.Play();

        SetHandModel(true);
        Player.transform.localPosition = PlayerPosition.PlayCDGDownJJTZPos;
        Player.transform.localEulerAngles = PlayerPosition.PlayCDGDownJJTZRot;
        SetHight(CDGJJTZBtn, true);
        SetCutTo(1f, 0f, 2f);
        Invoke("PlayCDGDownOverRecharge", 2f);
    }
    /// <summary>
    /// 转场到充电弓下
    /// </summary>
    private void PlayCDGDownOverRecharge()
    {
        PlayerHint_Text.text = "一 车辆着火后\n请立即按下充电弓上的紧急停止按钮";
        Player.transform.GetChild(0).name = "cdg";
        print("52注意事项一、车辆着火后，请立即按下充电弓上的紧急停止按钮");
        PlayerAudio.Instance.PlayAudioClipTest(52);
    }
    /// <summary>
    /// 播放或者在车端结束充电或集控屏进行断电
    /// </summary>
    public void PlayBusJKPPosOutageAudio()
    {
        PlayerAudio.Instance.StopDL();
        LavaSparks03_coll.Stop();
        //CDGJJTZBtn.transform.localPosition = new Vector3(0.004524f, 0.0152645f, -0.000113f);
        DOTweenMove(CDGJJTZBtn, new Vector3(0.004524f, 0.0152645f, -0.000113f), 1f);

        //LeftCDGgreenLight.DisableKeyword("_EMISSION");
        RightCDRedLight.EnableKeyword("_EMISSION");
        LeftCDGgreenLight.DisableKeyword("_EMISSION");
        PlayerHint_Canvas.SetActive(true);
        PlayerHint_Text.text = "也可以在车载屏幕端结束充电或集控屏进行断电";
        print("51也可以在车载屏幕端结束充电或集控屏进行断电");
        PlayerAudio.Instance.PlayAudioClipTest(51);

        Invoke("CDGJJTZZC", 6f);
    }
    /// <summary>
    /// 充电弓紧急停止转场
    /// </summary>
    private void CDGJJTZZC()
    {
        SetCutTo(0f, 1f, 2f);
        Invoke("CDGJJTZZC1", 2f);
    }
    /// <summary>
    /// 注意事项2
    /// </summary>
    private void CDGJJTZZC1()
    {
        arrowRotation_CDG.SetActive(true);
        PlayerHint_Canvas.SetActive(true);
        PlayerHint_Text.text = "二 若充电弓充电失败\n请检查紧急停止按钮是否被按下";

        SetHight(CDGJJTZBtn, true);
        SetCutTo(1f, 0f, 2f);
        Invoke("RechargeFail", 2f);
    }
    /// <summary>
    /// 注意事项2 充电失败语音
    /// </summary>
    private void RechargeFail()
    {
        Player.transform.GetChild(0).name = "cdg2";
        print("53注意事项二，若充电弓充电失败，请检查紧急停止按钮是否被按下");
        PlayerAudio.Instance.PlayAudioClipTest(53);
    }

    public void CDGJJTZZC2()
    {
        arrowRotation_CDG.SetActive(false);
        //CDGJJTZBtn.transform.localPosition = new Vector3(0.00496f, 0.0152645f, -0.000113f);
        DOTweenMove(CDGJJTZBtn, new Vector3(0.00496f, 0.0152645f, -0.000113f), 1f);

        RightCDRedLight.DisableKeyword("_EMISSION");
        LeftCDGgreenLight.EnableKeyword("_EMISSION");

        SetCutTo(0f, 1f, 2f);
        Invoke("CDGJJTZZC3", 2f);
    }
    /// <summary>
    /// 注意事项3
    /// </summary>
    private void CDGJJTZZC3()
    {
        SetC2ChildFalse();
        RechargeAbnormalTermination.SetActive(true);

        PlayerHint_Canvas.SetActive(true);
        PlayerHint_Text.text = "三 充电弓充电异常结束后 充电车载屏上显示异常原因 请将屏幕拍照 然后报修";

        Player.transform.localEulerAngles = Vector3.zero;
        Player.transform.position = PlayerPosition.PlayerCDGBusPos;
        SetCutTo(1f, 0f, 2f);
        Invoke("CDGModel_GOUPBus", 2f);
    }
    /// <summary>
    /// 上车 注意事项3语音
    /// </summary>
    private void CDGModel_GOUPBus()
    {
        print("54注意事项三、充电弓充电异常结束后，充电车载屏上显示异常原因，请将屏幕拍照，然后报修");
        PlayerAudio.Instance.PlayAudioClipTest(54);

        Invoke("SetCDGCanvasHint1", 12f);
    }

    #endregion



    #region 充电弓一键充电
    /// <summary>
    /// 充电弓一键开启
    /// </summary>
    public void SetAKeyToStart()
    {
        CDG_AkeyDown();
    }

    private float StarAKeyToStarttTime = 0f;
    private bool IsStarAKeyToStartt = false;
    /// <summary>
    /// 充电弓一键下降
    /// </summary>
    private void CDG_AkeyDown()
    {
        print("59正常情况下，按下一键开启后充电弓自动下降，充电完成后充电弓会自动上升，请注意一定要通过监控或下车查看充电弓是否上升再移车");
        PlayerAudio.Instance.PlayAudioClipTest(59);
        CDGAniDown();
        SetC2ChildFalse();
        RechargeDown_Image.SetActive(true);
        Invoke("CDG_AkeyRecharge", 5f);
    }
    /// <summary>
    /// 充电弓一键充电
    /// </summary>
    private void CDG_AkeyRecharge()
    {
        CDGJL_arrow.SetActive(false);
        SetC2ChildFalse();
        TwoRecharge_Img.SetActive(true);
        TwoRecharge_Img.transform.GetChild(0).gameObject.SetActive(false);
        TwoRecharge_Img.transform.GetChild(1).gameObject.SetActive(true);
        Invoke("CDG_AkeyStaSucceed", 2f);
    }
    /// <summary>
    /// 启动充电成功
    /// </summary>
    private void CDG_AkeyStaSucceed()
    {
        SetC2ChildFalse();
        RechargeYes_Image.SetActive(true);
        Invoke("StarAKeyToStartt", 2f);
    }
    /// <summary>
    /// 充电信息面板
    /// </summary>
    private void StarAKeyToStartt()
    {
        StarAKeyToStarttTime = 0f;
        IsStarAKeyToStartt = true;
        SetC2ChildFalse();
        RechargeState_Image.SetActive(true);
        BG_Image0.SetActive(false);
        BG_Image1.SetActive(true);
        ATS_OverRecharge_Btn.SetActive(true);
    }

    /// <summary>
    /// 一键充电完毕
    /// </summary>
    public void StarAKeyToStartCDOver()
    {
        CDGJL_arrow.SetActive(true);
        print("46充电完成");
        PlayerAudio.Instance.PlayAudioClipTest(46);

        IsStarAKeyToStartt = false;
        CDGAniUP();
        SetC2ChildFalse();
        BG_Image0.SetActive(true);
        BG_Image1.SetActive(false);
        RechargeOver_Image.SetActive(true);
        Invoke("AKeyToStartCDOverPlane", 5f);
    }
    /// <summary>
    /// 一键充电结束
    /// </summary>
    private void AKeyToStartCDOverPlane()
    {
        CDGSS_Over_Button1.gameObject.SetActive(true);
        CDGJL_arrow.SetActive(false);
        SetC2ChildFalse();
        RechargeOverHint_Image.gameObject.SetActive(true);
        RechargeOverHint_Image.transform.GetChild(0).GetComponent<Text>().text = ((int)StarAKeyToStarttTime).ToString() + "%";

        if (Player.transform.localPosition.x > -26f)
        {
            vr_cartoon_hand_prefab_Left.SetActive(false);
            vr_cartoon_hand_prefab_Left.transform.parent.GetComponent<MeshRenderer>().enabled = true;
            rayParent_L.transform.GetComponent<Ray_L>().enabled = true;
            L_Ray.SetActive(true);
            L_StartRayPos.SetActive(true);
            L_Sphere.SetActive(true);
            CDGSS_Over_Button1.gameObject.SetActive(true);

            Player.transform.GetChild(0).gameObject.name = "cdg0";
            PlayerHint_Canvas.SetActive(true);
            print("用左手手柄射线点击监控上方的确认按钮，或者按左手手柄上的Y键下车查看，按X键回到车内，确认充电弓已上升，然后再移车");
            PlayerHint_Text.text = "用左手手柄射线点击监控上方的确认按钮，或者按左手手柄上的Y键下车查看，按X键回到车内，确认充电弓已上升，然后再移车";
        }
        else
        {
            Player.transform.GetChild(0).gameObject.name = "cdg1";
        }
    }
    /// <summary>
    /// 手动模式
    /// </summary>
    public void AKeyToStartOver()
    {
        SetC2ChildFalse();
        AKeyToStartFail_Btn.gameObject.SetActive(true);
        print("4在有些情况下“一键充电”如果失败，我们可以使用手动模式充电");
        PlayerAudio.Instance.PlayAudioClipTest(4);
    }

    #endregion



    #region 充电弓考试
    /// <summary>
    /// 充电弓上升结束
    /// </summary>
    /// 
    public void CDGAniUpOver1()
    {
        PlayerHint_Canvas.SetActive(false);
        RechargeUPOver1();
    }
    /// <summary>
    /// 充电弓上升结束 充电报告信息面板
    /// </summary>
    public void RechargeUPOver1()
    {
        print("14充电弓上升完成，界面显示充电报告信息");
        CDGJL_arrow.SetActive(false);
        
        SetC2ChildFalse();
        RechargeOverHint_Image.gameObject.SetActive(true);
        RechargeOverHint_Image.transform.GetChild(0).GetComponent<Text>().text = ((int)GameObjectIns.Instance.RechargeTime).ToString() + "%";
        //RechargeTime = 0f;
        Invoke("SetCDGCanvasHint11", 5f);
    }
    /// <summary>
    /// 色湖之充电弓模式结束提示
    /// </summary>
    private void SetCDGCanvasHint11()
    {
        vr_cartoon_hand_prefab_Left.gameObject.SetActive(true);
        vr_cartoon_hand_prefab_Left.transform.parent.GetComponent<MeshRenderer>().enabled = false;

        print("充电弓结束提示");
        PlayerHint_Canvas.gameObject.SetActive(true);
        PlayerHint_Text.text = "充电弓测验合格 即将测验充电枪模式";
        print("42充电弓考试完成，下面进入充电枪操作流程考试");
        PlayerAudio.Instance.PlayAudioClipTest(42);
        Invoke("CDGGameOverHint1", 11f);
    }
    /// <summary>
    /// 充电弓结束转场动画
    /// </summary>
    public void CDGGameOverHint1()
    {
        SetCutTo(0f, 1f, 1.6f);
        Invoke("CDGGameOver1", 1.6f);
    }
    /// <summary>
    /// 充电弓模式结束 开始充电枪模式
    /// </summary>
    private void CDGGameOver1()
    {
        SetCDGValue();

        vr_cartoon_hand_prefab_Left.AddComponent<Left_CDQ_Test_Model>();
        vr_cartoon_hand_prefab_Right.AddComponent<Right_CDQ_Test_Model>();

        SetRawImage(false);
        Destroy(vr_cartoon_hand_prefab_Left.transform.GetComponent<LeftShouCdgTrigger>());
        Destroy(vr_cartoon_hand_prefab_Right.transform.GetComponent<RightShouCdgTrigger>());
    }
    #endregion

}

public class PlayerPosition
{
    /// <summary>
    /// 用户充电弓下紧急停止位置
    /// </summary>
    public static Vector3 PlayCDGDownJJTZPos = new Vector3(-20.401f, 1.85f, -42.591f);
    /// <summary>
    /// 用户充电弓下紧急停止旋转
    /// </summary>
    public static Vector3 PlayCDGDownJJTZRot = SetRot.RotY180;
    /// <summary>
    /// 用户初始位置
    /// </summary>
    public static Vector3 PlayerStartPos = new Vector3(-30f, 1.85f, -33.08f);
    /// <summary>
    /// 用户充电弓巴士上的位置
    /// </summary>
    public static Vector3 PlayerCDGBusPos = new Vector3(-23.7f, 2.45f, -36.1f);
    /// <summary>
    /// 用户充电弓下巴士位置
    /// </summary>
    public static Vector3 PlayerCDGDownBusPos = new Vector3(-29.04f, 1.85f, -40.32f);
    /// <summary>
    /// 用户充电弓下巴士旋转
    /// </summary>
    public static Vector3 PlayerCDGDownBusRot = new Vector3(0f, 90f, 0f);
    /// <summary>
    /// 用户充电枪集控屏位置
    /// </summary>
    public static Vector3 PlayerCDQJKPPos = new Vector3(-21.53f, 1.85f, -29.83f);
    /// <summary>
    /// 用户充电枪悬臂位置
    /// </summary>
    public static Vector3 PlayerCDQXuanBiPos = new Vector3(-21.53f, 1.85f, -32.42f);
    /// <summary>
    /// 用户选择充电弓位置
    /// </summary>
    public static Vector3 PlayerSelectCDGPos = new Vector3(-24.3f, 1.85f, -45.43f);
    /// <summary>
    /// 用户充电弓移车位置
    /// </summary>
    public static Vector3 PlayerCDGYiChePos = new Vector3(-23.7f, 2.45f, -35.4f);

    /// <summary>
    /// 巴士初始位置
    /// </summary>
    public static Vector3 CityBuswithInteriorStartPos = new Vector3(-23f, 0.01f, -55f);
    /// <summary>
    /// 巴士充电弓位置
    /// </summary>
    public static Vector3 CityBuswithInteriorCDGPos = new Vector3(-23f, 0.01f, -40.7f);
    /// <summary>
    /// 巴士充电枪位置
    /// </summary>
    public static Vector3 CityBuswithInteriorCDQPos = new Vector3(-23.47f, 0.01f, -27.5f);
   /// <summary>
   /// 巴士充电弓移车位置
   /// </summary>
    public static Vector3 CityBuswithInteriorCDGYiChePos = new Vector3(-23f, 0.01f, -40f);

    public static Vector3 DangWeiN_StartV3 = new Vector3(-0.01097435f, 0.01566895f, 0.05122027f);

    public static Vector3 DangWeiN_StopV3 = new Vector3(-0.01097435f, 0.015613f, 0.05122027f);

    public static Vector3 XuanBi_RedBuL1_StartV3 = new Vector3(-0.149869f, -0.1442243f, 1.4599f);

    public static Vector3 XuanBi_RedBuR1_StartV3 = new Vector3(0.1502034f, -0.1422426f, 1.4599f);

    public static Vector3 XuanBi_RedBuL1_StopV3 = new Vector3(-0.149869f, -0.1362f, 1.4599f);

    public static Vector3 XuanBi_RedBuR1_StopV3 = new Vector3(0.1502034f, -0.1362f, 1.4599f);
}

public class SetRot
{
    public static Vector3 RotY180 = new Vector3(0f, 180f, 0f);
    public static Vector3 RotY_180 = new Vector3(0f, -180f, 0f);
    public static Vector3 RotX90 = new Vector3(90f, 0f, 0f);
    public static Vector3 RotZ90 = new Vector3(0f, 0f, 90f);
    public static Vector3 RotX87 = new Vector3(87f, 0f, 0f);
}