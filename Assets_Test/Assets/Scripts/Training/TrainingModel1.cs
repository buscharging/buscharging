﻿using Pvr_UnitySDKAPI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrainingModel1 : MonoBehaviour
{
    private Vector3 ShoushaStartRot;
    private Vector3 ShoushaStopRot;
    private Transform lastHit;
    // Start is called before the first frame update
    void Start()
    {
        GameObjectIns.Instance.AKeyToStart_Button.transform.GetChild(0).transform.GetChild(0).GetComponent<MeshRenderer>().enabled = false;
        CancelInvoke();
        GameObjectIns.Instance.SetCDQStartValue();
        GameObjectIns.Instance.SetCDGValue();
        //隐藏手模型 显示手柄模型 射线
        GameObjectIns.Instance.vr_cartoon_hand_prefab_Left.gameObject.SetActive(false);
        GameObjectIns.Instance.vr_cartoon_hand_prefab_Right.gameObject.SetActive(false);
        RayManager.Instance.L_RayName = "5";
        RayManager.Instance.R_RayName = "5";
        GameObjectIns.Instance.rayParent_L.transform.GetComponent<Ray_L>().enabled = false;
        GameObjectIns.Instance.L_Ray.SetActive(false);
        GameObjectIns.Instance.L_StartRayPos.SetActive(false);
        GameObjectIns.Instance.L_Sphere.SetActive(false);
        GameObjectIns.Instance.rayParent_R.transform.GetComponent<Ray_R>().enabled = true;
        GameObjectIns.Instance.R_Ray.SetActive(true);
        GameObjectIns.Instance.R_StartRayPos.SetActive(true);
        GameObjectIns.Instance.R_Sphere.SetActive(true);
        GameObjectIns.Instance.vr_cartoon_hand_prefab_Left.transform.parent.GetComponent<MeshRenderer>().enabled = true;
        GameObjectIns.Instance.vr_cartoon_hand_prefab_Right.transform.parent.GetComponent<MeshRenderer>().enabled = true;
        //取消高光手刹 挡位 钥匙
        GameObjectIns.Instance.SetHight(GameObjectIns.Instance.ShouSha001, false);
        GameObjectIns.Instance.SetHight(GameObjectIns.Instance.DangWei_N_GameObject, false);
        GameObjectIns.Instance.SetHight(GameObjectIns.Instance.YaoShi, false);
        GameObjectIns.Instance.SetCancelInvoke();
        GameObjectIns.Instance.PlayerHint_Canvas.SetActive(false);
        print("3请使用VR手柄选择充电类型");
        PlayerAudio.Instance.PlayAudioClipTest(2);
        GameObjectIns.Instance.SetCutTo(1f, 0f, 1.4f);
        GameObjectIns.Instance.Canvas1.SetActive(true);
        GameObjectIns.Instance.RechargeOver_Image.transform.GetChild(0).gameObject.SetActive(true);
        GameObjectIns.Instance.RechargeOver_Image.transform.GetChild(1).gameObject.SetActive(false);
        GameObjectIns.Instance.CityBuswithInterior.transform.position = PlayerPosition.CityBuswithInteriorCDGPos;
        //GameObjectIns.Instance.SetPlayerHint_Canvas_Test("请扣扳机选择充电类型");
        GameObjectIns.Instance.Player.transform.localEulerAngles = Vector3.zero;
        GameObjectIns.Instance.Player.transform.position = PlayerPosition.PlayerCDGBusPos;
    }
}
