﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ray_R : MonoBehaviour
{
    public LineRenderer line;
    public GameObject OriginPos;
    public GameObject surSor;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        RayDetectionObj();
    }
    private void RayDetectionObj()
    {
        line.SetPosition(0, OriginPos.transform.position);
        Ray ray = new Ray(OriginPos.transform.position, OriginPos.transform.forward);
        RaycastHit hitInfo;
        if (Physics.Raycast(ray, out hitInfo, 20))
        {
            line.SetPosition(1, hitInfo.point);
            surSor.transform.position = hitInfo.point;
            RayManager.Instance.R_RayName = hitInfo.transform.gameObject.name;
            surSor.SetActive( true);
        }
        else
        {
            line.SetPosition(1, OriginPos.transform.position + OriginPos.transform.forward * 10f);
            surSor.transform.position = OriginPos.transform.position + OriginPos.transform.forward * 10f;
            surSor.SetActive(false);
            RayManager.Instance.R_RayName = "r";
        }
    }
}
