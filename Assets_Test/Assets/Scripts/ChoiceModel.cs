﻿using Pvr_UnitySDKAPI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChoiceModel : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        GameObjectIns.Instance.DangWei_N_GameObject.transform.localPosition = PlayerPosition.DangWeiN_StartV3;
        GameObjectIns.Instance.CDGSS_Over_Button.gameObject.SetActive(false);
        GameObjectIns.Instance.CDGSS_Over_Button1.gameObject.SetActive(false);
        GameObjectIns.Instance.IsTestFalse = false;
        GameObjectIns.Instance.IsTest = false;
        CancelInvoke();
        GameObjectIns.Instance.Che_10_2m_Hint.transform.localPosition = new Vector3(-23f, 0.14f, -37.37f);
        //GameObjectIns.Instance.Che_6_5m_Hint.transform.localPosition = new Vector3(-22.58f, 0.14f, -39.52f);
        print("18欢迎您参加电动公交车充电VR培训课程，请在语音提示结束后根据相应的提示进行操作。请使用手柄指向视野前方的按钮然后扣下扳机，进入对应的流程");
        PlayerAudio.Instance.PlayAudioClipTest(18);
        GameObjectIns.Instance.SetCutTo(1f, 0f, 2f);
        GameObjectIns.Instance.SetPlayerStartValue();
        
        GameObjectIns.Instance.vr_cartoon_hand_prefab_Right.SetActive(false);
        GameObjectIns.Instance.vr_cartoon_hand_prefab_Left.SetActive(false);
        //重置射线碰撞名字
        RayManager.Instance.L_RayName = "1";
        RayManager.Instance.R_RayName = "1";
        //隐藏左手 左手射线 脚本
        GameObjectIns.Instance.vr_cartoon_hand_prefab_Left.transform.parent.GetComponent<MeshRenderer>().enabled = true;
        GameObjectIns.Instance.rayParent_L.transform.GetComponent<Ray_L>().enabled = false;
        GameObjectIns.Instance.L_Ray.SetActive(false);
        GameObjectIns.Instance.L_StartRayPos.SetActive(false);
        GameObjectIns.Instance.L_Sphere.SetActive(false);
        //隐藏 右手 右手射线 脚本
        GameObjectIns.Instance.vr_cartoon_hand_prefab_Right.transform.parent.GetComponent<MeshRenderer>().enabled = true;
        GameObjectIns.Instance.rayParent_R.transform.GetComponent<Ray_R>().enabled = true;
        GameObjectIns.Instance.R_Ray.SetActive(true);
        GameObjectIns.Instance.R_StartRayPos.SetActive(true);
        GameObjectIns.Instance.R_Sphere.SetActive(true);
    }
}
