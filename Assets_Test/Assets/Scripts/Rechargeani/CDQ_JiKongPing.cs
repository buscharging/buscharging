﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CDQ_JiKongPing : MonoBehaviour
{
    public GameObject CDQ_309_KXZ;
    public GameObject CDQ_309_CDZ;
    public GameObject CDQ_310_KXZ;
    public GameObject CDQ_310_CDZ;

    public GameObject CDQ_JiKongTai_Btn_Parent;
    public GameObject CDQ_JiKongTai_CDXX_Parent;
    // Start is called before the first frame update
    void Start()
    {
        CDQ_309_KXZ = transform.Find("CDQ_309_KXZ").gameObject;
        CDQ_309_CDZ = transform.Find("CDQ_309_CDZ").gameObject;
        CDQ_310_KXZ = transform.Find("CDQ_310_KXZ").gameObject;
        CDQ_310_CDZ = transform.Find("CDQ_310_CDZ").gameObject;
        CDQ_JiKongTai_Btn_Parent = transform.Find("CDQ_JiKongTai_Btn_Parent").gameObject;
        CDQ_JiKongTai_CDXX_Parent = transform.Find("CDQ_JiKongTai_CDXX_Parent").gameObject;

        CDQ_JiKongTai_CDXX_Parent.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (GameObjectIns.Instance.IsYaoShi)
        {
            if (GameObjectIns.Instance.CDQAIsCDLe)
            {
                CDQ_309_KXZ.SetActive(false);
                CDQ_309_CDZ.SetActive(true);
            }
            else
            {
                CDQ_309_KXZ.SetActive(true);
                CDQ_309_CDZ.SetActive(false);
            }
            if (GameObjectIns.Instance.CDQBIsCDLe)
            {
                CDQ_310_KXZ.SetActive(false);
                CDQ_310_CDZ.SetActive(true);
            }
            else
            {
                CDQ_310_KXZ.SetActive(true);
                CDQ_310_CDZ.SetActive(false);
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.name == "CDQ_309_Button")
        {
           
        }
        else if (other.name == "CDQ_310_Button")
        {

        }
    }

}
