﻿using Pvr_UnitySDKAPI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Left_CDQ_Test_Model : MonoBehaviour
{
    //public Quaternion CDQRot = Quaternion.Euler(0f, 0f, 90f);
    //public Quaternion CDQRot1 = Quaternion.Euler(0f, 0f, 0f);
    // Start is called before the first frame update

    /// <summary>
    /// 重置初始值
    /// </summary>
    void Start()
    {
        GameObjectIns.Instance.DangWei_N_GameObject.transform.localPosition = PlayerPosition.DangWeiN_StartV3;

        GameObjectIns.Instance.CDQ_JiKongTai_Btn_Parent.SetActive(true);
        GameObjectIns.Instance.CDQ_JiKongTai_CDXX_Parent.SetActive(false);
        GameObjectIns.Instance.CDQ_309_KXZ.SetActive(false);
        GameObjectIns.Instance.CDQ_309_CDZ.SetActive(true);
        GameObjectIns.Instance.CDQ_310_KXZ.SetActive(false);
        GameObjectIns.Instance.CDQ_310_CDZ.SetActive(true);
        GameObjectIns.Instance.CDQ_309_CDZ.transform.parent.transform.GetChild(2).GetComponent<Text>().text = "309号直流";
        GameObjectIns.Instance.CDQ_310_CDZ.transform.parent.transform.GetChild(2).GetComponent<Text>().text = "310号直流";
        GameObjectIns.Instance.SetCDQHeadAni_Enter(GameObjectIns.Instance.B_HandVR, false, 0);
        GameObjectIns.Instance.SetCDQHeadAni_Enter(GameObjectIns.Instance.A_HandVR, false, 0);
        GameObjectIns.Instance.SetHandModel(true);
        GameObjectIns.Instance.IsTestFalse = false;
        GameObjectIns.Instance.SetObi(true);

        GameObjectIns.Instance.IsCDGCDQ = 1;

        for (int g = 0; g < GameObjectIns.Instance.JianShuDaiParent.transform.childCount; g++)
        {
            GameObjectIns.Instance.SetHight(GameObjectIns.Instance.JianShuDaiParent.transform.GetChild(g).gameObject, false);
        }
        GameObjectIns.Instance.TestModel_Water_Plane.SetActive(false);

        GameObjectIns.Instance.XuanBi_GunButtoonB.transform.localEulerAngles = SetRot.RotX87;
        GameObjectIns.Instance.XuanBi_GunButtoonA.transform.localEulerAngles = SetRot.RotX87;

        CancelInvoke();
        GameObjectIns.Instance.PlayerHint_Canvas.gameObject.SetActive(false);

        GameObjectIns.Instance.SetCDQStartValue();

        GameObjectIns.Instance.SetCutTo(1f, 0f, 2f);
        GameObjectIns.Instance.SetArrowListFalst();

        GameObjectIns.Instance.Canvas1.SetActive(false);
        GameObjectIns.Instance.SetCancelInvoke();
        GameObjectIns.Instance.CityBuswithInterior.transform.position = PlayerPosition.CityBuswithInteriorCDGPos;
        //GameObjectIns.Instance.SetPlayerHint_Canvas_Test("请扣扳机选择充电类型");
        GameObjectIns.Instance.Player.transform.localEulerAngles = Vector3.zero;
        GameObjectIns.Instance.Player.transform.position = PlayerPosition.PlayerCDGBusPos;


        GameObjectIns.Instance.YiWu.SetActive(false);
        GameObjectIns.Instance.IsGoJKP = false;
        GameObjectIns.Instance.CDK_Door_arrow.SetActive(false);
        GameObjectIns.Instance.IsOverCDK_Door = true;

        GameObjectIns.Instance.CDQ_309_CDZ.transform.GetChild(2).transform.gameObject.SetActive(false);
        //GameObjectIns.Instance.CDQ_310_CDZ.transform.GetChild(2).transform.gameObject.SetActive(false);
        //GameObjectIns.Instance.CDQ_JiKongTai_CDXX_Parent.transform.GetChild(2).transform.GetChild(0).gameObject.SetActive(false);


        GameObjectIns.Instance.CDQBIsCDLe = false;
        GameObjectIns.Instance.CDQAIsCDLe = false;


        GameObjectIns.Instance.SetHight(GameObjectIns.Instance.ShouSha001, false);
        GameObjectIns.Instance.SetHight(GameObjectIns.Instance.YaoShi, false);
        GameObjectIns.Instance.SetHight(GameObjectIns.Instance.DangWei_N_GameObject, false);

    }
   
    /// <summary>
    /// 充电提示
    /// </summary>
    void DengHit()
    {
        if (GameObjectIns.Instance.CDQ_Che_A || GameObjectIns.Instance.CDQ_Che_B)
        {
            GameObjectIns.Instance.Invoke("Is_CDQ_15s", 15f);
        }
        if (GameObjectIns.Instance.CDQ_Che_A && GameObjectIns.Instance.CDQ_Che_B)
        {
            GameObjectIns.Instance.CancelInvoke("Is_CDQ_15s");

            GameObjectIns.Instance.CDQ_JiKongTai_Btn_Parent.SetActive(true);
            GameObjectIns.Instance.CDQ_JiKongTai_CDXX_Parent.SetActive(false);
            GameObjectIns.Instance.CDQ_309_KXZ.SetActive(false);
            GameObjectIns.Instance.CDQ_309_CDZ.SetActive(true);
            GameObjectIns.Instance.CDQ_310_KXZ.SetActive(false);
            GameObjectIns.Instance.CDQ_310_CDZ.SetActive(true);
            GameObjectIns.Instance.CDQ_Che_B = true;
            GameObjectIns.Instance.CDQ_Che_A = true;
            GameObjectIns.Instance.CDQAIsCD = true;
            GameObjectIns.Instance.CDQBIsCD = true;
            GameObjectIns.Instance.SetCDQ_CD_OverAudio(true);
            GameObjectIns.Instance.CDQ_309_CDZ.transform.parent.transform.GetChild(2).GetComponent<Text>().text = "组-309";
            GameObjectIns.Instance.CDQ_310_CDZ.transform.parent.transform.GetChild(2).GetComponent<Text>().text = "组-310";
        }
        else
        {
            GameObjectIns.Instance.SetCDQ_CD_OverAudio(false);
        }
    }

    private void OnTriggerEnter(Collider other)
    { 
        //充电枪考试模式如果点击一键启动按钮就考试失败
        if (other.name == "AKeyToStart_Button")
        {
            CancelInvoke("PlayJKPAudio");
            CancelInvoke("playMeiQuJkpAudio");
            CancelInvoke("ChongDianZhongAudioHint");
            CancelInvoke("CDQ_TestModelFailing");
            GameObjectIns.Instance.SetAKeyToStart_ButtonCancelInvoke();

            GameObjectIns.Instance.SetCDQ_CD_OverAudio(false);
            PlayerAudio.Instance.PlayBtnAudio();
            GameObjectIns.Instance.VibateController(0, 100, 1);
            print("充电枪考试 不能点击充电弓模式 测验失败");
            GameObjectIns.Instance.PlayerHint_Canvas.SetActive(true);
            GameObjectIns.Instance.PlayerHint_Text.text = "充电枪考试 不能点击充电弓模式 测验失败";

            print("40操作错误！请重新培训后再进行考试");
            PlayerAudio.Instance.PlayAudioClipTest(40);

            CDQ_TestModelFailing();
            return;
        }
        //播放声音、考试没有失败
        if (GameObjectIns.Instance.IsPlayAudio == false && GameObjectIns.Instance.IsTestFalse == false)
        {
            //播放动画不能操作
            if (GameObjectIns.Instance.IsPlayAni)
            {
                //钥匙
                if (other.transform.name == "YaoShi")
                {
                    if (GameObjectIns.Instance.IsYaoShi)
                    {
                        GameObjectIns.Instance.CancelInvoke("PlayYaoShiAudio");
                        PlayerAudio.Instance.PlayBtnAudio();
                        GameObjectIns.Instance.VibateController(0, 100, 1);
                        print("钥匙");
                        //GameObjectIns.Instance.SetHight(GameObjectIns.Instance.YaoShi, false);
                        GameObjectIns.Instance.CloseYaoShi(true, 2f);
                    }
                    else
                    {
                        print("钥匙已关闭");
                        GameObjectIns.Instance.IsYaoShiShouShaOver();
                    }
                }
                //手刹
                else if (other.transform.name == "ShouSha001")
                {
                    if (GameObjectIns.Instance.IsShouSha == false)
                    {
                        GameObjectIns.Instance.CancelInvoke("PlayLaShouShaAudio");
                        PlayerAudio.Instance.PlayBtnAudio();
                        GameObjectIns.Instance.VibateController(0, 100, 1);
                        print("手刹");
                        //GameObjectIns.Instance.SetHight(GameObjectIns.Instance.ShouSha001, false);
                        GameObjectIns.Instance.CloseShouSha(true, 2f);
                        GameObjectIns.Instance.IsYaoShiShouShaOver();
                    }
                    else
                    {
                        print("手刹已关闭");
                        GameObjectIns.Instance.IsYaoShiShouShaOver();
                    }
                }
                //挡位
                else if (other.name == "DangWei_N")
                {
                    if (GameObjectIns.Instance.DangWei_N == false)
                    {
                        GameObjectIns.Instance.CancelInvoke("PlayDangWeiNAudio");
                        //GameObjectIns.Instance.SetHight(GameObjectIns.Instance.DangWei_N_GameObject.gameObject, false);
                        PlayerAudio.Instance.PlayBtnAudio();
                        GameObjectIns.Instance.VibateController(0, 100, 1);
                        GameObjectIns.Instance.DangWei_N = true;
                        GameObjectIns.Instance.IsYaoShiShouShaOver();
                    }
                }
                //充电口门
                else if (other.name == "CDK_Door")
                {
                    //GameObjectIns.Instance.SetHight(other.gameObject, false);
                    print("打开充电口");
                    if (GameObjectIns.Instance.IschargeDoor && GameObjectIns.Instance.IsOverCDK_Door)
                    {
                        GameObjectIns.Instance.CDK_Door_arrow.SetActive(false);
                        PlayerAudio.Instance.PlayBtnAudio();
                        GameObjectIns.Instance.VibateController(0, 100, 1);
                        GameObjectIns.Instance.IschargeDoor = false;
                        print("打开充电口");
                        GameObjectIns.Instance.DOTweenRotate(GameObjectIns.Instance.chargeDoor, SetRot.RotY_180, 2f, true);

                        //GameObjectIns.Instance.SetHight(GameObjectIns.Instance.XuanBi_GunButtoonA, true);
                        //GameObjectIns.Instance.SetHight(GameObjectIns.Instance.XuanBi_GunButtoonB, true);


                        GameObjectIns.Instance.YiWu.SetActive(true);
                        GameObjectIns.Instance.SetHight(GameObjectIns.Instance.YiWu, false);
                        //GameObjectIns.Instance.SetHight(GameObjectIns.Instance.YiWu, true);


                        print("37请检查充电口有无异物，如果有请先清理干净");
                        //PlayerAudio.Instance.PlayAudioClipTest(37);

                        //GameObjectIns.Instance.PlayerHint_Canvas.SetActive(true);
                        //GameObjectIns.Instance.PlayerHint_Text.text = "请检查充电口有无异物，如果有请用手点击先清理干净";

                    }
                    else
                    {
                        print("充电口已打开 ");
                    }
                    if (GameObjectIns.Instance.CDQAIsCDLe && GameObjectIns.Instance.CDQBIsCDLe && GameObjectIns.Instance.CDQ_XuanBi_A && GameObjectIns.Instance.CDQ_XuanBi_B && GameObjectIns.Instance.IsOverCDK_Door)
                    {
                        GameObjectIns.Instance.IsOverCDK_Door = false;
                        print("关闭充电门");
                        PlayerAudio.Instance.PlayBtnAudio();
                        GameObjectIns.Instance.VibateController(0, 100, 1);
                        GameObjectIns.Instance.IschargeDoor = true;
                        GameObjectIns.Instance.DOTweenRotate(GameObjectIns.Instance.chargeDoor, SetRot.RotY180, 2f, true);

                        GameObjectIns.Instance.CDQGameOver1();
                    }
                }
                //充电枪B身体
                else if (other.name == "XuanBi_GunL_B_Body")
                {
                    PlayerAudio.Instance.PlayBtnAudio();
                    GameObjectIns.Instance.VibateController(0, 100, 1);
                    print("碰到充电枪B");
                }
                //充电枪A身体
                else if (other.name == "XuanBi_GunL_A_Body")
                {
                    PlayerAudio.Instance.PlayBtnAudio();
                    GameObjectIns.Instance.VibateController(0, 100, 1);
                    print("碰到充电枪A");
                }
                //结束/复位按钮-右
                else if (other.name == "XuanBi_RedBuL1")
                {
                    if (GameObjectIns.Instance.CDQBIsCDLe && GameObjectIns.Instance.CDQBIsCD)
                    {
                        GameObjectIns.Instance.SetXuanBi_RedBu_Ani(true);

                        GameObjectIns.Instance.vr_cartoon_hand_prefab_Left.SetActive(true);
                        GameObjectIns.Instance.vr_cartoon_hand_prefab_Left.transform.parent.GetComponent<MeshRenderer>().enabled = false;
                        GameObjectIns.Instance.SetCDQ_CD_OverAudio(false);
                       
                        PlayerAudio.Instance.PlayBtnAudio();
                        GameObjectIns.Instance.VibateController(0, 100, 1);
                        GameObjectIns.Instance.PlayerHint_Canvas.SetActive(false);
                        GameObjectIns.Instance.CDQBIsCD = false;
                        GameObjectIns.Instance.CDQAIsCD = false;

                        GameObjectIns.Instance.SetCDQ_CDD(false, GameObjectIns.Instance.RightYellowLight);
                        GameObjectIns.Instance.SetCDQ_CDD(false, GameObjectIns.Instance.RightGreenLight);
                        GameObjectIns.Instance.SetCDQ_CDD(false, GameObjectIns.Instance.LeftYellowLight);
                        GameObjectIns.Instance.SetCDQ_CDD(false, GameObjectIns.Instance.LeftGreenLight);

                        GameObjectIns.Instance.CDQ_JiKongTai_Btn_Parent.SetActive(true);
                        GameObjectIns.Instance.CDQ_JiKongTai_CDXX_Parent.SetActive(false);
                        GameObjectIns.Instance.CDQ_309_KXZ.SetActive(true);
                        GameObjectIns.Instance.CDQ_309_CDZ.SetActive(false);
                        GameObjectIns.Instance.CDQ_310_KXZ.SetActive(true);
                        GameObjectIns.Instance.CDQ_310_CDZ.SetActive(false);
                        GameObjectIns.Instance.CDQ_309_CDZ.transform.parent.transform.GetChild(2).GetComponent<Text>().text = "309号直流";
                        GameObjectIns.Instance.CDQ_310_CDZ.transform.parent.transform.GetChild(2).GetComponent<Text>().text = "310号直流";

                    }
                    else
                    {
                        print("充电枪B还未充电");
                    }
                }
                //结束/复位按钮-左
                else if (other.name == "XuanBi_RedBuR1")
                {
                    if (GameObjectIns.Instance.CDQAIsCDLe && GameObjectIns.Instance.CDQBIsCD)
                    {
                        GameObjectIns.Instance.SetXuanBi_RedBu_Ani(false);

                        GameObjectIns.Instance.vr_cartoon_hand_prefab_Left.SetActive(true);
                        GameObjectIns.Instance.vr_cartoon_hand_prefab_Left.transform.parent.GetComponent<MeshRenderer>().enabled = false;
                        GameObjectIns.Instance.SetCDQ_CD_OverAudio(false);

                        PlayerAudio.Instance.PlayBtnAudio();
                        GameObjectIns.Instance.VibateController(0, 100, 1);
                        GameObjectIns.Instance.PlayerHint_Canvas.SetActive(false);
                        GameObjectIns.Instance.CDQBIsCD = false;
                        GameObjectIns.Instance.CDQAIsCD = false;

                        GameObjectIns.Instance.SetCDQ_CDD(false, GameObjectIns.Instance.RightYellowLight);
                        GameObjectIns.Instance.SetCDQ_CDD(false, GameObjectIns.Instance.RightGreenLight);
                        GameObjectIns.Instance.SetCDQ_CDD(false, GameObjectIns.Instance.LeftYellowLight);
                        GameObjectIns.Instance.SetCDQ_CDD(false, GameObjectIns.Instance.LeftGreenLight);

                        GameObjectIns.Instance.CDQ_JiKongTai_Btn_Parent.SetActive(true);
                        GameObjectIns.Instance.CDQ_JiKongTai_CDXX_Parent.SetActive(false);
                        GameObjectIns.Instance.CDQ_309_KXZ.SetActive(true);
                        GameObjectIns.Instance.CDQ_309_CDZ.SetActive(false);
                        GameObjectIns.Instance.CDQ_310_KXZ.SetActive(true);
                        GameObjectIns.Instance.CDQ_310_CDZ.SetActive(false);
                        GameObjectIns.Instance.CDQ_309_CDZ.transform.parent.transform.GetChild(2).GetComponent<Text>().text = "309号直流";
                        GameObjectIns.Instance.CDQ_310_CDZ.transform.parent.transform.GetChild(2).GetComponent<Text>().text = "310号直流";
                    }
                    else
                    {
                        print("充电枪B还未充电");
                    }
                }
                //异物
                else if (other.name == "YiWu")
                {
                    PlayerAudio.Instance.PlayBtnAudio();
                    GameObjectIns.Instance.VibateController(0, 100, 1);
                    GameObjectIns.Instance.YiWu.SetActive(false);
                    //GameObjectIns.Instance.SetHight(GameObjectIns.Instance.YiWu, false);

                    print("配音：26按下充电枪上方的按钮，垂直于充电桩方向将其拔下(1)");
                    //GameObjectIns.Instance.PlayerHint_Text.text = "请将手柄靠近充电枪把手 请注意握枪方式";
                    //PlayerAudio.Instance.PlayAudioClip(18);
                    //PlayerAudio.Instance.PlayAudioClipTest(26);

                }
                //集控屏 充电枪309
                else if (other.name == "CDQ_309_Button")
                {
                    if (GameObjectIns.Instance.CDQAIsCD && GameObjectIns.Instance.CDQBIsCD)
                    {
                        PlayerAudio.Instance.PlayBtnAudio();
                        print("36界面显示充电电压、电流、当前SOC、充电电量等信息");
                        //PlayerAudio.Instance.PlayAudioClipTest(36);
                        GameObjectIns.Instance.CDQ_JiKongTai_Btn_Parent.SetActive(false);
                        GameObjectIns.Instance.CDQ_JiKongTai_CDXX_Parent.SetActive(true);

                        GameObjectIns.Instance.CDQ_JiKongTai_CDXX_Parent.transform.GetChild(0).GetChild(0).GetComponent<Text>().text = "309";
                    }
                }
                //集控屏 充电枪310
                //else if (other.name == "CDQ_310_Button")
                //{
                //    PlayerAudio.Instance.PlayBtnAudio();
                //    print("36界面显示充电电压、电流、当前SOC、充电电量等信息");
                //    //PlayerAudio.Instance.PlayAudioClipTest(36);
                //    GameObjectIns.Instance.CDQ_JiKongTai_Btn_Parent.SetActive(false);
                //    GameObjectIns.Instance.CDQ_JiKongTai_CDXX_Parent.SetActive(true);

                //    GameObjectIns.Instance.CDQ_JiKongTai_CDXX_Parent.transform.GetChild(0).GetChild(0).GetComponent<Text>().text = "310";
                //}
                //集控屏充电枪充电信息返回按钮
                else if (other.name == "CDQ_CDXX_Return_Button")
                {
                    PlayerAudio.Instance.PlayBtnAudio();
                    GameObjectIns.Instance.CDQ_JiKongTai_Btn_Parent.SetActive(true);
                    GameObjectIns.Instance.CDQ_JiKongTai_CDXX_Parent.SetActive(false);
                    if (GameObjectIns.Instance.CDQAIsCD && GameObjectIns.Instance.CDQBIsCD)
                    {
                        GameObjectIns.Instance.CDQ_309_CDZ.SetActive(true);
                        GameObjectIns.Instance.CDQ_309_KXZ.SetActive(false);
                        GameObjectIns.Instance.CDQ_310_CDZ.SetActive(true);
                        GameObjectIns.Instance.CDQ_310_KXZ.SetActive(false);
                    }
                }
                //集控屏结束充电按钮
                else if (other.name == "CDQ_OVERCD_Button")
                {
                    if (GameObjectIns.Instance.CDQ_JiKongTai_CDXX_Parent.transform.GetChild(0).GetChild(0).GetComponent<Text>().text == "309")
                    {
                        if (GameObjectIns.Instance.CDQAIsCD && GameObjectIns.Instance.CDQBIsCD)
                        {
                            GameObjectIns.Instance.CDQAIsCD = false;
                            GameObjectIns.Instance.CDQBIsCD = false;

                            PlayerAudio.Instance.PlayBtnAudio();
                            GameObjectIns.Instance.VibateController(0, 100, 1);

                            GameObjectIns.Instance.SetCDQ_CDD(false, GameObjectIns.Instance.LeftYellowLight);
                            GameObjectIns.Instance.SetCDQ_CDD(false, GameObjectIns.Instance.LeftGreenLight);
                            GameObjectIns.Instance.SetCDQ_CDD(false, GameObjectIns.Instance.RightYellowLight);
                            GameObjectIns.Instance.SetCDQ_CDD(false, GameObjectIns.Instance.RightGreenLight);

                            GameObjectIns.Instance.CDQ_309_KXZ.SetActive(true);
                            GameObjectIns.Instance.CDQ_309_CDZ.SetActive(false);
                            GameObjectIns.Instance.CDQ_310_KXZ.SetActive(true);
                            GameObjectIns.Instance.CDQ_310_CDZ.SetActive(false);

                            GameObjectIns.Instance.CDQJKPReturnAudio();
                            GameObjectIns.Instance.PlayerHint_Canvas.SetActive(true);
                            GameObjectIns.Instance.PlayerHint_Text.text = "按左手手柄Y键可以查看集控屏，按左手手柄上的X键返回原地";
                        }
                    }
                    else
                    {
                        if (GameObjectIns.Instance.CDQBIsCD)
                        {
                            GameObjectIns.Instance.CDQBIsCD = false;

                            PlayerAudio.Instance.PlayBtnAudio();
                            GameObjectIns.Instance.VibateController(0, 100, 1);
                            //GameObjectIns.Instance.RightYellowLight.DisableKeyword("_EMISSION");
                            //GameObjectIns.Instance.RightGreenLight.DisableKeyword("_EMISSION");

                            GameObjectIns.Instance.SetCDQ_CDD(false, GameObjectIns.Instance.RightYellowLight);
                            GameObjectIns.Instance.SetCDQ_CDD(false, GameObjectIns.Instance.RightGreenLight);

                            GameObjectIns.Instance.CDQ_310_KXZ.SetActive(true);
                            //GameObjectIns.Instance.CDQ_310_CDZ.SetActive(false);

                            if (GameObjectIns.Instance.CDQ_Che_A || GameObjectIns.Instance.CDQ_Che_B)
                            {
                                print("关闭结束充电提示");
                                GameObjectIns.Instance.CDQB_arrow.SetActive(false);
                                //GameObjectIns.Instance.SetHight(GameObjectIns.Instance.XuanBi_GunButtoonB, true);
                            }
                            else
                            {
                                print("Bug");
                            }


                            GameObjectIns.Instance.PlayerHint_Canvas.SetActive(true);
                            GameObjectIns.Instance.PlayerHint_Text.text = "按左手手柄Y键可以查看集控屏，按左手手柄上的X键返回原地";

                            //GameObjectIns.Instance.CDQJKPReturnAudio();
                        }
                    }
                    PlayerAudio.Instance.PlayBtnAudio();
                    GameObjectIns.Instance.CDQ_JiKongTai_Btn_Parent.SetActive(true);
                    GameObjectIns.Instance.CDQ_JiKongTai_CDXX_Parent.SetActive(false);
                }
                //悬臂B子物体
                else if (other.name == "XuanBi_GunL_B_Child")
                {
                    PlayerAudio.Instance.PlayBtnAudio();
                    GameObjectIns.Instance.VibateController(0, 100, 1);
                    if (GameObjectIns.Instance.CDQ_XuanBi_B && IsHeadQiang())
                    {
                        if (GameObjectIns.Instance.XuanBi_GunL_B_Parent.transform.Find("XuanBi_GunL_B") != null)
                        {
                            GameObjectIns.Instance.B_HandVR.transform.GetComponent<Animator>().SetBool("IsHold", true);
                            GameObjectIns.Instance.B_HandVR.transform.GetComponent<Animator>().SetBool("IsPine", false);
                            GameObjectIns.Instance.B_HandVR.transform.GetChild(0).transform.gameObject.SetActive(true);
                            GameObjectIns.Instance.B_HandVR.transform.GetChild(1).transform.gameObject.SetActive(true);
                            GameObjectIns.Instance.Play_L_Hand_Mesh.SetActive(false);
                            GameObjectIns.Instance.Play_R_Hand_Mesh.SetActive(false);
                        }
                        else if (GameObjectIns.Instance.XuanBi_GunL_B_Parent.transform.Find("XuanBi_GunL_A") != null)
                        {
                            GameObjectIns.Instance.A_HandVR.transform.GetComponent<Animator>().SetBool("IsHold", true);
                            GameObjectIns.Instance.A_HandVR.transform.GetComponent<Animator>().SetBool("IsPine", false);
                            GameObjectIns.Instance.A_HandVR.transform.GetChild(0).transform.gameObject.SetActive(true);
                            GameObjectIns.Instance.A_HandVR.transform.GetChild(1).transform.gameObject.SetActive(true);
                            GameObjectIns.Instance.Play_L_Hand_Mesh.SetActive(false);
                            GameObjectIns.Instance.Play_R_Hand_Mesh.SetActive(false);
                        }
                        else
                        {
                            print("   XuanBi_GunL_B_Parent");
                        }
                    }

                }
                //悬臂A子物体
                else if (other.name == "XuanBi_GunL_A_Child")
                {
                    PlayerAudio.Instance.PlayBtnAudio();
                    GameObjectIns.Instance.VibateController(0, 100, 1);
                    if (GameObjectIns.Instance.CDQ_XuanBi_A && IsHeadQiang())
                    {
                        if (GameObjectIns.Instance.XuanBi_GunL_A_Parent.transform.Find("XuanBi_GunL_B") != null)
                        {
                            GameObjectIns.Instance.B_HandVR.transform.GetComponent<Animator>().SetBool("IsHold", true);
                            GameObjectIns.Instance.B_HandVR.transform.GetComponent<Animator>().SetBool("IsPine", false);
                            GameObjectIns.Instance.B_HandVR.transform.GetChild(0).transform.gameObject.SetActive(true);
                            GameObjectIns.Instance.B_HandVR.transform.GetChild(1).transform.gameObject.SetActive(true);
                            GameObjectIns.Instance.Play_L_Hand_Mesh.SetActive(false);
                            GameObjectIns.Instance.Play_R_Hand_Mesh.SetActive(false);
                        }
                        else if (GameObjectIns.Instance.XuanBi_GunL_A_Parent.transform.Find("XuanBi_GunL_A") != null)
                        {
                            GameObjectIns.Instance.A_HandVR.transform.GetComponent<Animator>().SetBool("IsHold", true);
                            GameObjectIns.Instance.A_HandVR.transform.GetComponent<Animator>().SetBool("IsPine", false);
                            GameObjectIns.Instance.A_HandVR.transform.GetChild(0).transform.gameObject.SetActive(true);
                            GameObjectIns.Instance.A_HandVR.transform.GetChild(1).transform.gameObject.SetActive(true);
                            GameObjectIns.Instance.Play_L_Hand_Mesh.SetActive(false);
                            GameObjectIns.Instance.Play_R_Hand_Mesh.SetActive(false);
                        }
                        else
                        {
                            print("   XuanBi_GunL_A_Parent");
                        }
                    }
                }
                //车A充电口子物体
                else if (other.name == "Che_A_Child")
                {
                    PlayerAudio.Instance.PlayBtnAudio();
                    GameObjectIns.Instance.VibateController(0, 100, 1);
                    print(GameObjectIns.Instance.CDQ_Che_A + " " + IsHeadQiang());
                    if (GameObjectIns.Instance.CDQ_Che_A && IsHeadQiang())
                    {
                        print("1");
                        if (GameObjectIns.Instance.Che_A_Parent.transform.Find("XuanBi_GunL_B") != null)
                        {
                            print("2");
                            GameObjectIns.Instance.SetCDQHeadAni_Enter(GameObjectIns.Instance.B_HandVR, true, 0);
                        }
                        else if (GameObjectIns.Instance.Che_A_Parent.transform.Find("XuanBi_GunL_A") != null)
                        {
                            print("3");
                            GameObjectIns.Instance.SetCDQHeadAni_Enter( GameObjectIns.Instance.A_HandVR, true, 0);
                        }
                        else
                        {
                            print("   Che_A_Parent");
                        }
                    }
                }
                //车B充电口子物体
                else if (other.name == "Che_B_Child")
                {
                    PlayerAudio.Instance.PlayBtnAudio();
                    GameObjectIns.Instance.VibateController(0, 100, 1);
                    if (GameObjectIns.Instance.CDQ_Che_B && IsHeadQiang())
                    {
                        if (GameObjectIns.Instance.Che_B_Parent.transform.Find("XuanBi_GunL_B") != null)
                        {
                            GameObjectIns.Instance.SetCDQHeadAni_Enter(GameObjectIns.Instance.B_HandVR, true, 0);
                        }
                        else if (GameObjectIns.Instance.Che_B_Parent.transform.Find("XuanBi_GunL_A") != null)
                        {
                            GameObjectIns.Instance.SetCDQHeadAni_Enter(GameObjectIns.Instance.A_HandVR, true, 0);
                        }
                        else
                        {
                            print("   Che_B_Parent");
                        }
                    }
                }
            }
            else
            {
                print("正在播放动画");
            }
        }
        else
        {
            print("正在播放声音 稍后操作");
        }
    }

    private bool is_KeyDown = true;

    private void OnTriggerStay(Collider other)
    {
        //没有播放声音 没有播放动画 考试没有失败
        if (GameObjectIns.Instance.IsPlayAudio == false && GameObjectIns.Instance.IsPlayAni && GameObjectIns.Instance.IsTestFalse == false)
        {
            //按下手柄扳机
            if (Controller.UPvr_GetKeyDown(0, Pvr_KeyCode.TRIGGER) || Controller.UPvr_GetKeyDown(1, Pvr_KeyCode.TRIGGER) || Input.GetKeyDown(KeyCode.N) && is_KeyDown)
            {
                if (IsHeadQiang() == false)
                return;
                //如果没打开充电口门就取枪 考试失败
                if (GameObjectIns.Instance.IschargeDoor)
                {
                    PlayerAudio.Instance.PlayBtnAudio();
                    GameObjectIns.Instance.VibateController(0, 100, 1);
                    print("未打开充电门 不能拿枪 测验失败");
                    GameObjectIns.Instance.PlayerHint_Canvas.SetActive(true);
                    GameObjectIns.Instance.PlayerHint_Text.text = "未打开充电门 不能拿枪 测验失败";

                    print("40操作错误！请重新培训后再进行考试");
                    PlayerAudio.Instance.PlayAudioClipTest(40);

                    CDQ_TestModelFailing();
                    return;
                }
                //充电枪B
                if (other.name == "XuanBi_GunL_B_Body")
                {
                    //充电枪B正在充电
                    if (GameObjectIns.Instance.CDQBIsCD)
                    {
                        PlayerAudio.Instance.PlayBtnAudio();
                        GameObjectIns.Instance.VibateController(0, 100, 1);
                        GameObjectIns.Instance.PlayerHint_Canvas.SetActive(true);
                        GameObjectIns.Instance.PlayerHint_Text.text = "充电枪B正在充电不能取下\n测验失败 ";
                        print("充电枪B 正在充电 不能取下 测验失败");

                        GameObjectIns.Instance.SetTestModelCDQHuoHua(true);

                        print("40操作错误！请重新培训后再进行考试");
                        PlayerAudio.Instance.PlayAudioClipTest(40);

                        CDQ_TestModelFailing();
                        return;
                    }
                    //判断充电枪B在悬臂 A B 下
                    if (GameObjectIns.Instance.CDQ_B.transform.parent.name == "XuanBi_GunL_B_Parent" || GameObjectIns.Instance.CDQ_B.transform.parent.name == "XuanBi_GunL_A_Parent")
                    {
                        //如果充电枪B已经充过电 就不能拿枪
                        if (GameObjectIns.Instance.CDQBIsCDLe)
                        {
                            print("充电枪B已经冲过电 测验失败  "  + GameObjectIns.Instance.CDQBIsCDLe);
                            GameObjectIns.Instance.PlayerHint_Canvas.SetActive(true);
                            GameObjectIns.Instance.PlayerHint_Text.text = "充电枪B已经冲过电 测验失败";
                            

                            print("40操作错误！请重新培训后再进行考试");
                            PlayerAudio.Instance.PlayAudioClipTest(40);

                            CDQ_TestModelFailing();
                            return;
                        }
                    }
                    //手里没枪
                    if (IsHeadQiang())
                    {
                        is_KeyDown = false;
                        print("按下了 充电枪B");
                        GameObjectIns.Instance.PlayerHint_Canvas.SetActive(false);
                        PlayerAudio.Instance.PlayBtnAudio();
                        GameObjectIns.Instance.VibateController(0, 100, 1);
                        //GameObjectIns.Instance.SetHight(GameObjectIns.Instance.XuanBi_GunButtoonB, false);
                        if (GameObjectIns.Instance.CDQ_B.transform.parent.name == "XuanBi_GunL_B_Parent")
                        {
                            GameObjectIns.Instance.CDQ_XuanBi_B = false;
                        }
                        else if (GameObjectIns.Instance.CDQ_B.transform.parent.name == "XuanBi_GunL_A_Parent")
                        {
                            GameObjectIns.Instance.CDQ_XuanBi_A = false;
                        }
                        else if (GameObjectIns.Instance.CDQ_B.transform.parent.name == "Che_B_Parent")
                        {
                            GameObjectIns.Instance.CDQ_Che_B = false;
                        }
                        else if (GameObjectIns.Instance.CDQ_B.transform.parent.name == "Che_A_Parent")
                        {
                            GameObjectIns.Instance.CDQ_Che_A = false;
                        }
                        else
                        {
                            return;
                        }
                        GameObjectIns.Instance.CDQ_B.transform.parent = transform;

                        GameObjectIns.Instance.XuanBi_GunButtoonB.transform.localEulerAngles = SetRot.RotX90;
                    }
                    else
                    {
                        print("已经拿枪了");
                    }
                }
                //充电枪A
                if (other.name == "XuanBi_GunL_A_Body")
                {
                    //充电枪A正在充电
                    if (GameObjectIns.Instance.CDQAIsCD)
                    {
                        PlayerAudio.Instance.PlayBtnAudio();
                        GameObjectIns.Instance.VibateController(0, 100, 1);
                        GameObjectIns.Instance.PlayerHint_Canvas.SetActive(true);
                        GameObjectIns.Instance.PlayerHint_Text.text = "充电枪A正在充电不能取下\n测验失败 ";
                        print("充电枪A 正在充电 测验失败");


                        GameObjectIns.Instance.SetTestModelCDQHuoHua(true);

                        print("40操作错误！请重新培训后再进行考试");
                        PlayerAudio.Instance.PlayAudioClipTest(40);

                        CDQ_TestModelFailing();
                        return;
                    }
                    //判断充电枪B在悬臂 A B 下
                    if (GameObjectIns.Instance.CDQ_A.transform.parent.name == "XuanBi_GunL_B_Parent" || GameObjectIns.Instance.CDQ_A.transform.parent.name == "XuanBi_GunL_A_Parent")
                    {
                        if (GameObjectIns.Instance.CDQAIsCDLe)
                        {
                            print("充电枪A已经冲过电 测验失败  "+ GameObjectIns.Instance.CDQAIsCDLe);

                            GameObjectIns.Instance.PlayerHint_Canvas.SetActive(true);
                            GameObjectIns.Instance.PlayerHint_Text.text = "充电枪A已经冲过电 测验失败";

                            print("40操作错误！请重新培训后再进行考试");
                            PlayerAudio.Instance.PlayAudioClipTest(40);

                            CDQ_TestModelFailing();
                            return;
                        }
                    }
                    //手里没枪
                    if (IsHeadQiang())
                    {
                        is_KeyDown = false;
                        print("按下了 充电枪A");
                        GameObjectIns.Instance.PlayerHint_Canvas.SetActive(false);
                        PlayerAudio.Instance.PlayBtnAudio();
                        GameObjectIns.Instance.VibateController(0, 100, 1);
                        //GameObjectIns.Instance.SetHight(GameObjectIns.Instance.XuanBi_GunButtoonA, false);
                        if (GameObjectIns.Instance.CDQ_A.transform.parent.name == "XuanBi_GunL_B_Parent")
                        {
                            GameObjectIns.Instance.CDQ_XuanBi_B = false;
                        }
                        else if (GameObjectIns.Instance.CDQ_A.transform.parent.name == "XuanBi_GunL_A_Parent")
                        {
                            GameObjectIns.Instance.CDQ_XuanBi_A = false;
                        }
                        else if (GameObjectIns.Instance.CDQ_A.transform.parent.name == "Che_B_Parent")
                        {
                            GameObjectIns.Instance.CDQ_Che_B = false;
                        }
                        else if (GameObjectIns.Instance.CDQ_A.transform.parent.name == "Che_A_Parent")
                        {
                            GameObjectIns.Instance.CDQ_Che_A = false;
                        }
                        else
                        {
                            return;
                        }
                        GameObjectIns.Instance.CDQ_A.transform.parent = transform;

                        GameObjectIns.Instance.XuanBi_GunButtoonA.transform.localEulerAngles = SetRot.RotX90;
                    }
                    else
                    {
                        print("已经拿枪了");
                    }

                }

            }
            //松开手柄扳机
            if (Controller.UPvr_GetKeyUp(0, Pvr_KeyCode.TRIGGER) || Controller.UPvr_GetKeyUp(1, Pvr_KeyCode.TRIGGER) || Input.GetKeyUp(KeyCode.N)&&!is_KeyDown)
            {
                is_KeyDown = true;
                print("松开了");
                //放到悬臂B下
                if (other.name == "XuanBi_GunL_B_Child")
                {
                    //悬臂B下没有充电枪 A 充电枪 B
                    if (!GameObjectIns.Instance.XuanBi_GunL_B_Parent.transform.Find("XuanBi_GunL_B") && !GameObjectIns.Instance.XuanBi_GunL_B_Parent.transform.Find("XuanBi_GunL_A"))
                    {
                        // 手里是充电枪B 就放到悬臂B
                        if (transform.Find("XuanBi_GunL_B"))
                        {
                            print("CDQ_B 可以放到悬臂B");
                            PlayerAudio.Instance.PlayBtnAudio();
                            GameObjectIns.Instance.VibateController(0, 100, 1);
                            GameObjectIns.Instance.CDQ_XuanBi_B = true;
                            GameObjectIns.Instance.CDQ_B.transform.parent = GameObjectIns.Instance.XuanBi_GunL_B_Parent.transform;
                            GameObjectIns.Instance.CDQ_B.transform.localPosition = Vector3.zero;
                            GameObjectIns.Instance.CDQ_B.transform.localEulerAngles = Vector3.zero;
                            GameObjectIns.Instance.XuanBi_GunButtoonB.transform.localEulerAngles = SetRot.RotX87;
                        }
                        // 手里是充电枪A 就放到悬臂B
                        else if (transform.Find("XuanBi_GunL_A"))
                        {
                            print("CDQ_A 可以放到悬臂B");
                            PlayerAudio.Instance.PlayBtnAudio();
                            GameObjectIns.Instance.VibateController(0, 100, 1);
                            GameObjectIns.Instance.CDQ_XuanBi_B = true;
                            GameObjectIns.Instance.CDQ_A.transform.parent = GameObjectIns.Instance.XuanBi_GunL_B_Parent.transform;
                            GameObjectIns.Instance.CDQ_A.transform.localPosition = Vector3.zero;
                            GameObjectIns.Instance.CDQ_A.transform.localEulerAngles = Vector3.zero;

                            GameObjectIns.Instance.XuanBi_GunButtoonA.transform.localEulerAngles = SetRot.RotX87;
                        }

                    }
                    else
                    {
                        print("悬臂B下有充电枪");
                    }
                }
                //放在悬臂A下
                else if (other.name == "XuanBi_GunL_A_Child")
                {
                    //悬臂A下没有充电枪 A 充电枪 B
                    if (!GameObjectIns.Instance.XuanBi_GunL_A_Parent.transform.Find("XuanBi_GunL_B") && !GameObjectIns.Instance.XuanBi_GunL_A_Parent.transform.Find("XuanBi_GunL_A"))
                    {
                        //手里有充电枪B 就放到悬臂A
                        if (transform.Find("XuanBi_GunL_B"))
                        {
                            print("CDQ_B 可以放到悬臂A");
                            PlayerAudio.Instance.PlayBtnAudio();
                            GameObjectIns.Instance.VibateController(0, 100, 1);
                            GameObjectIns.Instance.CDQ_XuanBi_A = true;
                            GameObjectIns.Instance.CDQ_B.transform.parent = GameObjectIns.Instance.XuanBi_GunL_A_Parent.transform;
                            GameObjectIns.Instance.CDQ_B.transform.localPosition = Vector3.zero;
                            GameObjectIns.Instance.CDQ_B.transform.localEulerAngles = Vector3.zero;

                            GameObjectIns.Instance.XuanBi_GunButtoonB.transform.localEulerAngles = SetRot.RotX87;
                        }
                        //手里有充电枪A 就放到悬臂A
                        else if (transform.Find("XuanBi_GunL_A"))
                        {
                            print("CDQ_A 可以放到悬臂A");
                            PlayerAudio.Instance.PlayBtnAudio();
                            GameObjectIns.Instance.VibateController(0, 100, 1);
                            GameObjectIns.Instance.CDQ_XuanBi_A = true;
                            GameObjectIns.Instance.CDQ_A.transform.parent = GameObjectIns.Instance.XuanBi_GunL_A_Parent.transform;
                            GameObjectIns.Instance.CDQ_A.transform.localPosition = Vector3.zero;
                            GameObjectIns.Instance.CDQ_A.transform.localEulerAngles = Vector3.zero;

                            GameObjectIns.Instance.XuanBi_GunButtoonA.transform.localEulerAngles = SetRot.RotX87;
                        }
                    }
                    else
                    {
                        print("悬臂A下有充电枪");
                    }
                }
                //放在车A充电口下
                else if (other.name == "Che_A_Child")
                {
                    //车A下没有充电枪 A 充电枪 B
                    if (!GameObjectIns.Instance.Che_A_Parent.transform.Find("XuanBi_GunL_B") && !GameObjectIns.Instance.Che_A_Parent.transform.Find("XuanBi_GunL_A"))
                    {
                        //手里有充电枪B 就放到车A充电口处
                        if (transform.Find("XuanBi_GunL_B"))
                        {
                            //车A充电口有异物 异物未清除 考试失败
                            if (GameObjectIns.Instance.YiWu.activeInHierarchy)
                            {
                                print("异物未清除 不能拿枪 测验失败");
                                GameObjectIns.Instance.PlayerHint_Canvas.SetActive(true);
                                GameObjectIns.Instance.PlayerHint_Text.text = "异物未清除 测验失败";

                                print("40操作错误！请重新培训后再进行考试");
                                PlayerAudio.Instance.PlayAudioClipTest(40);

                                GameObjectIns.Instance.LavaSparks03_coll_L.Play();

                                GameObjectIns.Instance.Che_A_Parent.transform.GetChild(0).GetComponent<MeshCollider>().enabled = false;
                                GameObjectIns.Instance.CDQ_B.transform.Find("XuanBi_GunBody").GetComponent<MeshCollider>().enabled = false;
                                GameObjectIns.Instance.CDQ_B.transform.Find("XuanBi_GunL_B_Body").GetComponent<MeshCollider>().enabled = false;



                                print("47滋滋的电流声音效_爱给网_aigei_com");
                                PlayerAudio.Instance.PlayCDQDL();

                                CancelInvoke("PlayJKPAudio");
                                CancelInvoke("playMeiQuJkpAudio");
                                CancelInvoke("ChongDianZhongAudioHint");
                                GameObjectIns.Instance.SetCDQ_CD_OverAudio(false);

                                Invoke("CDQ_TestModelFailing", 10f);
                            }

                            GameObjectIns.Instance.CDQ_Che_A = true;
                            GameObjectIns.Instance.CDQBIsCD = true;
                            print("CDQ_B 可以放到车A");
                            PlayerAudio.Instance.PlayBtnAudio();
                            GameObjectIns.Instance.VibateController(0, 100, 1);
                            GameObjectIns.Instance.CDQ_B.transform.parent = GameObjectIns.Instance.Che_A_Parent.transform;
                            GameObjectIns.Instance.CDQ_B.transform.localPosition = Vector3.zero;
                            GameObjectIns.Instance.CDQ_B.transform.localEulerAngles = SetRot.RotZ90;
                            
                            GameObjectIns.Instance.XuanBi_GunButtoonB.transform.localEulerAngles = SetRot.RotX87;

                            GameObjectIns.Instance.SetCDQ_CDD(true, GameObjectIns.Instance.RightYellowLight);

                            DengHit();
                        }
                        //手里有充电枪A 就放到车A充电口处
                        else if (transform.Find("XuanBi_GunL_A"))
                        {
                            //车A充电口有异物 异物未清除 考试失败
                            if (GameObjectIns.Instance.YiWu.activeInHierarchy)
                            {
                                print("异物未清除 不能拿枪 测验失败");
                                GameObjectIns.Instance.PlayerHint_Canvas.SetActive(true);
                                GameObjectIns.Instance.PlayerHint_Text.text = "异物未清除 测验失败";

                                print("40操作错误！请重新培训后再进行考试");
                                PlayerAudio.Instance.PlayAudioClipTest(40);

                                GameObjectIns.Instance.LavaSparks03_coll_L.Play();

                                print("47滋滋的电流声音效_爱给网_aigei_com");
                                PlayerAudio.Instance.PlayCDQDL();

                                CancelInvoke("PlayJKPAudio");
                                CancelInvoke("playMeiQuJkpAudio");
                                CancelInvoke("ChongDianZhongAudioHint");
                                GameObjectIns.Instance.SetCDQ_CD_OverAudio(false);

                                Invoke("CDQ_TestModelFailing", 10f);
                            }

                            GameObjectIns.Instance.CDQ_Che_A = true;
                            print("CDQ_A 可以放到车A");
                            PlayerAudio.Instance.PlayBtnAudio();
                            GameObjectIns.Instance.VibateController(0, 100, 1);
                            GameObjectIns.Instance.CDQ_A.transform.parent = GameObjectIns.Instance.Che_A_Parent.transform;
                            GameObjectIns.Instance.CDQ_A.transform.localPosition = Vector3.zero;
                            GameObjectIns.Instance.CDQ_A.transform.localEulerAngles = SetRot.RotZ90;

                            GameObjectIns.Instance.XuanBi_GunButtoonA.transform.localEulerAngles = SetRot.RotX87;

                            GameObjectIns.Instance.SetCDQ_CDD(true, GameObjectIns.Instance.LeftYellowLight);

                            DengHit();
                        }
                    }
                    else
                    {
                        print("车A下有充电枪");
                    }
                }
                //放在车B充电口下
                else if (other.name == "Che_B_Child")
                {
                    //车A下没有充电枪 A 充电枪 B
                    if (!GameObjectIns.Instance.Che_B_Parent.transform.Find("XuanBi_GunL_B") && !GameObjectIns.Instance.Che_B_Parent.transform.Find("XuanBi_GunL_A"))
                    {
                        //把充电枪B放在车B充电口下
                        if (transform.Find("XuanBi_GunL_B"))
                        {
                            GameObjectIns.Instance.CDQ_Che_B = true;
                            GameObjectIns.Instance.CDQBIsCD = true;
                            print("CDQ_B 可以放到车B");
                            PlayerAudio.Instance.PlayBtnAudio();
                            GameObjectIns.Instance.VibateController(0, 100, 1);
                            GameObjectIns.Instance.CDQ_B.transform.parent = GameObjectIns.Instance.Che_B_Parent.transform;
                            GameObjectIns.Instance.CDQ_B.transform.localPosition = Vector3.zero;
                            GameObjectIns.Instance.CDQ_B.transform.localEulerAngles = SetRot.RotZ90;

                            GameObjectIns.Instance.XuanBi_GunButtoonB.transform.localEulerAngles = SetRot.RotX87;

                            GameObjectIns.Instance.SetCDQ_CDD(true, GameObjectIns.Instance.RightYellowLight);

                            DengHit();
                        }
                        //把充电枪A放在车B充电口下
                        else if (transform.Find("XuanBi_GunL_A"))
                        {
                            GameObjectIns.Instance.CDQ_Che_B = true;
                            GameObjectIns.Instance.CDQAIsCD = true;
                            print("CDQ_A 可以放到车B");
                            PlayerAudio.Instance.PlayBtnAudio();
                            GameObjectIns.Instance.VibateController(0, 100, 1);
                            GameObjectIns.Instance.CDQ_A.transform.parent = GameObjectIns.Instance.Che_B_Parent.transform;
                            GameObjectIns.Instance.CDQ_A.transform.localPosition = Vector3.zero;
                            GameObjectIns.Instance.CDQ_A.transform.localEulerAngles = SetRot.RotZ90;

                            GameObjectIns.Instance.XuanBi_GunButtoonA.transform.localEulerAngles = SetRot.RotX87;

                            GameObjectIns.Instance.SetCDQ_CDD(true, GameObjectIns.Instance.LeftYellowLight);

                            DengHit();
                        }
                    }
                    else
                    {
                        print("车B下有充电枪");
                    }
                }
                // 充电枪复位 并且充电枪A 充电枪B 已经充过电
                if (GameObjectIns.Instance.CDQ_XuanBi_A && GameObjectIns.Instance.CDQ_XuanBi_B && GameObjectIns.Instance.CDQAIsCDLe && GameObjectIns.Instance.CDQBIsCDLe)
                {
                 
                }
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        //手里没拿充电枪 并且 考试没有失败
        if (IsHeadQiang() && GameObjectIns.Instance.IsTestFalse == false)
        {
            //手离开悬臂B的位置
            if (other.name == "XuanBi_GunL_B_Child")
            {
                //悬臂B下有充电枪B 设置充电枪B松枪动画
                if (GameObjectIns.Instance.XuanBi_GunL_B_Parent.transform.Find("XuanBi_GunL_B") != null)
                {
                    GameObjectIns.Instance.SetCDQHeadAni_Enter( GameObjectIns.Instance.B_HandVR, false, 0);
                }
                //悬臂B下有充电枪A 设置充电枪A松枪动画
                else if (GameObjectIns.Instance.XuanBi_GunL_B_Parent.transform.Find("XuanBi_GunL_A") != null)
                {
                    GameObjectIns.Instance.SetCDQHeadAni_Enter(GameObjectIns.Instance.A_HandVR, false, 0);
                }
                else
                {
                    print("···XuanBi_GunL_B_Parent");
                }
            }
            else if (other.name == "XuanBi_GunL_A_Child")
            {
                //悬臂A 下有充电枪B 设置充电枪B松枪动画
                if (GameObjectIns.Instance.XuanBi_GunL_A_Parent.transform.Find("XuanBi_GunL_B") != null)
                {
                    GameObjectIns.Instance.SetCDQHeadAni_Enter( GameObjectIns.Instance.B_HandVR, false, 0);
                }
                //悬臂A 下有充电枪A 设置充电枪A松枪动画
                else if (GameObjectIns.Instance.XuanBi_GunL_A_Parent.transform.Find("XuanBi_GunL_A") != null)
                {
                    GameObjectIns.Instance.SetCDQHeadAni_Enter( GameObjectIns.Instance.A_HandVR, false, 0);
                }
                else
                {
                    print("···XuanBi_GunL_A_Parent");
                }
            }
            else if (other.name == "Che_A_Child")
            {
                //车A 下有充电枪B 设置充电枪B松枪动画
                if (GameObjectIns.Instance.Che_A_Parent.transform.Find("XuanBi_GunL_B") != null)
                {
                    GameObjectIns.Instance.SetCDQHeadAni_Enter( GameObjectIns.Instance.B_HandVR, false, 0);
                }
                //车A 下有充电枪A 设置充电枪A松枪动画
                else if (GameObjectIns.Instance.Che_A_Parent.transform.Find("XuanBi_GunL_A") != null)
                {
                    GameObjectIns.Instance.SetCDQHeadAni_Enter(GameObjectIns.Instance.A_HandVR, false, 0);
                }
                else
                {
                    print("···Che_A_Parent");
                }
            }
            else if (other.name == "Che_B_Child")
            {
                //车B 下有充电枪B 设置充电枪B松枪动画
                if (GameObjectIns.Instance.Che_B_Parent.transform.Find("XuanBi_GunL_B") != null)
                {
                    GameObjectIns.Instance.SetCDQHeadAni_Enter(GameObjectIns.Instance.B_HandVR, false, 0);
                }
                //车B 下有充电枪A 设置充电枪A松枪动画
                else if (GameObjectIns.Instance.Che_B_Parent.transform.Find("XuanBi_GunL_A") != null)
                {
                    GameObjectIns.Instance.SetCDQHeadAni_Enter( GameObjectIns.Instance.A_HandVR, false, 0);
                }
                else
                {
                    print("···Che_B_Parent");
                }
            }
        }

    }
    /// <summary>
    /// 手里面是否拿枪
    /// </summary>
    /// <returns></returns>
    private bool IsHeadQiang()
    {
        return !transform.Find("XuanBi_GunL_B") && !transform.Find("XuanBi_GunL_A");
    }
    /// <summary>
    /// 充电枪考试失败
    /// </summary>
    private void CDQ_TestModelFailing()
    {
        GameObjectIns.Instance.CancelInvoke("Is_CDQ_15s");
        print("CDQ_TestModelFailing");
        if (GameObjectIns.Instance.IsTestFalse == false)
        {
            CancelInvoke();
            GameObjectIns.Instance.CancelInvoke();
            print("SetCutTo0");
            GameObjectIns.Instance.IsTestFalse = true;
            Invoke("SetCutTo0", 3f);
        }
    }
    /// <summary>
    /// 转场动画
    /// </summary>
    private void SetCutTo0()
    {
        GameObjectIns.Instance.SetCutTo0();
    }
}