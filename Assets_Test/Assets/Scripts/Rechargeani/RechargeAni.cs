﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RechargeAni : MonoBehaviour
{
    private void OnEnable()
    {

        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).transform.gameObject.SetActive(false);
        }
        StartCoroutine(PlayerAni());
    }

    IEnumerator PlayerAni()
    {
        while (true)
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                transform.GetChild(i).transform.gameObject.SetActive(true);
                yield return new WaitForSeconds(1.0f);
            }
            for (int i = 0; i < transform.childCount; i++)
            {
                transform.GetChild(i).transform.gameObject.SetActive(false);
            }
        }
    }
    private void OnDisable()
    {
        StopCoroutine(PlayerAni());
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
