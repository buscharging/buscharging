﻿using Pvr_UnitySDKAPI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LeftShouTrigger : MonoBehaviour
{
    //public Quaternion CDQRot = Quaternion.Euler(0f, 0f, 90f);
    //public Quaternion CDQRot1 = Quaternion.Euler(0f, 0f, 0f);
    // Start is called before the first frame update

    /// <summary>
    /// 参考Left_CDQ_Test_Model脚本注释
    /// </summary>
    void Start()
    {
        GameObjectIns.Instance.DangWei_N_GameObject.transform.localPosition = PlayerPosition.DangWeiN_StartV3;

        GameObjectIns.Instance.arrowRotation_CDG.SetActive(false);
        GameObjectIns.Instance.arrowRotation_R.SetActive(false);
        GameObjectIns.Instance.arrowRotation_L.SetActive(false);
        GameObjectIns.Instance.SetCDQHeadAni_Enter(GameObjectIns.Instance.B_HandVR, false, 0);
        GameObjectIns.Instance.SetCDQHeadAni_Enter(GameObjectIns.Instance.A_HandVR, false, 0);
        GameObjectIns.Instance.SetCDQStartValue();

        GameObjectIns.Instance.Player.transform.GetChild(0).name = "00";
        GameObjectIns.Instance.SetObi(true);
        GameObjectIns.Instance.IsCDGCDQ = 1;

        GameObjectIns.Instance.CDK_Door_arrow.SetActive(false);
        GameObjectIns.Instance.CDQA_arrow.SetActive(false);
        GameObjectIns.Instance.CDQB_arrow.SetActive(false);

        GameObjectIns.Instance.SetHandModel(true);

        GameObjectIns.Instance.SetArrowListTrue();

        if (GameObjectIns.Instance.IsTestFalse)
        {
            GameObjectIns.Instance.SetCutTo(1f, 0f, 2f);
        }

        GameObjectIns.Instance.Canvas1.SetActive(false);
        GameObjectIns.Instance.SetCancelInvoke();
        GameObjectIns.Instance.Set_CDQ_CancelInvoke();
        GameObjectIns.Instance.CityBuswithInterior.transform.position = PlayerPosition.CityBuswithInteriorCDGPos;
        //GameObjectIns.Instance.SetPlayerHint_Canvas_Test("请扣扳机选择充电类型");
        GameObjectIns.Instance.Player.transform.localEulerAngles = Vector3.zero;
        GameObjectIns.Instance.Player.transform.position = PlayerPosition.PlayerCDGBusPos;
        print("配音：请把手柄靠近手刹，关掉钥匙断电");
        GameObjectIns.Instance.PlayLaShouShaAudio();
        print("33请拉起手刹！");
        PlayerAudio.Instance.PlayAudioClipTest(33);

        GameObjectIns.Instance.YiWu.SetActive(false);
        GameObjectIns.Instance.IsGoJKP = false;
        GameObjectIns.Instance.CDK_Door_arrow.SetActive(false);
        GameObjectIns.Instance.IsOverCDK_Door = true;

        GameObjectIns.Instance.CDQ_309_CDZ.transform.GetChild(2).transform.gameObject.SetActive(false);
        //GameObjectIns.Instance.CDQ_310_CDZ.transform.GetChild(2).transform.gameObject.SetActive(false);
        //GameObjectIns.Instance.CDQ_JiKongTai_CDXX_Parent.transform.GetChild(2).transform.GetChild(0).gameObject.SetActive(false);

        GameObjectIns.Instance.CDQ_JJTZ_L_arrow.SetActive(false);
        GameObjectIns.Instance.CDQ_JJTZ_R_arrow.SetActive(false);

        GameObjectIns.Instance.SetHight(GameObjectIns.Instance.YaoShi, false);
        GameObjectIns.Instance.SetHight(GameObjectIns.Instance.DangWei_N_GameObject, false);
        GameObjectIns.Instance.SetHight(GameObjectIns.Instance.ShouSha001, true);

        GameObjectIns.Instance.AKeyToStart_Button.transform.GetChild(0).GetChild(0).gameObject.GetComponent<MeshRenderer>().enabled = false;
    }
    

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            GameObjectIns.Instance.SetCancelInvoke();
            GameObjectIns.Instance.Set_CDQ_CancelInvoke();
            PlayerAudio.Instance.PlayBtnAudio();
            GameObjectIns.Instance.VibateController(0, 100, 1);
            GameObjectIns.Instance.DangWei_N = true;
            GameObjectIns.Instance.CloseShouSha(true, 2f);
            GameObjectIns.Instance.CloseYaoShi(true, 2f);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        print(other.transform.name);
        print(GameObjectIns.Instance.CDQ_Che_A + " " + GameObjectIns.Instance.CDQ_Che_B);
        if (GameObjectIns.Instance.IsPlayAudio == false)
        {
            if (GameObjectIns.Instance.IsPlayAni)
            {
                if (other.transform.name == "YaoShi")
                {
                    if (GameObjectIns.Instance.IsYaoShi && GameObjectIns.Instance.IsShouSha && GameObjectIns.Instance.DangWei_N)
                    {
                        GameObjectIns.Instance.CancelInvoke("PlayYaoShiAudio");
                        PlayerAudio.Instance.PlayBtnAudio();
                        GameObjectIns.Instance.VibateController(0, 100, 1);
                        print("钥匙");
                        GameObjectIns.Instance.SetHight(GameObjectIns.Instance.YaoShi, false);
                        GameObjectIns.Instance.CloseYaoShi(true, 2f);
                    }
                    else
                    {
                        print("钥匙已关闭");
                    }
                }
                else if (other.transform.name == "ShouSha001")
                {
                    if (GameObjectIns.Instance.IsShouSha == false)
                    {
                        GameObjectIns.Instance.CancelInvoke("PlayLaShouShaAudio");
                        PlayerAudio.Instance.PlayBtnAudio();
                        GameObjectIns.Instance.VibateController(0, 100, 1);
                        print("手刹");
                        GameObjectIns.Instance.SetHight(GameObjectIns.Instance.ShouSha001, false);
                        GameObjectIns.Instance.CloseShouSha(true, 2f);
                    }
                    else
                    {
                        print("手刹已关闭");
                    }
                }
                else if (other.name == "CDK_Door")
                {
                    GameObjectIns.Instance.SetHight(other.gameObject, false);
                    print("打开充电口");
                    if (GameObjectIns.Instance.IschargeDoor && GameObjectIns.Instance.IsOverCDK_Door)
                    {
                        GameObjectIns.Instance.CDK_Door_arrow.SetActive(false);
                        PlayerAudio.Instance.PlayBtnAudio();
                        GameObjectIns.Instance.VibateController(0, 100, 1);
                        GameObjectIns.Instance.IschargeDoor = false;
                        print("打开充电口");
                        GameObjectIns.Instance.DOTweenRotate(GameObjectIns.Instance.chargeDoor, SetRot.RotY_180, 2f, true);

                        GameObjectIns.Instance.SetHight(GameObjectIns.Instance.XuanBi_GunButtoonA, true);
                        GameObjectIns.Instance.SetHight(GameObjectIns.Instance.XuanBi_GunButtoonB, true);


                        GameObjectIns.Instance.YiWu.SetActive(true);
                        GameObjectIns.Instance.SetHight(GameObjectIns.Instance.YiWu, true);


                        print("37请检查充电口有无异物，如果有请先清理干净");
                        PlayerAudio.Instance.PlayAudioClipTest(37);

                        GameObjectIns.Instance.PlayerHint_Canvas.SetActive(true);
                        GameObjectIns.Instance.PlayerHint_Text.text = "请检查充电口有无异物，如果有请用手点击先清理干净";

                    }
                    else
                    {
                        print("充电口已打开");
                    }
                    if (GameObjectIns.Instance.CDQAIsCDLe && GameObjectIns.Instance.CDQBIsCDLe && GameObjectIns.Instance.CDQ_XuanBi_A && GameObjectIns.Instance.CDQ_XuanBi_B && GameObjectIns.Instance.IsOverCDK_Door)
                    {
                        GameObjectIns.Instance.IsOverCDK_Door = false;
                        print("关闭充电门");
                        PlayerAudio.Instance.PlayBtnAudio();
                        GameObjectIns.Instance.VibateController(0, 100, 1);
                        GameObjectIns.Instance.IschargeDoor = true;
                        GameObjectIns.Instance.DOTweenRotate(GameObjectIns.Instance.chargeDoor, SetRot.RotY180, 2f, true);

                        GameObjectIns.Instance.CDQGameOver();
                    }
                }
                else if (other.name == "XuanBi_GunL_B_Body")
                {
                    PlayerAudio.Instance.PlayBtnAudio();
                    GameObjectIns.Instance.VibateController(0, 100, 1);
                    print("碰到充电枪B");
                }
                else if (other.name == "XuanBi_GunL_A_Body")
                {
                    PlayerAudio.Instance.PlayBtnAudio();
                    GameObjectIns.Instance.VibateController(0, 100, 1);
                    print("碰到充电枪A");
                }
                else if (other.name == "XuanBi_RedBuL1")
                {
                    if (GameObjectIns.Instance.CDQBIsCD && GameObjectIns.Instance.CDQAIsCD)
                    {
                        GameObjectIns.Instance.Set_CDQ_CancelInvoke();

                        GameObjectIns.Instance.SetXuanBi_RedBu_Ani(true);

                        PlayerAudio.Instance.PlayBtnAudio();
                        GameObjectIns.Instance.VibateController(0, 100, 1);
                        GameObjectIns.Instance.PlayerHint_Canvas.SetActive(false);
                        GameObjectIns.Instance.CDQBIsCD = false;
                        GameObjectIns.Instance.CDQAIsCD = false;
                        print(GameObjectIns.Instance.CDQBIsCD);
                        //GameObjectIns.Instance.RightYellowLight.DisableKeyword("_EMISSION");
                        //GameObjectIns.Instance.RightGreenLight.DisableKeyword("_EMISSION");

                        GameObjectIns.Instance.SetCDQ_CDD(false, GameObjectIns.Instance.RightYellowLight);
                        GameObjectIns.Instance.SetCDQ_CDD(false, GameObjectIns.Instance.RightGreenLight);
                        GameObjectIns.Instance.SetCDQ_CDD(false, GameObjectIns.Instance.LeftYellowLight);
                        GameObjectIns.Instance.SetCDQ_CDD(false, GameObjectIns.Instance.LeftGreenLight);

                        print("关闭结束充电提示");
                        GameObjectIns.Instance.CDQB_arrow.SetActive(false);
                        GameObjectIns.Instance.CDQA_arrow.SetActive(false);
                        GameObjectIns.Instance.SetHight(GameObjectIns.Instance.XuanBi_GunButtoonB, true);
                        GameObjectIns.Instance.SetHight(GameObjectIns.Instance.XuanBi_GunButtoonA, true);
                    }
                    GameObjectIns.Instance.IsOverDoor();
                }
                else if (other.name == "XuanBi_RedBuR1")
                {
                    if (GameObjectIns.Instance.CDQAIsCD && GameObjectIns.Instance.CDQBIsCD)
                    {
                        GameObjectIns.Instance.Set_CDQ_CancelInvoke();

                        GameObjectIns.Instance.SetXuanBi_RedBu_Ani(false);

                        PlayerAudio.Instance.PlayBtnAudio();
                        GameObjectIns.Instance.VibateController(0, 100, 1);
                        GameObjectIns.Instance.PlayerHint_Canvas.SetActive(false);
                        GameObjectIns.Instance.CDQBIsCD = false;
                        GameObjectIns.Instance.CDQAIsCD = false;
                        print(GameObjectIns.Instance.CDQBIsCD);
                        //GameObjectIns.Instance.RightYellowLight.DisableKeyword("_EMISSION");
                        //GameObjectIns.Instance.RightGreenLight.DisableKeyword("_EMISSION");

                        GameObjectIns.Instance.SetCDQ_CDD(false, GameObjectIns.Instance.RightYellowLight);
                        GameObjectIns.Instance.SetCDQ_CDD(false, GameObjectIns.Instance.RightGreenLight);
                        GameObjectIns.Instance.SetCDQ_CDD(false, GameObjectIns.Instance.LeftYellowLight);
                        GameObjectIns.Instance.SetCDQ_CDD(false, GameObjectIns.Instance.LeftGreenLight);

                        print("关闭结束充电提示");
                        GameObjectIns.Instance.CDQB_arrow.SetActive(false);
                        GameObjectIns.Instance.CDQA_arrow.SetActive(false);
                        GameObjectIns.Instance.SetHight(GameObjectIns.Instance.XuanBi_GunButtoonB, true);
                        GameObjectIns.Instance.SetHight(GameObjectIns.Instance.XuanBi_GunButtoonA, true);
                    }

                    GameObjectIns.Instance.IsOverDoor();
                }
                else if (other.name == "YiWu")
                {
                    PlayerAudio.Instance.PlayBtnAudio();
                    GameObjectIns.Instance.VibateController(0, 100, 1);
                    GameObjectIns.Instance.YiWu.SetActive(false);
                    GameObjectIns.Instance.SetHight(GameObjectIns.Instance.YiWu, false);

                    GameObjectIns.Instance.PlayerHint_Text.text = "请将手柄靠近充电枪把手 请注意握枪方式 等动画播完再扣扳机取枪 将充电枪插入充电口时在手柄震动之后松扳机";
                    //PlayerAudio.Instance.PlayAudioClip(18);
                    print("26按下充电枪上方的按钮，垂直于充电桩方向将其拔下，然后将双枪插入车上的充电口，注意，时间间隔不要超过15秒");
                    PlayerAudio.Instance.PlayAudioClipTest(26);

                }
                else if (other.name == "DangWei_N")
                {
                    if (GameObjectIns.Instance.DangWei_N == false && GameObjectIns.Instance.IsShouSha)
                    {
                        GameObjectIns.Instance.CancelInvoke("PlayDangWeiNAudio");
                        GameObjectIns.Instance.SetHight(GameObjectIns.Instance.DangWei_N_GameObject.gameObject, false);
                        PlayerAudio.Instance.PlayBtnAudio();
                        GameObjectIns.Instance.VibateController(0, 100, 1);
                        GameObjectIns.Instance.DangWei_N = true;
                        GameObjectIns.Instance.IsYaoShiShouShaOver();
                    }

                }
                else if (other.name == "CDQ_309_Button")
                {
                    if (GameObjectIns.Instance.CDQAIsCD && GameObjectIns.Instance.CDQBIsCD)
                    {
                        PlayerAudio.Instance.PlayBtnAudio();
                        print("36界面显示充电电压、电流、当前SOC、充电电量等信息");
                        PlayerAudio.Instance.PlayAudioClipTest(36);
                        GameObjectIns.Instance.CDQ_JiKongTai_Btn_Parent.SetActive(false);
                        GameObjectIns.Instance.CDQ_JiKongTai_CDXX_Parent.SetActive(true);

                        GameObjectIns.Instance.CDQ_JiKongTai_CDXX_Parent.transform.GetChild(0).GetChild(0).GetComponent<Text>().text = "309";
                    }
                       
                }
                //else if (other.name == "CDQ_310_Button")
                //{
                //    PlayerAudio.Instance.PlayBtnAudio();
                //    print("36界面显示充电电压、电流、当前SOC、充电电量等信息");
                //    PlayerAudio.Instance.PlayAudioClipTest(36);
                //    GameObjectIns.Instance.CDQ_JiKongTai_Btn_Parent.SetActive(false);
                //    GameObjectIns.Instance.CDQ_JiKongTai_CDXX_Parent.SetActive(true);

                //    GameObjectIns.Instance.CDQ_JiKongTai_CDXX_Parent.transform.GetChild(0).GetChild(0).GetComponent<Text>().text = "310";
                //}
                else if (other.name == "CDQ_CDXX_Return_Button")
                {
                    PlayerAudio.Instance.PlayBtnAudio();
                    GameObjectIns.Instance.CDQ_JiKongTai_Btn_Parent.SetActive(true);
                    GameObjectIns.Instance.CDQ_JiKongTai_CDXX_Parent.SetActive(false);
                }
                else if (other.name == "CDQ_OVERCD_Button")
                {
                    if (GameObjectIns.Instance.CDQ_JiKongTai_CDXX_Parent.transform.GetChild(0).GetChild(0).GetComponent<Text>().text == "309")
                    {
                        if (GameObjectIns.Instance.CDQAIsCD && GameObjectIns.Instance.CDQBIsCD)
                        {
                            GameObjectIns.Instance.Set_CDQ_CancelInvoke();

                            GameObjectIns.Instance.CDQAIsCD = false;
                            GameObjectIns.Instance.CDQBIsCD = false;

                            PlayerAudio.Instance.PlayBtnAudio();
                            GameObjectIns.Instance.VibateController(0, 100, 1);

                            //GameObjectIns.Instance.LeftYellowLight.DisableKeyword("_EMISSION");
                            //GameObjectIns.Instance.LeftGreenLight.DisableKeyword("_EMISSION");

                            GameObjectIns.Instance.SetCDQ_CDD(false, GameObjectIns.Instance.LeftYellowLight);
                            GameObjectIns.Instance.SetCDQ_CDD(false, GameObjectIns.Instance.LeftGreenLight);
                            GameObjectIns.Instance.SetCDQ_CDD(false, GameObjectIns.Instance.RightYellowLight);
                            GameObjectIns.Instance.SetCDQ_CDD(false, GameObjectIns.Instance.RightGreenLight);

                            GameObjectIns.Instance.CDQ_309_KXZ.SetActive(true);
                            GameObjectIns.Instance.CDQ_309_CDZ.SetActive(false);
                            GameObjectIns.Instance.CDQ_310_KXZ.SetActive(true);
                            GameObjectIns.Instance.CDQ_310_CDZ.SetActive(false);

                            print("关闭结束充电提示");
                            GameObjectIns.Instance.CDQA_arrow.SetActive(false);
                            GameObjectIns.Instance.CDQB_arrow.SetActive(false);
                            GameObjectIns.Instance.SetHight(GameObjectIns.Instance.XuanBi_GunButtoonA, true);
                            GameObjectIns.Instance.SetHight(GameObjectIns.Instance.XuanBi_GunButtoonB, true);

                            GameObjectIns.Instance.CDQJKPReturnAudio();
                            GameObjectIns.Instance.PlayerHint_Canvas.SetActive(true);
                            GameObjectIns.Instance.PlayerHint_Text.text = "按左手手柄Y键可以查看集控屏，按左手手柄上的X键返回原地";
                        }
                    }
                    else
                    {
                        //if (GameObjectIns.Instance.CDQBIsCD)
                        //{
                        //    GameObjectIns.Instance.CDQBIsCD = false;

                        //    PlayerAudio.Instance.PlayBtnAudio();
                        //    GameObjectIns.Instance.VibateController(0, 100, 1);
                        //    //GameObjectIns.Instance.RightYellowLight.DisableKeyword("_EMISSION");
                        //    //GameObjectIns.Instance.RightGreenLight.DisableKeyword("_EMISSION");

                        //    GameObjectIns.Instance.SetCDQ_CDD(false, GameObjectIns.Instance.RightYellowLight);
                        //    GameObjectIns.Instance.SetCDQ_CDD(false, GameObjectIns.Instance.RightGreenLight);

                        //    GameObjectIns.Instance.CDQ_310_KXZ.SetActive(true);
                        //    //GameObjectIns.Instance.CDQ_310_CDZ.SetActive(false);

                        //    if (GameObjectIns.Instance.CDQ_Che_A || GameObjectIns.Instance.CDQ_Che_B)
                        //    {
                        //        print("关闭结束充电提示");
                        //        GameObjectIns.Instance.CDQB_arrow.SetActive(false);
                        //        GameObjectIns.Instance.SetHight(GameObjectIns.Instance.XuanBi_GunButtoonB, true);
                        //    }
                        //    else
                        //    {
                        //        print("Bug");
                        //    }

                        //    GameObjectIns.Instance.CDQJKPReturnAudio();
                        //}
                    }
                    PlayerAudio.Instance.PlayBtnAudio();
                    GameObjectIns.Instance.CDQ_JiKongTai_Btn_Parent.SetActive(true);
                    GameObjectIns.Instance.CDQ_JiKongTai_CDXX_Parent.SetActive(false);
                }
                else if (other.name == "XuanBi_GunL_B_Child")
                {
                    PlayerAudio.Instance.PlayBtnAudio();
                    GameObjectIns.Instance.VibateController(0, 100, 1);
                    if (GameObjectIns.Instance.CDQ_XuanBi_B && IsHeadQiang()&& GameObjectIns.Instance.Player.transform.GetChild(0).name == "00")
                    {
                        if (GameObjectIns.Instance.XuanBi_GunL_B_Parent.transform.Find("XuanBi_GunL_B") != null)
                        {
                            GameObjectIns.Instance.B_HandVR.transform.GetComponent<Animator>().SetBool("IsHold", true);
                            GameObjectIns.Instance.B_HandVR.transform.GetComponent<Animator>().SetBool("IsPine", false);
                            GameObjectIns.Instance.B_HandVR.transform.GetChild(0).transform.gameObject.SetActive(true);
                            GameObjectIns.Instance.B_HandVR.transform.GetChild(1).transform.gameObject.SetActive(true);
                            GameObjectIns.Instance.Play_L_Hand_Mesh.SetActive(false);
                            GameObjectIns.Instance.Play_R_Hand_Mesh.SetActive(false);
                        }
                        else if (GameObjectIns.Instance.XuanBi_GunL_B_Parent.transform.Find("XuanBi_GunL_A") != null)
                        {
                            GameObjectIns.Instance.A_HandVR.transform.GetComponent<Animator>().SetBool("IsHold", true);
                            GameObjectIns.Instance.A_HandVR.transform.GetComponent<Animator>().SetBool("IsPine", false);
                            GameObjectIns.Instance.A_HandVR.transform.GetChild(0).transform.gameObject.SetActive(true);
                            GameObjectIns.Instance.A_HandVR.transform.GetChild(1).transform.gameObject.SetActive(true);
                            GameObjectIns.Instance.Play_L_Hand_Mesh.SetActive(false);
                            GameObjectIns.Instance.Play_R_Hand_Mesh.SetActive(false);
                        }
                        else
                        {
                            print("   XuanBi_GunL_B_Parent");
                        }
                    }

                }
                else if (other.name == "XuanBi_GunL_A_Child")
                {
                    PlayerAudio.Instance.PlayBtnAudio();
                    GameObjectIns.Instance.VibateController(0, 100, 1);
                    if (GameObjectIns.Instance.CDQ_XuanBi_A && IsHeadQiang()&& GameObjectIns.Instance.Player.transform.GetChild(0).name == "00")
                    {
                        if (GameObjectIns.Instance.XuanBi_GunL_A_Parent.transform.Find("XuanBi_GunL_B") != null)
                        {
                            GameObjectIns.Instance.B_HandVR.transform.GetComponent<Animator>().SetBool("IsHold", true);
                            GameObjectIns.Instance.B_HandVR.transform.GetComponent<Animator>().SetBool("IsPine", false);
                            GameObjectIns.Instance.B_HandVR.transform.GetChild(0).transform.gameObject.SetActive(true);
                            GameObjectIns.Instance.B_HandVR.transform.GetChild(1).transform.gameObject.SetActive(true);
                            GameObjectIns.Instance.Play_L_Hand_Mesh.SetActive(false);
                            GameObjectIns.Instance.Play_R_Hand_Mesh.SetActive(false);
                        }
                        else if (GameObjectIns.Instance.XuanBi_GunL_A_Parent.transform.Find("XuanBi_GunL_A") != null)
                        {
                            GameObjectIns.Instance.A_HandVR.transform.GetComponent<Animator>().SetBool("IsHold", true);
                            GameObjectIns.Instance.A_HandVR.transform.GetComponent<Animator>().SetBool("IsPine", false);
                            GameObjectIns.Instance.A_HandVR.transform.GetChild(0).transform.gameObject.SetActive(true);
                            GameObjectIns.Instance.A_HandVR.transform.GetChild(1).transform.gameObject.SetActive(true);
                            GameObjectIns.Instance.Play_L_Hand_Mesh.SetActive(false);
                            GameObjectIns.Instance.Play_R_Hand_Mesh.SetActive(false);
                        }
                        else
                        {
                            print("   XuanBi_GunL_A_Parent");
                        }
                    }
                }
                else if (other.name == "Che_A_Child")
                {
                    PlayerAudio.Instance.PlayBtnAudio();
                    GameObjectIns.Instance.VibateController(0, 100, 1);
                    print(GameObjectIns.Instance.CDQ_Che_A + " " + IsHeadQiang());
                    if (GameObjectIns.Instance.CDQ_Che_A && IsHeadQiang())
                    {
                        print("1");
                        if (GameObjectIns.Instance.Che_A_Parent.transform.Find("XuanBi_GunL_B") != null)
                        {
                            print("2");
                            GameObjectIns.Instance.SetCDQHeadAni_Enter( GameObjectIns.Instance.B_HandVR, true, 0);
                        }
                        else if (GameObjectIns.Instance.Che_A_Parent.transform.Find("XuanBi_GunL_A") != null)
                        {
                            print("3");
                            GameObjectIns.Instance.SetCDQHeadAni_Enter( GameObjectIns.Instance.A_HandVR, true, 0);
                        }
                        else
                        {
                            print("   Che_A_Parent");
                        }
                    }
                }
                else if (other.name == "Che_B_Child")
                {
                    PlayerAudio.Instance.PlayBtnAudio();
                    GameObjectIns.Instance.VibateController(0, 100, 1);
                    if (GameObjectIns.Instance.CDQ_Che_B && IsHeadQiang())
                    {
                        if (GameObjectIns.Instance.Che_B_Parent.transform.Find("XuanBi_GunL_B") != null)
                        {
                            GameObjectIns.Instance.SetCDQHeadAni_Enter( GameObjectIns.Instance.B_HandVR, true, 0);
                        }
                        else if (GameObjectIns.Instance.Che_B_Parent.transform.Find("XuanBi_GunL_A") != null)
                        {
                            GameObjectIns.Instance.SetCDQHeadAni_Enter( GameObjectIns.Instance.A_HandVR, true, 0);
                        }
                        else
                        {
                            print("   Che_B_Parent");
                        }
                    }
                }
                else if (other.name == "XuanBi_OringeBuR1" || other.name == "XuanBi_OringeBuL1")
                {
                    GameObjectIns.Instance.CDQ_JJTZ_L_arrow.SetActive(false);
                    GameObjectIns.Instance.CDQ_JJTZ_R_arrow.SetActive(false);

                    PlayerAudio.Instance.PlayBtnAudio();
                    GameObjectIns.Instance.VibateController(0, 100, 1);
                    if (GameObjectIns.Instance.Player.transform.GetChild(0).name == "22")
                    {
                        GameObjectIns.Instance.Player.transform.GetChild(0).name = "cdq";
                        GameObjectIns.Instance.CDQ_XuanBi_OringeBu();
                    }
                    else if (GameObjectIns.Instance.Player.transform.GetChild(0).name == "33")
                    {
                        GameObjectIns.Instance.Player.transform.GetChild(0).name = "cdq";
                        GameObjectIns.Instance.CDQ_XuanBi_OringeBu1();
                    }
                }
            }
            else
            {
                print("正在播放动画");
            }
          
        }
        else
        {
            print("正在播放声音 稍后操作");
        }
        print(GameObjectIns.Instance.CDQ_Che_A + " " + GameObjectIns.Instance.CDQ_Che_B);
    }

    private bool is_KeyDown = true;

    private void OnTriggerStay(Collider other)
    {
        if (other.name == "XuanBi_GunL_B_Body" && other.name == "XuanBi_GunL_A_Body")
        {

        }
        if (GameObjectIns.Instance.IschargeDoor == false && GameObjectIns.Instance.IsPlayAudio == false && GameObjectIns.Instance.IsPlayAni && !GameObjectIns.Instance.YiWu.activeInHierarchy && GameObjectIns.Instance.Player.transform.GetChild(0).name == "00")
        {
            if (Controller.UPvr_GetKeyDown(0, Pvr_KeyCode.TRIGGER) || Controller.UPvr_GetKeyDown(1, Pvr_KeyCode.TRIGGER) || Input.GetKeyDown(KeyCode.N) && is_KeyDown)
            {
                //异物未清除
                if (GameObjectIns.Instance.YiWu.activeInHierarchy)
                {
                    PlayerAudio.Instance.PlayBtnAudio();
                    GameObjectIns.Instance.VibateController(0, 100, 1);
                    print("异物未清除");
                    GameObjectIns.Instance.PlayerHint_Canvas.SetActive(true);
                    GameObjectIns.Instance.PlayerHint_Text.text = "异物未清除";
                    return;
                }
                 
                if (other.name == "XuanBi_GunL_B_Body")
                {
                    is_KeyDown = false;
                    print("!!!!!!!! 1  " + other.name);
                    //充电枪B正在充电
                    if (GameObjectIns.Instance.CDQBIsCD)
                    {
                        PlayerAudio.Instance.PlayBtnAudio();
                        GameObjectIns.Instance.VibateController(0, 100, 1);
                        GameObjectIns.Instance.PlayerHint_Canvas.SetActive(true);
                        GameObjectIns.Instance.PlayerHint_Text.text = "充电枪B正在充电不能取下\n请先结束充电 ";
                        print("充电枪B 正在充电 不能取下");
                        return;
                    }

                    if (IsHeadQiang())
                    {
                        print("按下了 充电枪B");
                        GameObjectIns.Instance.PlayerHint_Canvas.SetActive(false);
                        PlayerAudio.Instance.PlayBtnAudio();
                        GameObjectIns.Instance.VibateController(0, 100, 1);
                        GameObjectIns.Instance.SetHight(GameObjectIns.Instance.XuanBi_GunButtoonB, false);
                        if (GameObjectIns.Instance.CDQ_B.transform.parent.name == "XuanBi_GunL_B_Parent")
                        {
                            GameObjectIns.Instance.CDQ_XuanBi_B = false;
                        }
                        else if (GameObjectIns.Instance.CDQ_B.transform.parent.name == "XuanBi_GunL_A_Parent")
                        {
                            GameObjectIns.Instance.CDQ_XuanBi_A = false;
                        }
                        else if (GameObjectIns.Instance.CDQ_B.transform.parent.name == "Che_B_Parent")
                        {
                            GameObjectIns.Instance.CDQ_Che_B = false;
                        }
                        else if (GameObjectIns.Instance.CDQ_B.transform.parent.name == "Che_A_Parent")
                        {
                            GameObjectIns.Instance.CDQ_Che_A = false;
                        }
                        else
                        {
                            return;
                        }
                        GameObjectIns.Instance.CDQ_B.transform.parent = transform;

                        GameObjectIns.Instance.XuanBi_GunButtoonB.transform.localEulerAngles = SetRot.RotX90;
                    }
                    else
                    {
                        print("已经拿枪了");
                    }
                }
                if (other.name == "XuanBi_GunL_A_Body")
                {
                    is_KeyDown = false;
                    print("!!!!!!!! 2  " + other.name);
                    //充电枪A正在充电
                    if (GameObjectIns.Instance.CDQAIsCD)
                    {
                        PlayerAudio.Instance.PlayBtnAudio();
                        GameObjectIns.Instance.VibateController(0, 100, 1);
                        GameObjectIns.Instance.PlayerHint_Canvas.SetActive(true);
                        GameObjectIns.Instance.PlayerHint_Text.text = "充电枪A正在充电不能取下\n请先结束充电 ";
                        print("充电枪A 正在充电 不能取下");
                        return;
                    }

                    if (IsHeadQiang())
                    {
                        print("按下了 充电枪A");
                        GameObjectIns.Instance.PlayerHint_Canvas.SetActive(false);
                        PlayerAudio.Instance.PlayBtnAudio();
                        GameObjectIns.Instance.VibateController(0, 100, 1);
                        GameObjectIns.Instance.SetHight(GameObjectIns.Instance.XuanBi_GunButtoonA, false);
                        if (GameObjectIns.Instance.CDQ_A.transform.parent.name == "XuanBi_GunL_B_Parent")
                        {
                            GameObjectIns.Instance.CDQ_XuanBi_B = false;
                        }
                        else if (GameObjectIns.Instance.CDQ_A.transform.parent.name == "XuanBi_GunL_A_Parent")
                        {
                            GameObjectIns.Instance.CDQ_XuanBi_A = false;
                        }
                        else if (GameObjectIns.Instance.CDQ_A.transform.parent.name == "Che_B_Parent")
                        {
                            GameObjectIns.Instance.CDQ_Che_B = false;
                        }
                        else if (GameObjectIns.Instance.CDQ_A.transform.parent.name == "Che_A_Parent")
                        {
                            GameObjectIns.Instance.CDQ_Che_A = false;
                        }
                        else
                        {
                            return;
                        }
                        GameObjectIns.Instance.CDQ_A.transform.parent = transform;

                        GameObjectIns.Instance.XuanBi_GunButtoonA.transform.localEulerAngles = SetRot.RotX90;
                    }
                    else
                    {
                        print("已经拿枪了");
                    }
                  
                }
               
            }
            if (Controller.UPvr_GetKeyUp(0, Pvr_KeyCode.TRIGGER) || Controller.UPvr_GetKeyUp(1, Pvr_KeyCode.TRIGGER) || Input.GetKeyUp(KeyCode.N) && !is_KeyDown)
            {
                print("2  " + other.name);
                print("松开了");
                if (other.name == "XuanBi_GunL_B_Child")
                {
                    if (!GameObjectIns.Instance.XuanBi_GunL_B_Parent.transform.Find("XuanBi_GunL_B") && !GameObjectIns.Instance.XuanBi_GunL_B_Parent.transform.Find("XuanBi_GunL_A"))
                    {
                        if (transform.Find("XuanBi_GunL_B"))
                        {
                            is_KeyDown = true;
                            print("CDQ_B 可以放到悬臂B");
                            PlayerAudio.Instance.PlayBtnAudio();
                            GameObjectIns.Instance.VibateController(0, 100, 1);
                            GameObjectIns.Instance.CDQ_XuanBi_B = true;
                            GameObjectIns.Instance.CDQ_B.transform.parent = GameObjectIns.Instance.XuanBi_GunL_B_Parent.transform;
                            GameObjectIns.Instance.CDQ_B.transform.localPosition = Vector3.zero;
                            GameObjectIns.Instance.CDQ_B.transform.localEulerAngles = Vector3.zero;

                            GameObjectIns.Instance.XuanBi_GunButtoonB.transform.localEulerAngles = SetRot.RotX87;
                        }
                        else if (transform.Find("XuanBi_GunL_A"))
                        {
                            is_KeyDown = true;
                            print("CDQ_A 可以放到悬臂B");
                            PlayerAudio.Instance.PlayBtnAudio();
                            GameObjectIns.Instance.VibateController(0, 100, 1);
                            GameObjectIns.Instance.CDQ_XuanBi_B = true;
                            GameObjectIns.Instance.CDQ_A.transform.parent = GameObjectIns.Instance.XuanBi_GunL_B_Parent.transform;
                            GameObjectIns.Instance.CDQ_A.transform.localPosition = Vector3.zero;
                            GameObjectIns.Instance.CDQ_A.transform.localEulerAngles = Vector3.zero;

                            GameObjectIns.Instance.XuanBi_GunButtoonA.transform.localEulerAngles = SetRot.RotX87;
                        }
                       
                    }
                    else
                    {
                        print("悬臂B下有充电枪");
                    }
                }
                else if (other.name == "XuanBi_GunL_A_Child")
                {
                    if (!GameObjectIns.Instance.XuanBi_GunL_A_Parent.transform.Find("XuanBi_GunL_B") && !GameObjectIns.Instance.XuanBi_GunL_A_Parent.transform.Find("XuanBi_GunL_A"))
                    {
                       
                        if (transform.Find("XuanBi_GunL_B"))
                        {
                            is_KeyDown = true;
                            print("CDQ_B 可以放到悬臂A");
                            PlayerAudio.Instance.PlayBtnAudio();
                            GameObjectIns.Instance.VibateController(0, 100, 1);
                            GameObjectIns.Instance.CDQ_XuanBi_A = true;
                            GameObjectIns.Instance.CDQ_B.transform.parent = GameObjectIns.Instance.XuanBi_GunL_A_Parent.transform;
                            GameObjectIns.Instance.CDQ_B.transform.localPosition = Vector3.zero;
                            GameObjectIns.Instance.CDQ_B.transform.localEulerAngles = Vector3.zero;

                            GameObjectIns.Instance.XuanBi_GunButtoonB.transform.localEulerAngles = SetRot.RotX87;

                            print(" GameObjectIns.Instance.CDQ_XuanBi_A" + GameObjectIns.Instance.CDQ_XuanBi_A);
                        }
                        else if (transform.Find("XuanBi_GunL_A"))
                        {
                            is_KeyDown = true;
                            print("CDQ_A 可以放到悬臂A");
                            PlayerAudio.Instance.PlayBtnAudio();
                            GameObjectIns.Instance.VibateController(0, 100, 1);
                            GameObjectIns.Instance.CDQ_XuanBi_A = true;
                            GameObjectIns.Instance.CDQ_A.transform.parent = GameObjectIns.Instance.XuanBi_GunL_A_Parent.transform;
                            GameObjectIns.Instance.CDQ_A.transform.localPosition = Vector3.zero;
                            GameObjectIns.Instance.CDQ_A.transform.localEulerAngles = Vector3.zero;

                            GameObjectIns.Instance.XuanBi_GunButtoonA.transform.localEulerAngles = SetRot.RotX87;
                        }
                    }
                    else
                    {
                        print("悬臂A下有充电枪");
                    }
                }
                else if (other.name == "Che_A_Child")
                {
                    if (!GameObjectIns.Instance.Che_A_Parent.transform.Find("XuanBi_GunL_B") && !GameObjectIns.Instance.Che_A_Parent.transform.Find("XuanBi_GunL_A"))
                    {
                        if (transform.Find("XuanBi_GunL_B"))
                        {
                            if (GameObjectIns.Instance.CDQBIsCDLe)
                            {
                                print("充电枪B已经充电 操作错误");
                                return;
                            }

                            is_KeyDown = true;
                            GameObjectIns.Instance.CDQBIsCD = true;
                            print("CDQ_B 可以放到车A");
                            PlayerAudio.Instance.PlayBtnAudio();
                            GameObjectIns.Instance.VibateController(0, 100, 1);
                            GameObjectIns.Instance.CDQ_Che_A = true;
                            GameObjectIns.Instance.CDQ_B.transform.parent = GameObjectIns.Instance.Che_A_Parent.transform;
                            GameObjectIns.Instance.CDQ_B.transform.localPosition = Vector3.zero;
                            GameObjectIns.Instance.CDQ_B.transform.localEulerAngles = SetRot.RotZ90 ;
                            GameObjectIns.Instance.DengHit();
                            GameObjectIns.Instance.SetCDQ_CDD(true, GameObjectIns.Instance.RightYellowLight);

                            GameObjectIns.Instance.XuanBi_GunButtoonB.transform.localEulerAngles = SetRot.RotX87;
                        }
                        else if (transform.Find("XuanBi_GunL_A"))
                        {
                            if (GameObjectIns.Instance.CDQAIsCDLe)
                            {
                                print("充电枪A已经充电 操作错误");
                                return;
                            }

                            is_KeyDown = true;
                            print("CDQ_A 可以放到车A");
                            PlayerAudio.Instance.PlayBtnAudio();
                            GameObjectIns.Instance.VibateController(0, 100, 1);
                            GameObjectIns.Instance.CDQ_Che_A = true;
                            GameObjectIns.Instance.CDQ_A.transform.parent = GameObjectIns.Instance.Che_A_Parent.transform;
                            GameObjectIns.Instance.CDQ_A.transform.localPosition = Vector3.zero;
                            GameObjectIns.Instance.CDQ_A.transform.localEulerAngles = SetRot.RotZ90;
                            GameObjectIns.Instance.DengHit();

                            GameObjectIns.Instance.SetCDQ_CDD(true, GameObjectIns.Instance.LeftYellowLight);

                            GameObjectIns.Instance.XuanBi_GunButtoonA.transform.localEulerAngles = SetRot.RotX87;
                        }
                    }
                    else
                    {
                        print("车A下有充电枪");
                    }
                }
                else if (other.name == "Che_B_Child")
                {
                    if (!GameObjectIns.Instance.Che_B_Parent.transform.Find("XuanBi_GunL_B") && !GameObjectIns.Instance.Che_B_Parent.transform.Find("XuanBi_GunL_A"))
                    {
                        
                        if (transform.Find("XuanBi_GunL_B"))
                        {
                            if (GameObjectIns.Instance.CDQBIsCDLe)
                            {
                                print("充电枪B已经充电 操作错误");
                                return;
                            }

                            is_KeyDown = true;
                            GameObjectIns.Instance.CDQBIsCD = true;
                            print("CDQ_B 可以放到车B");
                            PlayerAudio.Instance.PlayBtnAudio();
                            GameObjectIns.Instance.VibateController(0, 100, 1);
                            GameObjectIns.Instance.CDQ_Che_B = true;
                            GameObjectIns.Instance.CDQ_B.transform.parent = GameObjectIns.Instance.Che_B_Parent.transform;
                            GameObjectIns.Instance.CDQ_B.transform.localPosition = Vector3.zero;
                            GameObjectIns.Instance.CDQ_B.transform.localEulerAngles = SetRot.RotZ90;
                            GameObjectIns.Instance.DengHit();

                            GameObjectIns.Instance.SetCDQ_CDD(true, GameObjectIns.Instance.RightYellowLight);

                            GameObjectIns.Instance.XuanBi_GunButtoonB.transform.localEulerAngles =SetRot.RotX87;
                        }
                        else if (transform.Find("XuanBi_GunL_A"))
                        {
                            if (GameObjectIns.Instance.CDQAIsCDLe)
                            {
                                print("充电枪A已经充电 操作错误");
                                return;
                            }

                            is_KeyDown = true;
                            print("CDQ_A 可以放到车B");
                            PlayerAudio.Instance.PlayBtnAudio();
                            GameObjectIns.Instance.VibateController(0, 100, 1);
                            GameObjectIns.Instance.CDQ_Che_B = true;
                            GameObjectIns.Instance.CDQ_A.transform.parent = GameObjectIns.Instance.Che_B_Parent.transform;
                            GameObjectIns.Instance.CDQ_A.transform.localPosition = Vector3.zero;
                            GameObjectIns.Instance.CDQ_A.transform.localEulerAngles = SetRot.RotZ90;
                            GameObjectIns.Instance.DengHit();

                            GameObjectIns.Instance.SetCDQ_CDD(true, GameObjectIns.Instance.LeftYellowLight);

                            GameObjectIns.Instance.XuanBi_GunButtoonA.transform.localEulerAngles = SetRot.RotX87;
                        }
                    }
                    else
                    {
                        print("车B下有充电枪");
                    }
                }
                print(GameObjectIns.Instance.CDQ_XuanBi_A + "\n" + GameObjectIns.Instance.CDQ_XuanBi_B+ "\n" + GameObjectIns.Instance.CDQ_Che_A + "\n" + GameObjectIns.Instance.CDQ_Che_B);
                if (GameObjectIns.Instance.CDQ_XuanBi_A && GameObjectIns.Instance.CDQ_XuanBi_B && GameObjectIns.Instance.CDQAIsCDLe && GameObjectIns.Instance.CDQBIsCDLe)
                {
                    //GameObjectIns.Instance.CDQA_arrow.SetActive(true);
                    //GameObjectIns.Instance.CDQB_arrow.SetActive(true);
                    GameObjectIns.Instance.SetHight(GameObjectIns.Instance.chargeDoor.transform.GetChild(0).gameObject, true);
                    print("22请关闭充电口门");
                    PlayerAudio.Instance.PlayAudioClipTest(22);
                }
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        print(other.name +" "+ GameObjectIns.Instance.IsNaQiang);
        if (IsHeadQiang() && GameObjectIns.Instance.Player.transform.GetChild(0).name == "00")
        {
            if (other.name == "XuanBi_GunL_B_Child")
            {
                if (GameObjectIns.Instance.XuanBi_GunL_B_Parent.transform.Find("XuanBi_GunL_B") != null)
                {
                    GameObjectIns.Instance.SetCDQHeadAni_Enter(GameObjectIns.Instance.B_HandVR, false,0);
                }
                else if (GameObjectIns.Instance.XuanBi_GunL_B_Parent.transform.Find("XuanBi_GunL_A") != null)
                {
                    GameObjectIns.Instance.SetCDQHeadAni_Enter( GameObjectIns.Instance.A_HandVR, false, 0);
                }
                else
                {
                    print("···XuanBi_GunL_B_Parent");
                }
            }
            else if (other.name == "XuanBi_GunL_A_Child")
            {
                if (GameObjectIns.Instance.XuanBi_GunL_A_Parent.transform.Find("XuanBi_GunL_B") != null)
                {
                    GameObjectIns.Instance.SetCDQHeadAni_Enter( GameObjectIns.Instance.B_HandVR, false, 0);
                }
                else if (GameObjectIns.Instance.XuanBi_GunL_A_Parent.transform.Find("XuanBi_GunL_A") != null)
                {
                    GameObjectIns.Instance.SetCDQHeadAni_Enter( GameObjectIns.Instance.A_HandVR, false, 0);
                }
                else
                {
                    print("···XuanBi_GunL_A_Parent");
                }
            }
            else if (other.name == "Che_A_Child" )
            {
                if (GameObjectIns.Instance.Che_A_Parent.transform.Find("XuanBi_GunL_B") != null)
                {
                    GameObjectIns.Instance.SetCDQHeadAni_Enter(GameObjectIns.Instance.B_HandVR, false, 0);
                }
                else if (GameObjectIns.Instance.Che_A_Parent.transform.Find("XuanBi_GunL_A") != null)
                {
                    GameObjectIns.Instance.SetCDQHeadAni_Enter(GameObjectIns.Instance.A_HandVR, false, 0);
                }
                else
                {
                    print("···Che_A_Parent");
                }
            }
            else if (other.name == "Che_B_Child" )
            {
                if (GameObjectIns.Instance.Che_B_Parent.transform.Find("XuanBi_GunL_B") != null)
                {
                    GameObjectIns.Instance.SetCDQHeadAni_Enter(GameObjectIns.Instance.B_HandVR, false, 0);
                }
                else if (GameObjectIns.Instance.Che_B_Parent.transform.Find("XuanBi_GunL_A") != null)
                {
                    GameObjectIns.Instance.SetCDQHeadAni_Enter(GameObjectIns.Instance.A_HandVR, false, 0);
                }
                else
                {
                    print("···Che_B_Parent");
                }
            }
        }
       
    }
    /// <summary>
    /// 手里面是否拿枪
    /// </summary>
    /// <returns></returns>
    private bool IsHeadQiang()
    {
        return !transform.Find("XuanBi_GunL_B") && !transform.Find("XuanBi_GunL_A");
    }
}