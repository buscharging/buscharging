﻿using Pvr_UnitySDKAPI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RightYaoShiTrigger : MonoBehaviour
{
    // Start is called before the first frame update

    /// <summary>
    /// 参考LeftShouCdgTrigger脚本注释
    /// </summary>
    void Start()
    {
        GameObjectIns.Instance.IsDownBus = false;
        GameObjectIns.Instance.LeftCDGgreenLight.EnableKeyword("_EMISSION");
        GameObjectIns.Instance.RightCDRedLight.DisableKeyword("_EMISSION");
        GameObjectIns.Instance.IsZYSXOne = true;
        if (GameObjectIns.Instance.vr_cartoon_hand_prefab_Left.activeInHierarchy == false)
        {
            GameObjectIns.Instance.vr_cartoon_hand_prefab_Left.SetActive(true);
        }

        if (GameObjectIns.Instance.IsTest)
        {
            GameObjectIns.Instance.SetCutTo(1f, 0f, 2f);
        }

        CancelInvoke();
        GameObjectIns.Instance.TwoRecharge_Btn.transform.GetChild(3).gameObject.SetActive(true);
        GameObjectIns.Instance.CDGJL_arrow.SetActive(false);
        GameObjectIns.Instance.SetHight(GameObjectIns.Instance.CDG_arrow, false);
        GameObjectIns.Instance.CDG_arrow.SetActive(false);
        //GameObjectIns.Instance.RechargeTime = 0f;
        GameObjectIns.Instance.IsRecharge = false;

        GameObjectIns.Instance.IsRecharge30 = false;
        GameObjectIns.Instance.TwoRecharge_Img.transform.GetChild(0).gameObject.SetActive(true);
        GameObjectIns.Instance.TwoRecharge_Img.transform.GetChild(1).gameObject.SetActive(false);
        GameObjectIns.Instance.BG_Image0.SetActive(true);
        GameObjectIns.Instance.BG_Image1.SetActive(false);
        if (GameObjectIns.Instance.TwoRecharge_Img.activeInHierarchy)
        {
            GameObjectIns.Instance.TwoRecharge_Img.SetActive(false);
        }
        GameObjectIns.Instance.CDGIsDown = false;
        GameObjectIns.Instance.Canvas2.SetActive(true);
        GameObjectIns.Instance.SetC2ChildFalse();
        GameObjectIns.Instance.AKeyToStart_Button.gameObject.SetActive(true);

        if (GameObjectIns.Instance.IsYaoShi == false)
        {
            GameObjectIns.Instance.CloseYaoShi(false, 0f);
        }
        if (GameObjectIns.Instance.IsShouSha)
        {
            GameObjectIns.Instance.CloseShouSha(false, 0f);
        }

        GameObjectIns.Instance.DangWei_N = false;
        GameObjectIns.Instance.SetHight(GameObjectIns.Instance.ShouSha001, true);
        GameObjectIns.Instance.SetHight(GameObjectIns.Instance.DangWei_N_GameObject, false);
        GameObjectIns.Instance.SetHight(GameObjectIns.Instance.YaoShi, false);

        //GameObjectIns.Instance.SetHight(GameObjectIns.Instance.CDG_arrow, true);
        //GameObjectIns.Instance.SetHight(GameObjectIns.Instance.CDGMark_Plane, true);
        GameObjectIns.Instance.CDQ_JJTZ_L_arrow.SetActive(false);
        GameObjectIns.Instance.CDQ_JJTZ_R_arrow.SetActive(false);
        //隐藏手柄模型 射线
        GameObjectIns.Instance.SetHandModel(true);

        GameObjectIns.Instance.CDGSS_Over_Button.gameObject.SetActive(false);
        GameObjectIns.Instance.CDGSS_Over_Button1.gameObject.SetActive(false);

    }

    private void Update()
    {
        if (Controller.UPvr_GetKeyDown(0, Pvr_KeyCode.Y) || Controller.UPvr_GetKeyDown(1, Pvr_KeyCode.Y) || Input.GetKeyDown(KeyCode.Y))
        {
            if (GameObjectIns.Instance.IsDownBus == false)
            {
                if (GameObjectIns.Instance.Player.transform.GetChild(0).name == "cdg0")
                {
                    GameObjectIns.Instance.Player.transform.GetChild(0).name = "cdg1";
                }
                else
                {
                    GameObjectIns.Instance.CDGSS_Over_Button.gameObject.SetActive(false);

                    GameObjectIns.Instance.CDGAniDownOverSetHandTrue(false, 0f);
                }
                GameObjectIns.Instance.IsDownBus = true;
                GameObjectIns.Instance.Invoke("SetIsDownBus", 2f);
                GameObjectIns.Instance.PlayerHint_Canvas.SetActive(false);

                GameObjectIns.Instance.SetCutTo(0f, 1f, 2f);

                Invoke("SetPlayPosDown", 2f);
            }
        }
        else if (Controller.UPvr_GetKeyDown(0, Pvr_KeyCode.X) || Controller.UPvr_GetKeyDown(1, Pvr_KeyCode.X) || Input.GetKeyDown(KeyCode.X))
        {
            if (GameObjectIns.Instance.IsDownBus == false)
            {
                GameObjectIns.Instance.IsDownBus = true;
                GameObjectIns.Instance.Invoke("SetIsDownBus", 2f);

                if (GameObjectIns.Instance.Player.transform.GetChild(0).name == "0")
                {
                    GameObjectIns.Instance.CancelInvoke("SetIsDownBus");
                }
                GameObjectIns.Instance.SetCutTo(0f, 1f, 2f);
                Invoke("SetPlayPosUP", 2f);
            }
        }

        if (Input.GetKeyDown(KeyCode.T) || Controller.UPvr_GetKeyLongPressed(0, Pvr_KeyCode.APP)|| Controller.UPvr_GetKeyLongPressed(1, Pvr_KeyCode.APP))
        {
            GameObjectIns.Instance.IsDownBus = true;
            PlayerAudio.Instance.StopAudio();
            GameObjectIns.Instance.SetCutTo(0f, 1f, 2f);
            GameObjectIns.Instance.SetInvokeToParkingLotScene();
        }
    }

    private void SetPlayPosDown()
    {
        GameObjectIns.Instance.SetCutTo(1f, 0f, 1.5f);
        GameObjectIns.Instance.Player.transform.localEulerAngles = PlayerPosition.PlayerCDGDownBusRot;
        GameObjectIns.Instance.Player.transform.position = PlayerPosition.PlayerCDGDownBusPos;
    }

    private void SetPlayPosUP()
    {
        if (GameObjectIns.Instance.Player.transform.GetChild(0).name == "0")
        {
            GameObjectIns.Instance.CDG_UpBusToZYSX();
        }
        else
        {
            //GameObjectIns.Instance.SetHandModel(true);
            GameObjectIns.Instance.PlayerHint_Canvas.SetActive(false);
        }
        GameObjectIns.Instance.PlayerHint_Canvas.SetActive(false);
        if (GameObjectIns.Instance.Player.transform.GetChild(0).name == "cdg1")
        {
            GameObjectIns.Instance.CDGSS_Over_Button1.gameObject.SetActive(false);
            GameObjectIns.Instance.Player.transform.GetChild(0).name = "cdg2";
            GameObjectIns.Instance.AKeyToStartOver();
            GameObjectIns.Instance.SetHandModel(true);
        }
        GameObjectIns.Instance.SetCutTo(1f, 0f, 1.5f);
        GameObjectIns.Instance.Player.transform.localEulerAngles = Vector3.zero;
        GameObjectIns.Instance.Player.transform.position = PlayerPosition.PlayerCDGBusPos;
    }

    // 重置 ： 刹车 钥匙 CDG UI 
    private void OnTriggerEnter(Collider other)
    {
        print(other.transform.name);
        if (GameObjectIns.Instance.IsPlayAudio == false)
        {
            if (other.transform.name == "YaoShi")
            {
               
            }
            else if (other.transform.name == "ShouSha001")
            {
                if (GameObjectIns.Instance.IsShouSha == false)
                {
                    PlayerAudio.Instance.PlayBtnAudio();
                    GameObjectIns.Instance.VibateController(1, 100, 1);
                    if (GameObjectIns.Instance.IsPlayAudio == false)
                    {
                        GameObjectIns.Instance.CancelInvoke("PlayLaShouShaAudio");
                        print("手刹");
                        //PlayerAudio.Instance.PlayAudioClip(12);
                        GameObjectIns.Instance.SetHight(GameObjectIns.Instance.ShouSha001, false);

                        GameObjectIns.Instance.SetC2ChildFalse();
                        GameObjectIns.Instance.AKeyToStart_Button.gameObject.SetActive(true);

                        GameObjectIns.Instance.CloseShouSha(true, 2f);
                        //GameObjectIns.Instance.RawImage_Camera0.SetActive(true);
                        //GameObjectIns.Instance.RawImage_Camera1.SetActive(false);
                        GameObjectIns.Instance.NoDeclineCDG_Img.SetActive(false);
                    }
                    else
                    {
                        print("钥匙已关闭");
                    }
                }
                else
                {
                    print("手刹已关闭");
                }
            }
            else if (other.transform.name == "DangWei_N")
            {
                if (GameObjectIns.Instance.DangWei_N == false && GameObjectIns.Instance.IsShouSha)
                {
                    PlayerAudio.Instance.PlayBtnAudio();
                    GameObjectIns.Instance.VibateController(1, 100, 1);
                    GameObjectIns.Instance.DangWei_N = true;
                    GameObjectIns.Instance.SetHight(GameObjectIns.Instance.DangWei_N_GameObject, false);
                    GameObjectIns.Instance.IsYaoShiShouShaOver();
                }
            }
            else if (other.transform.name == "AKeyToStart_Button")
            {
                PlayerAudio.Instance.PlayBtnAudio();
                GameObjectIns.Instance.VibateController(1, 100, 1);
            }
            else if (other.transform.name == "OneRecharge_Btn")
            {
                PlayerAudio.Instance.PlayBtnAudio();
                GameObjectIns.Instance.VibateController(1, 100, 1);
                print("16充电弓选择错误");
                PlayerAudio.Instance.PlayAudioClipTest(16);
            }
            else if (other.transform.name == "TwoRecharge_Btn")
            {
                PlayerAudio.Instance.PlayBtnAudio();
                GameObjectIns.Instance.VibateController(1, 100, 1);
            }
            else if (other.transform.name == "ThreeRecharge_Btn")
            {
                PlayerAudio.Instance.PlayBtnAudio();
                GameObjectIns.Instance.VibateController(1, 100, 1);
                print("16充电弓选择错误");
                PlayerAudio.Instance.PlayAudioClipTest(16);
            }
            else if (other.transform.name == "FourRecharge_Btn")
            {
                PlayerAudio.Instance.PlayBtnAudio();
                GameObjectIns.Instance.VibateController(1, 100, 1);
                print("16充电弓选择错误");
                PlayerAudio.Instance.PlayAudioClipTest(16);
            }
            else if (other.transform.name == "RechargeDown_Btn")
            {
                PlayerAudio.Instance.PlayBtnAudio();
                GameObjectIns.Instance.VibateController(1, 100, 1);
            }
            else if (other.transform.name == "RechargeUp_Btn")
            {
                PlayerAudio.Instance.PlayBtnAudio();
                GameObjectIns.Instance.VibateController(1, 100, 1);
            }
            else if (other.transform.name == "RechargeUp_Btn1")
            {
                PlayerAudio.Instance.PlayBtnAudio();
                GameObjectIns.Instance.VibateController(1, 100, 1);
            }
            else if (other.transform.name == "StartRecharge_Btn")
            {
                PlayerAudio.Instance.PlayBtnAudio();
                GameObjectIns.Instance.VibateController(1, 100, 1);
            }
            else if (other.transform.name == "AKeyToStartFail_Btn")
            {
                PlayerAudio.Instance.PlayBtnAudio();
                GameObjectIns.Instance.VibateController(1, 100, 1);
            }
            else if (other.transform.name == "OverRecharge_Btn")
            {
                PlayerAudio.Instance.PlayBtnAudio();
                GameObjectIns.Instance.VibateController(1, 100, 1);
            }
            else if (other.transform.name == "Homepage_Btn")
            {
                PlayerAudio.Instance.PlayBtnAudio();
                GameObjectIns.Instance.VibateController(1, 100, 1);
            }
            else if (other.name == "CDGJJTZBtn")
            {
                if (GameObjectIns.Instance.IsZYSXOne && GameObjectIns.Instance.Player.transform.GetChild(0).name == "cdg")
                {
                    GameObjectIns.Instance.Player.transform.GetChild(0).name = "cdg1";
                    GameObjectIns.Instance.SetHight(GameObjectIns.Instance.CDGJJTZBtn, false);
                    PlayerAudio.Instance.PlayBtnAudio();
                    GameObjectIns.Instance.VibateController(1, 100, 1);
                    GameObjectIns.Instance.IsZYSXOne = false;
                    GameObjectIns.Instance.PlayBusJKPPosOutageAudio();
                }
                else if (GameObjectIns.Instance.Player.transform.GetChild(0).name == "cdg2")
                {
                    GameObjectIns.Instance.Player.transform.GetChild(0).name = "cdg3";
                    GameObjectIns.Instance.SetHight(GameObjectIns.Instance.CDGJJTZBtn, false);
                    PlayerAudio.Instance.PlayBtnAudio();
                    GameObjectIns.Instance.VibateController(1, 100, 1);
                    GameObjectIns.Instance.CDGJJTZZC2();
                }
            }
        }
    }

    private void hit_AKeyToStartHit_Image()
    {
        GameObjectIns.Instance.SetC2ChildFalse();
        GameObjectIns.Instance.AKeyToStartFail_Btn.gameObject.SetActive(true);
    }

    private void OnTriggerExit(Collider other)
    {
        print(other.transform.name);
        if (GameObjectIns.Instance.IsPlayAudio ==false)
        {
            if (other.transform.name == "AKeyToStart_Button")
            {
                if (GameObjectIns.Instance.IsShouSha == false || GameObjectIns.Instance.DangWei_N == false)
                {
                    print("手刹或挡位未关闭 不允许降弓");
                    GameObjectIns.Instance.SetC2ChildFalse();
                    GameObjectIns.Instance.NoDeclineCDG_Img.SetActive(true);
                }
                else
                {
                    GameObjectIns.Instance.PlayerHint_Canvas.gameObject.SetActive(false);
                    print("一键启动失败 请开始手动模式");
                    //PlayerAudio.Instance.PlayAudioClip(7);
                    GameObjectIns.Instance.SetC2ChildFalse();
                    GameObjectIns.Instance.TwoRecharge_Img.SetActive(true);
                    GameObjectIns.Instance.TwoRecharge_Img.transform.GetChild(0).gameObject.SetActive(false);
                    GameObjectIns.Instance.TwoRecharge_Img.transform.GetChild(1).gameObject.SetActive(true);
                    GameObjectIns.Instance.SetAKeyToStart();

                    GameObjectIns.Instance.AKeyToStart_Button.transform.GetChild(0).GetChild(0).gameObject.GetComponent<MeshRenderer>().enabled = false;
                    GameObjectIns.Instance.CDGJL_arrow.SetActive(true);
                    //PlayerAudio.Instance.PlayAudioClipTest(4);
                    //GameObjectIns.Instance.AKeyToStartHit_Image.SetActive(true);
                    //Invoke("hit_AKeyToStartHit_Image", 7f);

                }
            }
            else if (other.transform.name == "OneRecharge_Btn")
            {
                print("一号充电弓 位置不正确");
                //if (Vector3.Distance(GameObjectIns.Instance.ShouSha001.transform.parent.transform.position, GameObjectIns.Instance.CDG_ani.transform.position) < 5f)
                //{
                //    if (GameObjectIns.Instance.CDGIndex != 0)
                //    {
                //        print("配音：点击“充电弓降落”按钮，并通过监控或下车确认充电弓是否已降落");
                //        GameObjectIns.Instance.SetC2ChildFalse();
                //        GameObjectIns.Instance.RechargeDown_Btn.transform.gameObject.SetActive(true);
                //        GameObjectIns.Instance.RechargeUp_Btn.transform.gameObject.SetActive(true);
                //    }
                //    else
                //    {
                //        print("正在充电");
                //        GameObjectIns.Instance.SetC2ChildFalse();
                //        GameObjectIns.Instance.RechargeState_Image.transform.gameObject.SetActive(true);
                //        GameObjectIns.Instance.OverRecharge_Btn.transform.gameObject.SetActive(true);
                //    }
                //}
                //else
                //{
                //    print("位置不正确");
                //}
            }
            else if (other.transform.name == "TwoRecharge_Btn")
            {
                CancelInvoke();
                print("二号充电弓");
                if (Vector3.Distance(GameObjectIns.Instance.ShouSha001.transform.parent.transform.position, GameObjectIns.Instance.CDG_ani1.transform.position) < 5f)
                {
                    GameObjectIns.Instance.SetHight(GameObjectIns.Instance.CDG_arrow, false);
                    GameObjectIns.Instance.CDG_arrow.SetActive(false);
                    if (GameObjectIns.Instance.CDGIsDown == false)
                    {
                        GameObjectIns.Instance.SetC2ChildFalse();
                        GameObjectIns.Instance.TwoRecharge_Img.SetActive(true);
                        GameObjectIns.Instance.TwoRecharge_Img.transform.GetChild(0).gameObject.SetActive(true);
                        GameObjectIns.Instance.TwoRecharge_Img.transform.GetChild(1).gameObject.SetActive(false);

                        GameObjectIns.Instance.Homepage_Btn.gameObject.SetActive(true);
                        print("9通信连接过程中，请稍后，等待通信连接完成");
                        PlayerAudio.Instance.PlayAudioClipTest(9);
                        GameObjectIns.Instance.Homepage_Btn.transform.GetChild(0).gameObject.SetActive(false);

                        Invoke("TwoRechargeEvent", 6.5f);
                    }
                    else
                    {
                        print("正在充电");
                        print("12_2充电信息包括电池电量百分比（SOC），已充电量，充电功率，充电电流，充电电压。充电过程中，如果需要返回上一级菜单可点击左上角小房子按钮，如果需要提前结束充电，可点击终端面板右下角的“结束充电”按钮结束充电。");
                        PlayerAudio.Instance.PlayAudioClipTest(12);
                        GameObjectIns.Instance.SetC2ChildFalse();
                        GameObjectIns.Instance.RechargeState_Image.transform.gameObject.SetActive(true);
                        GameObjectIns.Instance.OverRecharge_Btn.transform.gameObject.SetActive(true);
                        GameObjectIns.Instance.Homepage_Btn.gameObject.SetActive(true);

                        GameObjectIns.Instance.BG_Image0.SetActive(false);
                        GameObjectIns.Instance.BG_Image1.SetActive(true);
                    }
                }
                else
                {
                    print("位置不正确");
                }
            }
            else if (other.transform.name == "RechargeDown_Btn")
            {
                GameObjectIns.Instance.PlayerHint_Canvas.SetActive(true);
                GameObjectIns.Instance.PlayerHint_Text.text = "用左手手柄上的Y键下车查看，按X键回到车内，确认充电弓已降落";

                GameObjectIns.Instance.vr_cartoon_hand_prefab_Left.gameObject.SetActive(false);
                GameObjectIns.Instance.vr_cartoon_hand_prefab_Left.transform.parent.GetComponent<MeshRenderer>().enabled = true;

                //GameObjectIns.Instance.RechargeDown_Btn.transform.parent.gameObject.SetActive(false);
                GameObjectIns.Instance.CDGAniDown();
                GameObjectIns.Instance.SetC2ChildFalse();
                GameObjectIns.Instance.RechargeDown_Image.SetActive(true);
                GameObjectIns.Instance.RechargeUp_Btn.gameObject.SetActive(true);
                print("7充电弓正在降落，请稍后，此时也可以点击屏幕右下角“充电弓上升”按钮，控制充电弓上升");
                PlayerAudio.Instance.PlayAudioClipTest(7);

                GameObjectIns.Instance.CDGJL_arrow.SetActive(true); 
                Invoke("CDGAniDownOver", 10.5f);
                //GameObjectIns.Instance.CDGAniDownOverSetHandTrue(true,11f);
            }
            else if (other.transform.name == "RechargeUp_Btn")
            {
                GameObjectIns.Instance.CDGJL_arrow.SetActive(true);
                CancelInvoke();
                print("充电弓正在上升 请稍后...");
                GameObjectIns.Instance.IsRecharge = false;
                GameObjectIns.Instance.IsRecharge30 = false;
                GameObjectIns.Instance.SetC2ChildFalse();
                GameObjectIns.Instance.RechargeOver_Image.gameObject.SetActive(true);
                GameObjectIns.Instance.RechargeOver_Image.transform.GetChild(0).gameObject.SetActive(true);
                GameObjectIns.Instance.RechargeOver_Image.transform.GetChild(1).gameObject.SetActive(false);
                GameObjectIns.Instance.CDGAniUP();

                GameObjectIns.Instance.BG_Image0.SetActive(true);
                GameObjectIns.Instance.BG_Image1.SetActive(false);

                Invoke("CDGAniUpOver", 10f);
            }
            else if (other.transform.name == "RechargeUp_Btn1")
            {
                GameObjectIns.Instance.CDGJL_arrow.SetActive(true);
                print("充电弓正在上升 请稍后...");
                GameObjectIns.Instance.IsRecharge = false;
                GameObjectIns.Instance.IsRecharge30 = false;
                GameObjectIns.Instance.SetC2ChildFalse();
                GameObjectIns.Instance.RechargeOver_Image.gameObject.SetActive(true);
                GameObjectIns.Instance.RechargeOver_Image.transform.GetChild(0).gameObject.SetActive(true);
                GameObjectIns.Instance.RechargeOver_Image.transform.GetChild(1).gameObject.SetActive(false);
                GameObjectIns.Instance.CDGAniUP();

                GameObjectIns.Instance.BG_Image0.SetActive(true);
                GameObjectIns.Instance.BG_Image1.SetActive(false);

                Invoke("CDGAniUpOver", 10f);
            }
            else if (other.transform.name == "StartRecharge_Btn")
            {
                GameObjectIns.Instance.CDGAniDownOverSetHandTrue(true, 0.0001f);
                GameObjectIns.Instance.RechargeTime = 0f;
                GameObjectIns.Instance.PlayerHint_Canvas.SetActive(false);
                print("9通信连接过程中，请稍后，等待通信连接完成");
                PlayerAudio.Instance.PlayAudioClipTest(9);
                GameObjectIns.Instance.SetC2ChildFalse();
                GameObjectIns.Instance.TwoRecharge_Img.SetActive(true);
                GameObjectIns.Instance.TwoRecharge_Img.transform.GetChild(0).gameObject.SetActive(false);
                GameObjectIns.Instance.TwoRecharge_Img.transform.GetChild(1).gameObject.SetActive(true);
                GameObjectIns.Instance.BG_Image0.SetActive(true);
                GameObjectIns.Instance.BG_Image1.SetActive(false);
                Invoke("StartRecharge_Btn_Event", 7f);
            }
            else if (other.transform.name == "AKeyToStartFail_Btn")
            {
                print("开始手动模式");
                if (GameObjectIns.Instance.CDGIsDown == false)
                {
                    SetAKeyToStartFail_Btn();
                }
                else
                {
                    print("2正在充电");
                    GameObjectIns.Instance.SetC2ChildFalse();

                    GameObjectIns.Instance.OneRecharge_Btn.gameObject.SetActive(true);
                    GameObjectIns.Instance.OneRecharge_Btn.transform.GetChild(1).transform.gameObject.SetActive(true);
                    GameObjectIns.Instance.OneRecharge_Btn.transform.GetChild(2).transform.gameObject.SetActive(false);
                    GameObjectIns.Instance.TwoRecharge_Btn.gameObject.SetActive(true);
                    GameObjectIns.Instance.TwoRecharge_Btn.transform.GetChild(1).transform.gameObject.SetActive(false);
                    GameObjectIns.Instance.TwoRecharge_Btn.transform.GetChild(2).transform.gameObject.SetActive(true);
                    GameObjectIns.Instance.ThreeRecharge_Btn.gameObject.SetActive(true);
                    GameObjectIns.Instance.FourRecharge_Btn.gameObject.SetActive(true);
                }
            }
            else if (other.transform.name == "OverRecharge_Btn")
            {
                GameObjectIns.Instance.vr_cartoon_hand_prefab_Left.SetActive(false);
                GameObjectIns.Instance.vr_cartoon_hand_prefab_Left.transform.parent.GetComponent<MeshRenderer>().enabled = true;

                print("13充电结束后，充电弓自动上升，可通过监控或者下车查看充电弓是否上升成功");
                PlayerAudio.Instance.PlayAudioClipTest(13);

                GameObjectIns.Instance.vr_cartoon_hand_prefab_Left.gameObject.SetActive(false);
                GameObjectIns.Instance.vr_cartoon_hand_prefab_Left.transform.parent.GetComponent<MeshRenderer>().enabled = true;



                GameObjectIns.Instance.IsRecharge = false;
                GameObjectIns.Instance.CDGAniUP();

                GameObjectIns.Instance.CDGJL_arrow.SetActive(true);
                
                //PlayerAudio.Instance.PlayAudioClip(4);
                GameObjectIns.Instance.SetC2ChildFalse();
                GameObjectIns.Instance.BG_Image0.SetActive(true);
                GameObjectIns.Instance.BG_Image1.SetActive(false);


                GameObjectIns.Instance.RechargeOver_Image.gameObject.SetActive(true);

                GameObjectIns.Instance.RechargeOver_Image.transform.GetChild(0).gameObject.SetActive(true);
                GameObjectIns.Instance.RechargeOver_Image.transform.GetChild(1).gameObject.SetActive(false);


                Invoke("CDGAniUpOver1", 9f);
            }
            else if (other.transform.name == "Homepage_Btn")
            {
                GameObjectIns.Instance.OneRecharge_Btn.transform.GetComponent<BoxCollider>().enabled = false;
                Invoke("SetOneBtn", 1f);
                other.transform.GetChild(0).gameObject.SetActive(false);
                GameObjectIns.Instance.BG_Image0.SetActive(true);
                GameObjectIns.Instance.BG_Image1.SetActive(false);
                if (GameObjectIns.Instance.CDGIsDown == false)
                {
                    print("11点击主界面对应的充电弓，进入充电信息界面");
                    PlayerAudio.Instance.PlayAudioClipTest(11);
                    GameObjectIns.Instance.SetC2ChildFalse();
                    GameObjectIns.Instance.OneRecharge_Btn.gameObject.SetActive(true);
                    GameObjectIns.Instance.OneRecharge_Btn.transform.GetChild(1).transform.gameObject.SetActive(true);
                    GameObjectIns.Instance.OneRecharge_Btn.transform.GetChild(2).transform.gameObject.SetActive(false);
                    GameObjectIns.Instance.TwoRecharge_Btn.gameObject.SetActive(true);
                    GameObjectIns.Instance.TwoRecharge_Btn.transform.GetChild(1).transform.gameObject.SetActive(true);
                    GameObjectIns.Instance.TwoRecharge_Btn.transform.GetChild(2).transform.gameObject.SetActive(false);

                    GameObjectIns.Instance.ThreeRecharge_Btn.gameObject.SetActive(true);
                    GameObjectIns.Instance.ThreeRecharge_Btn.transform.GetChild(1).transform.gameObject.SetActive(true);
                    GameObjectIns.Instance.ThreeRecharge_Btn.transform.GetChild(2).transform.gameObject.SetActive(false);
                    GameObjectIns.Instance.FourRecharge_Btn.gameObject.SetActive(true);
                    GameObjectIns.Instance.FourRecharge_Btn.transform.GetChild(1).transform.gameObject.SetActive(true);
                    GameObjectIns.Instance.FourRecharge_Btn.transform.GetChild(2).transform.gameObject.SetActive(false);
                }
                else
                {
                    GameObjectIns.Instance.Homepage_Btn.transform.GetChild(0).transform.gameObject.SetActive(false);
                    GameObjectIns.Instance.IsRecharge30 = true;
                    print("2正在充电");
                    GameObjectIns.Instance.SetC2ChildFalse();

                    GameObjectIns.Instance.OneRecharge_Btn.gameObject.SetActive(true);
                    GameObjectIns.Instance.OneRecharge_Btn.transform.GetChild(1).transform.gameObject.SetActive(true);
                    GameObjectIns.Instance.OneRecharge_Btn.transform.GetChild(2).transform.gameObject.SetActive(false);
                    GameObjectIns.Instance.TwoRecharge_Btn.gameObject.SetActive(true);
                    GameObjectIns.Instance.TwoRecharge_Btn.transform.GetChild(1).transform.gameObject.SetActive(false);
                    GameObjectIns.Instance.TwoRecharge_Btn.transform.GetChild(2).transform.gameObject.SetActive(true);

                    GameObjectIns.Instance.ThreeRecharge_Btn.gameObject.SetActive(true);
                    GameObjectIns.Instance.ThreeRecharge_Btn.transform.GetChild(1).transform.gameObject.SetActive(true);
                    GameObjectIns.Instance.ThreeRecharge_Btn.transform.GetChild(2).transform.gameObject.SetActive(false);
                    GameObjectIns.Instance.FourRecharge_Btn.gameObject.SetActive(true);
                    GameObjectIns.Instance.FourRecharge_Btn.transform.GetChild(1).transform.gameObject.SetActive(true);
                    GameObjectIns.Instance.FourRecharge_Btn.transform.GetChild(2).transform.gameObject.SetActive(false);
                }
                //if (GameObjectIns.Instance.CDGIndex == -1)
                //{
                //    print("都没有充电");
                //    GameObjectIns.Instance.SetC2ChildFalse();
                //    GameObjectIns.Instance.OneRecharge_Btn.gameObject.SetActive(true);
                //    GameObjectIns.Instance.OneRecharge_Btn.transform.GetChild(1).transform.gameObject.SetActive(true);
                //    GameObjectIns.Instance.OneRecharge_Btn.transform.GetChild(2).transform.gameObject.SetActive(false);
                //    GameObjectIns.Instance.TwoRecharge_Btn.gameObject.SetActive(true);
                //    GameObjectIns.Instance.TwoRecharge_Btn.transform.GetChild(1).transform.gameObject.SetActive(true);
                //    GameObjectIns.Instance.TwoRecharge_Btn.transform.GetChild(2).transform.gameObject.SetActive(false);
                //}
                //else if (GameObjectIns.Instance.CDGIndex == 0)
                //{
                //    print("1正在充电");
                //    GameObjectIns.Instance.SetC2ChildFalse();

                //    GameObjectIns.Instance.OneRecharge_Btn.gameObject.SetActive(true);
                //    GameObjectIns.Instance.OneRecharge_Btn.transform.GetChild(1).transform.gameObject.SetActive(false);
                //    GameObjectIns.Instance.OneRecharge_Btn.transform.GetChild(2).transform.gameObject.SetActive(true);
                //    GameObjectIns.Instance.TwoRecharge_Btn.gameObject.SetActive(true);
                //    GameObjectIns.Instance.TwoRecharge_Btn.transform.GetChild(1).transform.gameObject.SetActive(true);
                //    GameObjectIns.Instance.TwoRecharge_Btn.transform.GetChild(2).transform.gameObject.SetActive(false);
                //}
                //else if (GameObjectIns.Instance.CDGIndex == 1)
                //{
                //    print("2正在充电");
                //    GameObjectIns.Instance.SetC2ChildFalse();

                //    GameObjectIns.Instance.OneRecharge_Btn.gameObject.SetActive(true);
                //    GameObjectIns.Instance.OneRecharge_Btn.transform.GetChild(1).transform.gameObject.SetActive(true);
                //    GameObjectIns.Instance.OneRecharge_Btn.transform.GetChild(2).transform.gameObject.SetActive(false);
                //    GameObjectIns.Instance.TwoRecharge_Btn.gameObject.SetActive(true);
                //    GameObjectIns.Instance.TwoRecharge_Btn.transform.GetChild(1).transform.gameObject.SetActive(false);
                //    GameObjectIns.Instance.TwoRecharge_Btn.transform.GetChild(2).transform.gameObject.SetActive(true);
                //}
            }
            else if (other.name == "ATS_OverRecharge_Btn")
            {
                PlayerAudio.Instance.PlayBtnAudio();
                GameObjectIns.Instance.StarAKeyToStartCDOver();
            }
           
        }
        else
        {
            print("正在播放声音 稍后操作");
        }
    }


    private void SetOneBtn()
    {
        GameObjectIns.Instance.OneRecharge_Btn.transform.GetComponent<BoxCollider>().enabled = true;
    }

    private void SetTwoCDGArrow()
    {
        GameObjectIns.Instance.CDG_arrow.SetActive(false);
        print("19核对编号完成后点击屏幕上对应的编号");
        PlayerAudio.Instance.PlayAudioClipTest(19);
        GameObjectIns.Instance.TwoRecharge_Btn.transform.GetChild(3).gameObject.SetActive(true);
    }

    private void StartRecharge_Btn_Event()
    {
        GameObjectIns.Instance.SetC2ChildFalse();
        print("10启动充电成功，等待倒计时30秒后自动跳转至充电信息界面，或者点击屏幕左上方小房子图标进入主界面");
        //PlayerAudio.Instance.PlayAudioClip(5);
        PlayerAudio.Instance.PlayAudioClipTest(10);

        GameObjectIns.Instance.SetC2ChildFalse();
        GameObjectIns.Instance.Homepage_Btn.gameObject.SetActive(true);
        GameObjectIns.Instance.Homepage_Btn.transform.GetChild(0).transform.gameObject.SetActive(true);

        GameObjectIns.Instance.Recharge30Time = 30f;
        GameObjectIns.Instance.IsRecharge = true;
        GameObjectIns.Instance.RechargeYes_Image.SetActive(true);
        //GameObjectIns.Instance.Homepage_Btn.gameObject.SetActive(true);
    }

    private void TwoRechargeEvent()
    {
        print("6点击“充电弓降落”按钮，并通过监控或下车确认充电弓是否已降落");
        PlayerAudio.Instance.PlayAudioClipTest(6);
        //PlayerAudio.Instance.PlayAudioClip(11);
        GameObjectIns.Instance.CDGIsDown = true;
        GameObjectIns.Instance.SetC2ChildFalse();
        GameObjectIns.Instance.RechargeDown_Btn.transform.gameObject.SetActive(true);
        GameObjectIns.Instance.RechargeUp_Btn.transform.gameObject.SetActive(true);
    }
    
    private void CDGAniDownOver()
    {
        GameObjectIns.Instance.PlayerHint_Canvas.SetActive(false);
        print("8充电弓降落成功 请点击屏幕启动按钮");
        PlayerAudio.Instance.PlayAudioClipTest(8);
        //PlayerAudio.Instance.PlayAudioClip(19);

        GameObjectIns.Instance.CDGJL_arrow.SetActive(false);

        GameObjectIns.Instance.BG_Image0.SetActive(false);
        GameObjectIns.Instance.BG_Image1.SetActive(true);

        GameObjectIns.Instance.SetC2ChildFalse();
        GameObjectIns.Instance.RechargeDownYes_Image.SetActive(true);
        GameObjectIns.Instance.RechargeUp_Btn1.gameObject.SetActive(true);
        GameObjectIns.Instance.StartRecharge_Btn.transform.gameObject.SetActive(true);
        //Invoke("CDGAniUpOver()", 10f);
    }

    private void CDGAniUpOver()
    {
        print("充电弓上升结束");
        GameObjectIns.Instance.SetC2ChildFalse();
        //GameObjectIns.Instance.RechargeUPOver();
        GameObjectIns.Instance.CDGIsDown = false;
        GameObjectIns.Instance.RechargeOver_Image.gameObject.SetActive(true);
        GameObjectIns.Instance.RechargeOver_Image.transform.GetChild(0).gameObject.SetActive(false);
        GameObjectIns.Instance.RechargeOver_Image.transform.GetChild(1).gameObject.SetActive(true);
        GameObjectIns.Instance.CDGJL_arrow.SetActive(false);

        //GameObjectIns.Instance.RechargeUPOver();
        Invoke("SetAKeyToStartFail_Btn", 5f);
    }

    private void SetAKeyToStartFail_Btn()
    {
        GameObjectIns.Instance.Player.transform.GetChild(0).name = "c0";
        print("5通过地面标识或者充电弓体标识核对充电指定区域的充电弓编号");
        PlayerAudio.Instance.PlayAudioClipTest(5);
        //PlayerAudio.Instance.PlayAudioClip(21);
        GameObjectIns.Instance.CDG_arrow.SetActive(true);

        GameObjectIns.Instance.SetHight(GameObjectIns.Instance.CDG_arrow, true);

        GameObjectIns.Instance.SetC2ChildFalse();
        GameObjectIns.Instance.OneRecharge_Btn.gameObject.SetActive(true);

        GameObjectIns.Instance.TwoRecharge_Btn.gameObject.SetActive(true);
        GameObjectIns.Instance.TwoRecharge_Btn.transform.GetChild(1).transform.gameObject.SetActive(true);
        GameObjectIns.Instance.TwoRecharge_Btn.transform.GetChild(2).transform.gameObject.SetActive(false);

        GameObjectIns.Instance.TwoRecharge_Btn.transform.Find("Two_CDGJL_arrow").gameObject.SetActive(false);
        Invoke("SetTwoCDGArrow", 8f);

        GameObjectIns.Instance.ThreeRecharge_Btn.gameObject.SetActive(true);
        GameObjectIns.Instance.FourRecharge_Btn.gameObject.SetActive(true);
    }

    private void CDGAniUpOver1()
    {
        GameObjectIns.Instance.PlayerHint_Canvas.SetActive(false);
        //print("充电弓上升结束");
        GameObjectIns.Instance.RechargeUPOver();
        
    }

    private void SetOverRecharge_Btn()
    {
        //GameObjectIns.Instance.AKeyToStart_Button.gameObject.SetActive(true);

        GameObjectIns.Instance.SetC2ChildFalse();
        GameObjectIns.Instance.RechargeOverHint_Image.gameObject.SetActive(true);
        if (GameObjectIns.Instance.RechargeTime > 100f)
        {
            GameObjectIns.Instance.RechargeTime = 100f;
        }
        GameObjectIns.Instance.RechargeOverHint_Image.transform.GetChild(0).GetComponent<Text>().text = ((int)GameObjectIns.Instance.RechargeTime).ToString()+"%";
        Invoke("GameOverHint1", 10f);
    }

    private void GameOverHint1()
    {
        GameObjectIns.Instance.CDGGameOverHint();
    }
}
