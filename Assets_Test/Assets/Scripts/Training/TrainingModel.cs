﻿using DG.Tweening;
using Pvr_UnitySDKAPI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrainingModel : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        GameObjectIns.Instance.Canvas.SetActive(false);
        GameObjectIns.Instance.SetCutTo(1, 0, 1.5f);
        GameObjectIns.Instance.Canvas1.SetActive(false);
        GameObjectIns.Instance.Player.transform.position = PlayerPosition.PlayerStartPos;
        GameObjectIns.Instance.Player.transform.localRotation = GameObjectIns.Instance.PlayerStartRotation;
        GameObjectIns.Instance.CityBuswithInterior.transform.position = PlayerPosition.CityBuswithInteriorStartPos;
        Invoke("SetJianShuDaiHitTrue", 2f);
    }

    //车移动
    void CityBuswithInteriorStartMove()
    {
        GameObjectIns.Instance.DOTweenMove(GameObjectIns.Instance.CityBuswithInterior, PlayerPosition.CityBuswithInteriorCDGPos, 5f);

        GameObjectIns.Instance.DOTweenRotate(GameObjectIns.Instance.Wheel_FL, Vector3.zero, 0f, true);
        GameObjectIns.Instance.DOTweenRotate(GameObjectIns.Instance.Wheel_FR, Vector3.zero, 0f, true);
        GameObjectIns.Instance.DOTweenRotate(GameObjectIns.Instance.Wheel_RL, Vector3.zero, 0f, true);
        GameObjectIns.Instance.DOTweenRotate(GameObjectIns.Instance.Wheel_RR, Vector3.zero, 0f, true);

        GameObjectIns.Instance.DOTweenRotate(GameObjectIns.Instance.Wheel_FL, new Vector3(30f * 60f, 0f, 0f), 5f, true);
        GameObjectIns.Instance.DOTweenRotate(GameObjectIns.Instance.Wheel_FR, new Vector3(30f * 60f, 0f, 0f), 5f, true);
        GameObjectIns.Instance.DOTweenRotate(GameObjectIns.Instance.Wheel_RL, new Vector3(30f * 60f, 0f, 0f), 5f, true);
        GameObjectIns.Instance.DOTweenRotate(GameObjectIns.Instance.Wheel_RR, new Vector3(30f * 60f, 0f, 0f), 5f, true);

        Invoke("SetHintText", 5f);
    }

    void SetHintText()
    {
        if (GameObjectIns.Instance.IsTestFalse)
        {
            GameObjectIns.Instance.PlayerHint_Canvas.SetActive(true);
            GameObjectIns.Instance.SetPlayerHint_Canvas_Test("培训完毕,即将返回测验");
        }
        else
        {
            GameObjectIns.Instance.PlayerHint_Canvas.SetActive(true);
            GameObjectIns.Instance.SetPlayerHint_Canvas_Test("车辆停靠完毕,系统将切换到车内视角");
        }

        GameObjectIns.Instance.SetCutTo(0f, 1f, 3f);
        Invoke("TheNextLevel", 3f);
    }

    //水向上移动
    void WaterPlaneUPMove()
    {
        print("2如果停车位积水超过200毫米，禁止停车");
        PlayerAudio.Instance.PlayAudioClipTest(1);
        GameObjectIns.Instance.DOTweenMove(GameObjectIns.Instance.Water_Plane, new Vector3(0f, 0.0035f, 0f), 4f);
        Invoke("WaterPlaneDownMove", 4f);
    }
    //水向下移动
    void WaterPlaneDownMove()
    {
        GameObjectIns.Instance.DOTweenMove(GameObjectIns.Instance.Water_Plane, new Vector3(0f, -0.0003f, 0f), 4f);
        Invoke("CityBuswithInteriorStartMove", 4f);
    }

    /// <summary>
    /// 设置减速带高亮
    /// </summary>
    /// <param name="b">b=true 高亮， b == false 关闭高亮</param>
    void SetJianShuDaiHitTrue()
    {
        print("0进行车辆充电前，需根据现场充电区域划分，行驶车辆停放至指定充电区域，车辆前轮行驶至减速带中间，车身停在车位线中间，两边切勿超过车位线边线");
        PlayerAudio.Instance.PlayAudioClipTest(0);
        for (int g = 0; g < GameObjectIns.Instance.JianShuDaiParent.transform.childCount; g++)
        {
            GameObjectIns.Instance.SetHight(GameObjectIns.Instance.JianShuDaiParent.transform.GetChild(g).gameObject, true);
        }

        GameObjectIns.Instance.DOTweenMove(GameObjectIns.Instance.Che_10_2m_Hint, new Vector3(GameObjectIns.Instance.Che_10_2m_Hint.transform.localPosition.x, GameObjectIns.Instance.Che_10_2m_Hint.transform.localPosition.y + 1.1f, GameObjectIns.Instance.Che_10_2m_Hint.transform.localPosition.z), 2f);
        //GameObjectIns.Instance.DOTweenMove(GameObjectIns.Instance.Che_6_5m_Hint, new Vector3(GameObjectIns.Instance.Che_6_5m_Hint.transform.localPosition.x, GameObjectIns.Instance.Che_6_5m_Hint.transform.localPosition.y + 0.8f, GameObjectIns.Instance.Che_6_5m_Hint.transform.localPosition.z), 2f);

        Invoke("SetJianSudaiFalse", 17f);
    }

    void SetJianSudaiFalse()
    {
        GameObjectIns.Instance.DOTweenMove(GameObjectIns.Instance.Che_10_2m_Hint, new Vector3(GameObjectIns.Instance.Che_10_2m_Hint.transform.localPosition.x, GameObjectIns.Instance.Che_10_2m_Hint.transform.localPosition.y - 1.1f, GameObjectIns.Instance.Che_10_2m_Hint.transform.localPosition.z), 2f);
        //GameObjectIns.Instance.DOTweenMove(GameObjectIns.Instance.Che_6_5m_Hint, new Vector3(GameObjectIns.Instance.Che_6_5m_Hint.transform.localPosition.x, GameObjectIns.Instance.Che_6_5m_Hint.transform.localPosition.y - 0.8f, GameObjectIns.Instance.Che_6_5m_Hint.transform.localPosition.z), 2f);

        for (int g = 0; g < GameObjectIns.Instance.JianShuDaiParent.transform.childCount; g++)
        {
            GameObjectIns.Instance.SetHight(GameObjectIns.Instance.JianShuDaiParent.transform.GetChild(g).gameObject, false);
        }

        Invoke("WaterPlaneUPMove", 1f);
    }

    //下一关
    void TheNextLevel()
    {
        if (GameObjectIns.Instance.IsTestFalse)
        {
            GameObjectIns.Instance.CityBuswithInterior.transform.localPosition = PlayerPosition.CityBuswithInteriorStartPos;
            GameObjectIns.Instance.PlayerHint_Canvas.SetActive(false);
            GameObjectIns.Instance.IsTestFalse = false;
            GameObjectIns.Instance.Player.transform.localPosition = PlayerPosition.PlayerSelectCDGPos;
            GameObjectIns.Instance.Player.transform.localEulerAngles = PlayerPosition.PlayerCDGDownBusRot;
            GameObjectIns.Instance.TestModel_Water_Plane.SetActive(true);
            GameObjectIns.Instance.SetCutTo(1f, 0f, 3f);

            if (GameObjectIns.Instance.IsTest)
            {
                print("38欢迎进入特来电公交车充电VR考试，请在语音提示结束后开始操作。请用手柄选择正确的充电场地编号并扣扳机");
                PlayerAudio.Instance.PlayAudioClipTest(38);
            }
           

            Invoke("GoTestModelXZCDG", 3f);

        }
        else
        {
            GameObjectIns.Instance.TwenerMove.Pause();
            transform.gameObject.AddComponent<TrainingModel1>();
            GameObjectIns.Instance.Canvas1.transform.gameObject.SetActive(true);
            Destroy(transform.GetComponent<TrainingModel>());
        }
    }

    void GoTestModelXZCDG()
    {
        GameObjectIns.Instance.TestModel_Water_Plane.SetActive(true);
        GameObjectIns.Instance.TestCDG_arrow.SetActive(true);
        GameObjectIns.Instance.TestCDG_arrow1.SetActive(true);
        Destroy(transform.GetComponent<TrainingModel>());
    }

    //上一关
    void OnALevel()
    {
        CancelInvoke();
        GameObjectIns.Instance.TwenerMove.Pause();
        Destroy(transform.GetComponent<TrainingModel>());
        transform.gameObject.AddComponent<ChoiceModel>();
    }
}
