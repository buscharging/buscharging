﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnTriggerStayTest : MonoBehaviour
{
    private bool is_Down = true;

    private void OnTriggerStay(Collider other)
    {
        if (Input.GetKeyDown(KeyCode.N) && is_Down)
        {
            is_Down = false;
            print("##########################################");
        }
        if (Input.GetKeyUp(KeyCode.N) && !is_Down)
        {
            is_Down = true;
            print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
        }
    }
}
