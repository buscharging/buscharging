﻿/// <summary>
/// 软键盘控制
/// </summary>
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Keyboard : MonoBehaviour
{
    private Button btn_DeleteKey;//删除键
    private Button Btn_CloseKeyboard;//确认
    [HideInInspector]
    public InputField PlayID_Key;//激活码输入框
    public AudioSource BtnAudio;
   
    private void Awake()
    {
        PlayID_Key = GameObject.Find("PlayID_Key").GetComponent<InputField>();

        btn_DeleteKey = transform.Find("Btn_DeleteKey").GetComponent<Button>();
        btn_DeleteKey.onClick.AddListener(DeleteButton);//删除键
        Btn_CloseKeyboard = transform.Find("Btn_CloseKeyboard").GetComponent<Button>();
        Btn_CloseKeyboard.onClick.AddListener(btn_Ok_Button);//确认

        for (int i = 0; i < transform.Find("Obj_FigureTheParentObject").childCount; i++)
        {
            string key = transform.Find("Obj_FigureTheParentObject").GetChild(i).transform.name;
            transform.Find("Obj_FigureTheParentObject").GetChild(i).GetComponent<Button>().onClick.AddListener(delegate () {
                NumberBtn(key);
            });
        }
    }
    /// <summary>
    /// 数字键
    /// </summary>
    /// <param name="Key"></param>
    public void NumberBtn(string Key)
    {
        BtnAudio.Play();
        PlayID_Key.text = PlayID_Key.text + transform.transform.Find("Obj_FigureTheParentObject").Find(Key).GetChild(0).GetComponent<Text>().text;
    }
 
    /// <summary>
    /// 退格键
    /// </summary>
    public void DeleteButton()
    {
        BtnAudio.Play();
        if (PlayID_Key.text.Length > 0)
        {
            PlayID_Key.text = PlayID_Key.text.Remove(PlayID_Key.text.Length - 1);
        }
    }

    private void btn_Ok_Button()
    {
        BtnAudio.Play();
    }
}
