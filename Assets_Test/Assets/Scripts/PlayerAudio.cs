﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAudio : MonoBehaviour
{
    public static PlayerAudio Instance;
    private AudioSource Play_Audio;
    private AudioSource Play_Audio1;
    private AudioSource Play_CDGDLAudio;
    private AudioSource Play_CDQDLAudio;
    private AudioSource Play_CDGGGAudio;
    private AudioSource Btn_Audio;
    public List<AudioClip> ListAudio;
    public AudioClip BtnAudioClip;

    public List<AudioClip> ListAudioTest;


    private void Awake()
    {
        Instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        Play_Audio = GameObject.Find("PlayerAudio"). transform.GetComponent<AudioSource>();
        Play_Audio1 = GameObject.Find("PlayerAudio1"). transform.GetComponent<AudioSource>();
        Btn_Audio = GameObject.Find("BtnAudio"). transform.GetComponent<AudioSource>();
        Play_CDGDLAudio = GameObject.Find("CDGDL_Audio"). transform.GetComponent<AudioSource>();
        Play_CDQDLAudio = GameObject.Find("CDQDL_Audio"). transform.GetComponent<AudioSource>();
        Play_CDGGGAudio = GameObject.Find("CDGGG_Audio"). transform.GetComponent<AudioSource>();
        Btn_Audio.clip = BtnAudioClip;
    }


    public void PlayAudioClipTest(int i)
    {
        StopAudio();
        Play_Audio1.clip = ListAudioTest[i];
        Play_Audio1.Play();
    }

    //public void PlayAudioClip(int i)
    //{
    //    StopAudio();
    //    Play_Audio.clip = ListAudio[i];
    //    Play_Audio.Play();
    //}

    public void StopAudio()
    {
        Play_Audio.Stop();
        Play_Audio1.Stop();
        Play_CDGGGAudio.Stop();
    }

    public void PlayCDGDL()
    {
        Play_CDGDLAudio.Play();
        Play_CDGDLAudio.clip = ListAudioTest[47];
    }
    public void StopDL()
    {
        Play_CDQDLAudio.Stop();
        Play_CDGDLAudio.Stop();
    }
    public void PlayCDQDL()
    {
        Play_CDQDLAudio.Play();
        Play_CDQDLAudio.clip = ListAudioTest[47];
    }

    public void PlayCDGGG()
    {
        Play_CDGGGAudio.Play();
        Play_CDGGGAudio.clip = ListAudioTest[50];
    }


    public void PlayBtnAudio()
    {
        Btn_Audio.Play();
    }


    // Update is called once per frame
    void Update()
    {
        if (Play_Audio !=null && Play_Audio1 != null)
        {
            if (Play_Audio.isPlaying || Play_Audio1.isPlaying)
            {
                GameObjectIns.Instance.IsPlayAudio = true;
            }
            else
            {
                GameObjectIns.Instance.IsPlayAudio = false;
            }
        }
      
    }
}
