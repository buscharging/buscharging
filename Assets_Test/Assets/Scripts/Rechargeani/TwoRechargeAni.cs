﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TwoRechargeAni : MonoBehaviour
{
    private void OnEnable()
    {

        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).transform.gameObject.SetActive(false);
        }
        StartCoroutine(PlayerAni());
    }

    IEnumerator PlayerAni()
    {
        while (true)
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                for (int j = 0; j < transform.childCount; j++)
                {
                    transform.GetChild(j).transform.gameObject.SetActive(false);
                }
                transform.GetChild(i).transform.gameObject.SetActive(true);
                yield return new WaitForSeconds(0.2f);
            }
            for (int i = 0; i < transform.childCount; i++)
            {
                transform.GetChild(i).transform.gameObject.SetActive(false);
            }
        }
    }
    private void OnDisable()
    {
        StopCoroutine(PlayerAni());
    }
}
