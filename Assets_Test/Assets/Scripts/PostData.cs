﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class PostData : MonoBehaviour
{
    public TestValue_Manager tvm;
    public void Login()
    {
        Login(TestValue.PlayID);
    }

    public void Logout()
    {
        Logout(TestValue.PlayID, TestValue.PlayScore);
    }

    // Use this for initialization
    public void Login(int userID)
    {
        StartCoroutine(IEPost($"record?cmd={0}&userid={userID}&score={0}"));
    }

    public void Logout(int userID, int score)
    {
        StartCoroutine(IEPost($"record?cmd={1}&userid={userID}&score={score}"));
    }

    IEnumerator IEPost(string cmd)
    {
        string fullUrl = $"http://{TestValue.Ip}:8000/{cmd}";

        //加入http 头信息
        Dictionary<string, string> JsonDic = new Dictionary<string, string>();
        JsonDic.Add("Content-Type", "application/json");

        //转换为字节
        byte[] post_data;
        post_data = System.Text.UTF8Encoding.UTF8.GetBytes("XXX");

        WWW www = new WWW(fullUrl, post_data, JsonDic);
        yield return www;

        if (www.error != null)
        {
            tvm.hint_text.text = "错误:无法连接到目标主机";
            Debug.LogError("error:" + www.error);
            TestValue.info = "error:" + www.error;
            yield break;
        }
        Debug.Log(www.text);
        tvm.hint_text.text = "考试编号错误";
        var operationText = cmd.ToLower().Contains("cmd=0") ? "登入" : "登出";
        if (www.text.ToLower().Contains("false"))
        {
            TestValue.info = $"无此用户!无法正常{operationText}";

            //tvm.hint_text.text = TestValue.info;
        }
        else
        {
            tvm.hint_text.text=$"{operationText}成功";
            TestValue.info = $"{operationText}成功";

            if (TestValue.info == "登入成功")
            {
                tvm.hint_text.text = TestValue.info;

                DontDestroyOnLoad(this);

                SceneManager.LoadScene("ParkingLotScene");
            }
            else
            {
                TestValue.PlayID = 0;
                TestValue.PlayScore = 0;
                SceneManager.LoadScene("Login");
            }
        }
    }
}
public class TestValue
{
    public static int PlayID;
    public static int PlayScore;
    public static string Ip = "192.168.0.110";
    public static string info = "";
}
