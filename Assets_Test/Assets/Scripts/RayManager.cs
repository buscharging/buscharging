﻿using Pvr_UnitySDKAPI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class RayManager : MonoBehaviour
{
    public static RayManager Instance;
    private void Awake()
    {
        Instance = this;
    }
    //左手手柄碰到物体的名字
    public string L_RayName;
    //右手手柄碰到物体的名字
    public string R_RayName;

    private void Start()
    {
        L_RayName = "4";
        R_RayName = "4";
    }
    // Update is called once per frame
    void Update()
    {
        InputDownEvent();
    }

    private void InputDownEvent()
    {
        //按下左手手柄
        if (Controller.UPvr_GetKeyDown(0, Pvr_KeyCode.TRIGGER) || Input.GetMouseButtonDown(0) && GameObjectIns.Instance.L_Ray.activeInHierarchy)
        {
            //播放声音 左手模型显示情况下 跳过
            if (GameObjectIns.Instance.IsPlayAudio || GameObjectIns.Instance.vr_cartoon_hand_prefab_Left.activeInHierarchy)
                return;

            L_BtnEvent();
        }
        //按下右手手柄
        if (Controller.UPvr_GetKeyDown(1, Pvr_KeyCode.TRIGGER) || Input.GetMouseButtonDown(1) && GameObjectIns.Instance.R_Ray.activeInHierarchy)
        {
            //播放声音 右手模型显示情况下 跳过
            if (GameObjectIns.Instance.IsPlayAudio || GameObjectIns.Instance.vr_cartoon_hand_prefab_Right.activeInHierarchy)
                return;

            R_BtnEvent();
        }
    }
    private void L_BtnEvent()
    {
        if (L_RayName == "TrainingBtn")
        {
            TrainingBtnEvent();
        }
        else if (L_RayName == "TestBtn")
        {
            TestBtnBtnEvent();
        }
        else if (L_RayName == "ChargingBowBtn")
        {
            ChargingBowBtnEvent();
        }
        else if (L_RayName == "EVChargerBtn")
        {
            EVChargerBtnEvent();
        }
        else if (L_RayName == "StartGameBtn")
        {
            StartGameBtnEvent();
        }
        else if (L_RayName == "CDG_ani1")
        {
            if (GameObjectIns.Instance.IsTest && GameObjectIns.Instance.IsTestFalse == false && GameObjectIns.Instance.Player.transform.GetChild(0).name == "test")
            {
                GameObjectIns.Instance.TestCDG_arrow.SetActive(false);
                GameObjectIns.Instance.TestCDG_arrow1.SetActive(false);
                PlayerAudio.Instance.PlayBtnAudio();
                print("点击了：" + L_RayName + " 选择正确");
                GameObjectIns.Instance.CDGIsDown = false;
                GameObjectIns.Instance.Che_Test_Hint.SetActive(true);
                GameObjectIns.Instance.DOTweenMove(GameObjectIns.Instance.Che_Test_Hint, new Vector3(-23f, 1f, -37.398f), 1f);
                print("39请根据车型用手柄选择正确的停车位置并扣扳机");
                PlayerAudio.Instance.PlayAudioClipTest(39);
            }

        }
        else if (L_RayName == "CDG_ani")
        {
            if (GameObjectIns.Instance.IsTest && GameObjectIns.Instance.IsTestFalse == false && GameObjectIns.Instance.Player.transform.GetChild(0).name == "test")
            {
                GameObjectIns.Instance.TestCDG_arrow.SetActive(false);
                GameObjectIns.Instance.TestCDG_arrow1.SetActive(false);
                PlayerAudio.Instance.PlayBtnAudio();
                print("40操作错误！请重新培训后再进行考试");
                PlayerAudio.Instance.PlayAudioClipTest(40);
                GameObjectIns.Instance.IsTestFalse = true;
                Invoke("TestModel_TestOver", 5f);
            }

        }
        else if (L_RayName == "Canvas10_2m_Image")
        {
            if (GameObjectIns.Instance.IsTest && GameObjectIns.Instance.IsTestFalse == false && GameObjectIns.Instance.Player.transform.GetChild(0).name == "test")
            {
                PlayerAudio.Instance.PlayBtnAudio();
                print("选择了10.2米");
                GameObjectIns.Instance.DOTweenMove(GameObjectIns.Instance.Che_Test_Hint, new Vector3(-23f, -0.6f, -37.398f), 1f);
                GameObjectIns.Instance.PlayerHint_Canvas.SetActive(true);
                GameObjectIns.Instance.PlayerHint_Text.text = "开始测验充电弓模式";
                print("41下面进入充电弓操作流程考试");
                PlayerAudio.Instance.PlayAudioClipTest(41);
                Invoke("Test_ZC", 8f);
            }

        }
        else if (L_RayName == "Canvas6_5m_Image")
        {
            if (GameObjectIns.Instance.IsTest && GameObjectIns.Instance.IsTestFalse == false && GameObjectIns.Instance.Player.transform.GetChild(0).name == "test")
            {
                PlayerAudio.Instance.PlayBtnAudio();
                print("选择了6.5米");
                print("40操作错误！请重新培训后再进行考试");
                PlayerAudio.Instance.PlayAudioClipTest(40);
                GameObjectIns.Instance.IsTestFalse = true;
                Invoke("TestModel_TestOver", 5f);
            }

        }
        else if (L_RayName == "CDGSS_Over_Button")
        {
            ReturnCDG_Model();
        }
        else if (L_RayName == "CDGSS_Over_Button1")
        {
            GameObjectIns.Instance.PlayerHint_Canvas.SetActive(false);
            PlayerAudio.Instance.PlayBtnAudio();
            GameObjectIns.Instance.CDGSS_Over_Button1.gameObject.SetActive(false);
            GameObjectIns.Instance.AKeyToStartOver();
            GameObjectIns.Instance.SetHandModel(true);
        }
    }
    private void R_BtnEvent()
    {
        if (R_RayName == "TrainingBtn")
        {
            TrainingBtnEvent();
        }
        else if (R_RayName == "TestBtn")
        {
            TestBtnBtnEvent();
        }
        else if (R_RayName == "ChargingBowBtn")
        {
            ChargingBowBtnEvent();
        }
        else if (R_RayName == "EVChargerBtn")
        {
            EVChargerBtnEvent();
        }
        else if (R_RayName == "StartGameBtn")
        {
            StartGameBtnEvent();
        }
        else if (R_RayName == "CDG_ani1")
        {
            if (GameObjectIns.Instance.IsTest && GameObjectIns.Instance.IsTestFalse == false && GameObjectIns.Instance.Player.transform.GetChild(0).name == "test")
            {
                GameObjectIns.Instance.TestCDG_arrow.SetActive(false);
                GameObjectIns.Instance.TestCDG_arrow1.SetActive(false);
                PlayerAudio.Instance.PlayBtnAudio();
                print("点击了：" + L_RayName + " 选择正确");
                GameObjectIns.Instance.CDGIsDown = false;
                GameObjectIns.Instance.Che_Test_Hint.SetActive(true);
                GameObjectIns.Instance.DOTweenMove(GameObjectIns.Instance.Che_Test_Hint, new Vector3(-23f, 1f, -37.398f), 1f);
                print("39请根据车型用手柄选择正确的停车位置并扣扳机");
                PlayerAudio.Instance.PlayAudioClipTest(39);
            }

        }
        else if (R_RayName == "CDG_ani")
        {
            if (GameObjectIns.Instance.IsTest && GameObjectIns.Instance.IsTestFalse == false && GameObjectIns.Instance.Player.transform.GetChild(0).name == "test")
            {
                GameObjectIns.Instance.TestCDG_arrow.SetActive(false);
                GameObjectIns.Instance.TestCDG_arrow1.SetActive(false);
                PlayerAudio.Instance.PlayBtnAudio();
                print("40操作错误！请重新培训后再进行考试");
                PlayerAudio.Instance.PlayAudioClipTest(40);
                GameObjectIns.Instance.IsTestFalse = true;
                Invoke("TestModel_TestOver", 5f);
            }

        }
        else if (R_RayName == "Canvas10_2m_Image")
        {
            if (GameObjectIns.Instance.IsTest && GameObjectIns.Instance.IsTestFalse == false && GameObjectIns.Instance.Player.transform.GetChild(0).name == "test")
            {
                PlayerAudio.Instance.PlayBtnAudio();
                print("选择了10.2米");
                GameObjectIns.Instance.DOTweenMove(GameObjectIns.Instance.Che_Test_Hint, new Vector3(-23f, -0.6f, -37.398f), 1f);
                GameObjectIns.Instance.PlayerHint_Canvas.SetActive(true);
                GameObjectIns.Instance.PlayerHint_Text.text = "开始测验充电弓模式";
                print("41下面进入充电弓操作流程考试");
                PlayerAudio.Instance.PlayAudioClipTest(41);
                Invoke("Test_ZC", 8f);
            }

        }
        else if (R_RayName == "Canvas6_5m_Image")
        {
            if (GameObjectIns.Instance.IsTest && GameObjectIns.Instance.IsTestFalse == false && GameObjectIns.Instance.Player.transform.GetChild(0).name == "test")
            {
                PlayerAudio.Instance.PlayBtnAudio();
                print("选择了6.5米");
                print("40操作错误！请重新培训后再进行考试");
                PlayerAudio.Instance.PlayAudioClipTest(40);
                GameObjectIns.Instance.IsTestFalse = true;
                Invoke("TestModel_TestOver", 5f);
            }

        }
        else if (R_RayName == "CDGSS_Over_Button")
        {
            ReturnCDG_Model();
        }
        else if (R_RayName == "CDGSS_Over_Button1")
        {
            GameObjectIns.Instance.Player.transform.GetChild(0).name = "c0";
            GameObjectIns.Instance.PlayerHint_Canvas.SetActive(false);
            PlayerAudio.Instance.PlayBtnAudio();
            GameObjectIns.Instance.CDGSS_Over_Button1.gameObject.SetActive(false);
            GameObjectIns.Instance.AKeyToStartOver();
            GameObjectIns.Instance.SetHandModel(true);
        }
    }

    /// <summary>
    /// 培训模式按钮事件
    /// </summary>
    public void TrainingBtnEvent()
    {
        PlayerAudio.Instance.PlayBtnAudio();
        ChoiceTrainingModel();
    }
    /// <summary>
    /// 测试模式事件
    /// </summary>
    public void TestBtnBtnEvent()
    {
        PlayerAudio.Instance.PlayBtnAudio();
        ChoiceTestModel();
    }
    /// <summary>
    /// 开始培训
    /// </summary>
    public void ChoiceTrainingModel()
    {
        print("开始培训");
        GameObjectIns.Instance.IsTest = false;
        transform.gameObject.AddComponent<TrainingModel>();
        Destroy(transform.GetComponent<ChoiceModel>());
    }
    /// <summary>
    /// 开始测验
    /// </summary>
    public void ChoiceTestModel()
    {
        print("开始测验");
        GameObjectIns.Instance.SetCutTo(0f, 1f, 2f);
        GameObjectIns.Instance.Canvas.SetActive(false);
        //PlayerAudio.Instance.PlayAudioClip(20);
        Invoke("SetPlayPos", 2f);
    }
    /// <summary>
    /// 设置玩家位置
    /// </summary>
    private void SetPlayPos()
    {
        GameObjectIns.Instance.TestModel_Water_Plane.SetActive(true);
        GameObjectIns.Instance.TestCDG_arrow.SetActive(true);
        GameObjectIns.Instance.TestCDG_arrow1.SetActive(true);
        GameObjectIns.Instance.IsTest = true;
        GameObjectIns.Instance.SetCutTo(1f, 0f, 2f);
        GameObjectIns.Instance.Player.transform.localPosition = new Vector3(-24.3f, 1.8f, -45.43f);
        GameObjectIns.Instance.Player.transform.localEulerAngles = PlayerPosition.PlayerCDGDownBusRot;
        Invoke("PlayTestModelAudio", 2f);
    }
    /// <summary>
    /// 播放语音
    /// </summary>
    private void PlayTestModelAudio()
    {
        print("38欢迎进入特来电公交车充电VR考试，请在语音提示结束后开始操作。请用手柄选择正确的充电场地编号并扣扳机");
        PlayerAudio.Instance.PlayAudioClipTest(38);
        Invoke("DesTans_CM", 13f);
    }
    /// <summary>
    /// 删除ChoiceModel脚本
    /// </summary>
    private void DesTans_CM()
    {
        GameObjectIns.Instance.Player.transform.GetChild(0).name = "test";
        Destroy(transform.GetComponent<ChoiceModel>());
    }
    /// <summary>
    /// 充电弓模式
    /// </summary>
    private void ChargingBowBtnEvent()
    {
        if (Pvr_UnitySDKAPI.Controller.UPvr_GetKey(0, Pvr_KeyCode.TRIGGER))
        {
            GameObjectIns.Instance.VibateController(0, 100, 1);
        }
        else if (Pvr_UnitySDKAPI.Controller.UPvr_GetKey(1, Pvr_KeyCode.TRIGGER))
        {
            GameObjectIns.Instance.VibateController(1, 100, 1);
        }
        PlayerAudio.Instance.PlayBtnAudio();
        //GameObjectIns.Instance.SetPlayerHint_Canvas_Test("请把手柄靠近手刹，请注意不要关钥匙");
        print("配音：请把手柄靠近手刹，请注意不要关钥匙");
        //PlayerAudio.Instance.PlayAudioClip(16);
        GameObjectIns.Instance.Canvas1.SetActive(false);
        //CloseShouSha();

        //显示手模型     隐藏手柄
        GameObjectIns.Instance.vr_cartoon_hand_prefab_Left.gameObject.SetActive(true);
        GameObjectIns.Instance.vr_cartoon_hand_prefab_Right.gameObject.SetActive(true);

        GameObjectIns.Instance.vr_cartoon_hand_prefab_Left.transform.parent.transform.GetComponent<MeshRenderer>().enabled = false;
        GameObjectIns.Instance.vr_cartoon_hand_prefab_Right.transform.parent.transform.GetComponent<MeshRenderer>().enabled = false;

        //GameObjectIns.Instance.R_Hand_Dot.enabled = false;
        //GameObjectIns.Instance.R_Hand_Ray.enabled = false;
        //GameObjectIns.Instance.L_Hand_Dot.enabled = false;
        //GameObjectIns.Instance.L_Hand_Ray.enabled = false;


        GameObjectIns.Instance.vr_cartoon_hand_prefab_Left.AddComponent<LeftShouShaTrigger>();
        GameObjectIns.Instance.vr_cartoon_hand_prefab_Right.AddComponent<RightYaoShiTrigger>();

        Destroy(transform.GetComponent<TrainingModel1>());
    }
    /// <summary>
    /// 充电枪模式
    /// </summary>
    private void EVChargerBtnEvent()
    {
        //隐藏手柄模型 射线 显示手模型
        GameObjectIns.Instance.vr_cartoon_hand_prefab_Left.gameObject.SetActive(true);
        GameObjectIns.Instance.vr_cartoon_hand_prefab_Right.gameObject.SetActive(true);
        GameObjectIns.Instance.vr_cartoon_hand_prefab_Left.transform.parent.transform.GetComponent<MeshRenderer>().enabled = false;
        GameObjectIns.Instance.vr_cartoon_hand_prefab_Right.transform.parent.transform.GetComponent<MeshRenderer>().enabled = false;

        GameObjectIns.Instance.vr_cartoon_hand_prefab_Left.AddComponent<LeftShouTrigger>();
        GameObjectIns.Instance.vr_cartoon_hand_prefab_Right.AddComponent<RightShouTrigger>();

        Destroy(transform.GetComponent<TrainingModel1>());
    }
    /// <summary>
    /// 返回主页按钮 重新开始游戏
    /// </summary>
    private void StartGameBtnEvent()
    {
        if (Pvr_UnitySDKAPI.Controller.UPvr_GetKey(0, Pvr_KeyCode.TRIGGER))
        {
            GameObjectIns.Instance.VibateController(0, 100, 1);
        }
        else if (Pvr_UnitySDKAPI.Controller.UPvr_GetKey(1, Pvr_KeyCode.TRIGGER))
        {
            GameObjectIns.Instance.VibateController(1, 100, 1);
        }
        PlayerAudio.Instance.PlayBtnAudio();
        //Destroy(transform.GetComponent<TrainingModel1>());
        //GameObjectIns.Instance.transform.gameObject.AddComponent<ChoiceModel>();
        SceneManager.LoadScene("ParkingLotScene");
    }
    /// <summary>
    /// 测试模式测试失败
    /// </summary>
    private void TestModel_TestOver()
    {
        GameObjectIns.Instance.SetCutTo(0f, 1f, 2f);
        Invoke("SetCutToTM", 2f);
    }
    /// <summary>
    /// 重新培训停车模式
    /// </summary>
    private void SetCutToTM()
    {
        GameObjectIns.Instance.TestCDG_arrow.SetActive(false);
        GameObjectIns.Instance.TestCDG_arrow1.SetActive(false);
        GameObjectIns.Instance.TestModel_Water_Plane.SetActive(false);
        GameObjectIns.Instance.DOTweenMove(GameObjectIns.Instance.Che_Test_Hint, new Vector3(-23f, -0.6f, -37.398f), 0.001f);
        GameObjectIns.Instance.transform.gameObject.AddComponent<TrainingModel>();
    }
    /// <summary>
    /// 测试模式转场
    /// </summary>
    private void Test_ZC()
    {
        GameObjectIns.Instance.SetCutTo(0f, 1f, 2f);
        Invoke("Test_CDG_Model", 2f);
    }
    /// <summary>
    /// 测试充电弓模式
    /// </summary>
    private void Test_CDG_Model()
    {
        GameObjectIns.Instance.TestCDG_arrow.SetActive(false);
        GameObjectIns.Instance.TestCDG_arrow1.SetActive(false);

        GameObjectIns.Instance.SetHandModel(true);

        GameObjectIns.Instance.vr_cartoon_hand_prefab_Left.AddComponent<LeftShouCdgTrigger>();
        GameObjectIns.Instance.vr_cartoon_hand_prefab_Right.AddComponent<RightShouCdgTrigger>();

        //GameObjectIns.Instance.vr_cartoon_hand_prefab_Left.AddComponent<Left_CDQ_Test_Model>();
    }
    /// <summary>
    /// 看过了充电弓上升完毕 返回充电弓模式
    /// </summary>
    private void ReturnCDG_Model()
    {
        GameObjectIns.Instance.SetHandModel(true);
        GameObjectIns.Instance.IsDownBus = true;
        PlayerAudio.Instance.PlayBtnAudio();
        GameObjectIns.Instance.SetCDGCanvasHint();
        GameObjectIns.Instance.CDGSS_Over_Button.gameObject.SetActive(false);
    }
}
