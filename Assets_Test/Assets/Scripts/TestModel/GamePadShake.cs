﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamePadShake : MonoBehaviour
{
    public int hand;

    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        GameObjectIns.Instance.VibateController(hand, 100, 1);
    }

    private void OnCollisionEnter(Collision collision)
    {
        GameObjectIns.Instance.VibateController(hand, 100,1);
    }


}
