﻿using Pvr_UnitySDKAPI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RightShouCdgTrigger : MonoBehaviour
{
    /// <summary>
    /// 参考LeftShouCdgTrigger脚本注释
    /// </summary>
    // Start is called before the first frame update
    void Start()
    {
        GameObjectIns.Instance.IsDownBus = false;
        if (GameObjectIns.Instance.vr_cartoon_hand_prefab_Left.activeInHierarchy == false)
        {
            GameObjectIns.Instance.vr_cartoon_hand_prefab_Left.SetActive(true);
        }
        for (int g = 0; g < GameObjectIns.Instance.JianShuDaiParent.transform.childCount; g++)
        {
            GameObjectIns.Instance.SetHight(GameObjectIns.Instance.JianShuDaiParent.transform.GetChild(g).gameObject, false);
        }
        GameObjectIns.Instance.TestModel_Water_Plane.SetActive(false);
        CancelInvoke();
    }



    // Update is called once per frame
    void Update()
    {
        if (GameObjectIns.Instance.IsTestFalse == false)
        {
            if (Controller.UPvr_GetKeyDown(0, Pvr_KeyCode.Y) || Controller.UPvr_GetKeyDown(1, Pvr_KeyCode.Y) || Input.GetKeyDown(KeyCode.Y) && GameObjectIns.Instance.IsDownBus == false)
            {
                GameObjectIns.Instance.IsDownBus = true;
                GameObjectIns.Instance.Invoke("SetIsDownBus", 2f);

                GameObjectIns.Instance.PlayerHint_Canvas.SetActive(false);
                GameObjectIns.Instance.SetCutTo(0f, 1f, 2f);
                Invoke("SetPlayPosDown", 2f);
            }
            else if (Controller.UPvr_GetKeyDown(0, Pvr_KeyCode.X) || Controller.UPvr_GetKeyDown(1, Pvr_KeyCode.X) || Input.GetKeyDown(KeyCode.X) && GameObjectIns.Instance.IsDownBus == false)
            {
                GameObjectIns.Instance.IsDownBus = true;
                GameObjectIns.Instance.Invoke("SetIsDownBus", 2f);

                //GameObjectIns.Instance.vr_cartoon_hand_prefab_Left.gameObject.SetActive(true);
                //GameObjectIns.Instance.vr_cartoon_hand_prefab_Left.transform.parent.GetComponent<MeshRenderer>().enabled = false;

                GameObjectIns.Instance.PlayerHint_Canvas.SetActive(false);
                GameObjectIns.Instance.SetCutTo(0f, 1f, 2f);
                Invoke("SetPlayPosUP", 2f);
            }
        }
    }

    private void SetPlayPosDown()
    {
        GameObjectIns.Instance.SetCutTo(1f, 0f, 1.5f);
        GameObjectIns.Instance.Player.transform.localEulerAngles = PlayerPosition.PlayerCDGDownBusRot;
        GameObjectIns.Instance.Player.transform.position = PlayerPosition.PlayerCDGDownBusPos;
    }

    private void SetPlayPosUP()
    {
        GameObjectIns.Instance.SetCutTo(1f, 0f, 1.5f);
        GameObjectIns.Instance.Player.transform.localEulerAngles = Vector3.zero;
        GameObjectIns.Instance.Player.transform.position = PlayerPosition.PlayerCDGBusPos;
    }

    // 重置 ： 刹车 钥匙 CDG UI 
    private void OnTriggerEnter(Collider other)
    {
        print(other.transform.name);
        if (GameObjectIns.Instance.IsPlayAudio == false && GameObjectIns.Instance.IsTestFalse == false)
        {
            if (other.transform.name == "YaoShi")
            {
                PlayerAudio.Instance.PlayBtnAudio();
                GameObjectIns.Instance.VibateController(1, 100, 1);
                print("充电弓模式不需要关钥匙 操作错误");
                TestOver();
            }
            else if (other.transform.name == "ShouSha001")
            {
                if (GameObjectIns.Instance.IsRecharge == false)
                {
                    if (GameObjectIns.Instance.IsShouSha == false)
                    {
                        GameObjectIns.Instance.PlayerHint_Canvas.SetActive(false);
                        PlayerAudio.Instance.PlayBtnAudio();
                        GameObjectIns.Instance.VibateController(1, 100, 1);
                        if (GameObjectIns.Instance.IsPlayAudio == false)
                        {
                            print("手刹");
                            //PlayerAudio.Instance.PlayAudioClip(12);

                            GameObjectIns.Instance.SetC2ChildFalse();
                            GameObjectIns.Instance.AKeyToStart_Button.gameObject.SetActive(true);

                            GameObjectIns.Instance.CloseShouSha(true, 2f);

                            //GameObjectIns.Instance.RawImage_Camera0.SetActive(true);
                            //GameObjectIns.Instance.RawImage_Camera1.SetActive(false);
                            GameObjectIns.Instance.NoDeclineCDG_Img.SetActive(false);
                        }
                        else
                        {
                            print("钥匙已关闭");
                        }
                    }
                    else
                    {
                        print("手刹已关闭");
                    }
                }
                else
                {
                    PlayerAudio.Instance.PlayBtnAudio();
                    GameObjectIns.Instance.VibateController(1, 100, 1);
                    print("充电中 不能拉手刹挡位 测验失败   IsRecharge:" + GameObjectIns.Instance.IsRecharge);
                    TestOver();
                }
            }
            else if (other.transform.name == "DangWei_N")
            {
                if (GameObjectIns.Instance.IsRecharge == false)
                {
                    if (GameObjectIns.Instance.DangWei_N == false)
                    {
                        GameObjectIns.Instance.PlayerHint_Canvas.SetActive(false);
                        PlayerAudio.Instance.PlayBtnAudio();
                        GameObjectIns.Instance.VibateController(1, 100, 1);
                        GameObjectIns.Instance.DangWei_N = true;
                        GameObjectIns.Instance.IsYaoShiShouShaOver();
                    }
                }
                else
                {
                    PlayerAudio.Instance.PlayBtnAudio();
                    GameObjectIns.Instance.VibateController(1, 100, 1);
                    print("充电中 不能拉手刹挡位 测验失败 ");
                    TestOver();
                }

            }
            else if (other.transform.name == "AKeyToStart_Button")
            {
                PlayerAudio.Instance.PlayBtnAudio();
                GameObjectIns.Instance.VibateController(1, 100, 1);
            }
            else if (other.transform.name == "OneRecharge_Btn")
            {
                PlayerAudio.Instance.PlayBtnAudio();
                GameObjectIns.Instance.VibateController(1, 100, 1);
                //PlayerAudio.Instance.PlayAudioClipTest(16);
                print("充电弓选择错误 测验失败");
                TestOver();
            }
            else if (other.transform.name == "TwoRecharge_Btn")
            {
                PlayerAudio.Instance.PlayBtnAudio();
                GameObjectIns.Instance.VibateController(1, 100, 1);
            }
            else if (other.transform.name == "ThreeRecharge_Btn")
            {
                PlayerAudio.Instance.PlayBtnAudio();
                GameObjectIns.Instance.VibateController(1, 100, 1);
                //PlayerAudio.Instance.PlayAudioClipTest(16);
                print("充电弓选择错误 测验失败");
                TestOver();
            }
            else if (other.transform.name == "FourRecharge_Btn")
            {
                PlayerAudio.Instance.PlayBtnAudio();
                GameObjectIns.Instance.VibateController(1, 100, 1);
                //PlayerAudio.Instance.PlayAudioClipTest(16);
                print("充电弓选择错误 测验失败");
                TestOver();
            }
            else if (other.transform.name == "RechargeDown_Btn")
            {
                PlayerAudio.Instance.PlayBtnAudio();
                GameObjectIns.Instance.VibateController(1, 100, 1);
            }
            else if (other.transform.name == "RechargeUp_Btn")
            {
                PlayerAudio.Instance.PlayBtnAudio();
                GameObjectIns.Instance.VibateController(1, 100, 1);
            }
            else if (other.transform.name == "RechargeUp_Btn1")
            {
                PlayerAudio.Instance.PlayBtnAudio();
                GameObjectIns.Instance.VibateController(1, 100, 1);
            }
            else if (other.transform.name == "StartRecharge_Btn")
            {
                PlayerAudio.Instance.PlayBtnAudio();
                GameObjectIns.Instance.VibateController(1, 100, 1);
            }
            else if (other.transform.name == "AKeyToStartFail_Btn")
            {
                PlayerAudio.Instance.PlayBtnAudio();
                GameObjectIns.Instance.VibateController(1, 100, 1);
            }
            else if (other.transform.name == "OverRecharge_Btn")
            {
                PlayerAudio.Instance.PlayBtnAudio();
                GameObjectIns.Instance.VibateController(1, 100, 1);
            }
            else if (other.transform.name == "Homepage_Btn")
            {
                PlayerAudio.Instance.PlayBtnAudio();
                GameObjectIns.Instance.VibateController(1, 100, 1);
            }
        }
    }

    private void hit_AKeyToStartHit_Image()
    {
        GameObjectIns.Instance.SetC2ChildFalse();
        GameObjectIns.Instance.AKeyToStartFail_Btn.gameObject.SetActive(true);
    }

    private void OnTriggerExit(Collider other)
    {
        print(other.transform.name);
        if (GameObjectIns.Instance.IsPlayAudio == false && GameObjectIns.Instance.IsTestFalse == false)
        {
            if (other.transform.name == "AKeyToStart_Button")
            {
                if (GameObjectIns.Instance.IsShouSha == false || GameObjectIns.Instance.DangWei_N == false)
                {
                    print("手刹或挡位未关闭 不允许降弓 测验失败");
                    TestOver();
                    //GameObjectIns.Instance.SetC2ChildFalse();
                    //GameObjectIns.Instance.NoDeclineCDG_Img.SetActive(true);
                    GameObjectIns.Instance.PlayerHint_Canvas.SetActive(true);
                    GameObjectIns.Instance.PlayerHint_Text.text = "手刹或挡位未关闭 不允许降弓 测验失败";
                }
                else
                {
                    GameObjectIns.Instance.PlayerHint_Canvas.gameObject.SetActive(false);
                    print("一键启动失败 请开始手动模式");
                    GameObjectIns.Instance.RechargeTime = 0f;
                    //PlayerAudio.Instance.PlayAudioClip(7);
                    //print("4在有些情况下“一键充电”如果失败，我们可以使用手动模式充电");
                    //PlayerAudio.Instance.PlayAudioClipTest(4);
                    GameObjectIns.Instance.SetC2ChildFalse();
                    GameObjectIns.Instance.AKeyToStartHit_Image.SetActive(true);
                    Invoke("hit_AKeyToStartHit_Image", 3f);
                }
            }
            else if (other.transform.name == "OneRecharge_Btn")
            {
                print("一号充电弓");
            }
            else if (other.transform.name == "TwoRecharge_Btn")
            {
                CancelInvoke();
                print("二号充电弓");
                if (Vector3.Distance(GameObjectIns.Instance.ShouSha001.transform.parent.transform.position, GameObjectIns.Instance.CDG_ani1.transform.position) < 5f)
                {
                    GameObjectIns.Instance.SetHight(GameObjectIns.Instance.CDG_arrow, false);
                    GameObjectIns.Instance.CDG_arrow.SetActive(false);
                    if (GameObjectIns.Instance.CDGIsDown == false)
                    {
                        GameObjectIns.Instance.SetC2ChildFalse();
                        GameObjectIns.Instance.TwoRecharge_Img.SetActive(true);
                        GameObjectIns.Instance.TwoRecharge_Img.transform.GetChild(0).gameObject.SetActive(true);
                        GameObjectIns.Instance.TwoRecharge_Img.transform.GetChild(1).gameObject.SetActive(false);
                        print("9通信连接过程中，请稍后，等待通信连接完成");
                        //PlayerAudio.Instance.PlayAudioClipTest(9);
                        Invoke("TwoRechargeEvent", 2f);
                    }
                    else
                    {

                        print("正在充电");
                        print("12_2充电信息包括电池电量百分比（SOC），已充电量，充电功率，充电电流，充电电压。充电过程中，如果需要返回上一级菜单可点击左上角小房子按钮，如果需要提前结束充电，可点击终端面板右下角的“结束充电”按钮结束充电。");
                        //PlayerAudio.Instance.PlayAudioClipTest(12);
                        GameObjectIns.Instance.SetC2ChildFalse();
                        GameObjectIns.Instance.RechargeState_Image.transform.gameObject.SetActive(true);
                        GameObjectIns.Instance.OverRecharge_Btn.transform.gameObject.SetActive(true);
                        GameObjectIns.Instance.Homepage_Btn.gameObject.SetActive(true);

                        GameObjectIns.Instance.BG_Image0.SetActive(false);
                        GameObjectIns.Instance.BG_Image1.SetActive(true);
                    }
                }
                else
                {
                    print("位置不正确");
                }
            }
            else if (other.transform.name == "RechargeDown_Btn")
            {

                GameObjectIns.Instance.PlayerHint_Canvas.SetActive(true);
                GameObjectIns.Instance.PlayerHint_Text.text = "按左手手柄Y键可以下车查看，X键可以回到车内";

                
                GameObjectIns.Instance.vr_cartoon_hand_prefab_Left.gameObject.SetActive(false);
                GameObjectIns.Instance.vr_cartoon_hand_prefab_Left.transform.parent.GetComponent<MeshRenderer>().enabled = true;


                //GameObjectIns.Instance.RechargeDown_Btn.transform.parent.gameObject.SetActive(false);
                GameObjectIns.Instance.CDGAniDown();
                GameObjectIns.Instance.SetC2ChildFalse();
                GameObjectIns.Instance.RechargeDown_Image.SetActive(true);
                GameObjectIns.Instance.RechargeUp_Btn.gameObject.SetActive(true);
                print("7充电弓正在降落，请稍后，此时也可以点击屏幕右下角“充电弓上升”按钮，控制充电弓上升");
                //PlayerAudio.Instance.PlayAudioClipTest(7);

                GameObjectIns.Instance.CDGJL_arrow.SetActive(true);
                Invoke("CDGAniDownOver", 5f);
            }
            else if (other.transform.name == "RechargeUp_Btn")
            {
                CancelInvoke();
                GameObjectIns.Instance.PlayerHint_Canvas.SetActive(false);
                GameObjectIns.Instance.CDGJL_arrow.SetActive(true);
                GameObjectIns.Instance.IsRecharge = false;
                GameObjectIns.Instance.IsRecharge30 = false;
                print("充电弓正在上升 请稍后...");
                GameObjectIns.Instance.SetC2ChildFalse();
                GameObjectIns.Instance.RechargeOver_Image.gameObject.SetActive(true);
                GameObjectIns.Instance.RechargeOver_Image.transform.GetChild(0).gameObject.SetActive(true);
                GameObjectIns.Instance.RechargeOver_Image.transform.GetChild(1).gameObject.SetActive(false);
                GameObjectIns.Instance.CDGAniUP();

                GameObjectIns.Instance.BG_Image0.SetActive(true);
                GameObjectIns.Instance.BG_Image1.SetActive(false);
                Invoke("CDGAniUpOver", 10f);
            }
            else if (other.transform.name == "RechargeUp_Btn1")
            {
                GameObjectIns.Instance.PlayerHint_Canvas.SetActive(false);
                print("充电弓正在上升 请稍后...");
                GameObjectIns.Instance.CDGJL_arrow.SetActive(true);
                GameObjectIns.Instance.IsRecharge = false;
                GameObjectIns.Instance.IsRecharge30 = false;
                GameObjectIns.Instance.SetC2ChildFalse();
                GameObjectIns.Instance.RechargeOver_Image.gameObject.SetActive(true);
                GameObjectIns.Instance.RechargeOver_Image.transform.GetChild(0).gameObject.SetActive(true);
                GameObjectIns.Instance.RechargeOver_Image.transform.GetChild(1).gameObject.SetActive(false);
                GameObjectIns.Instance.CDGAniUP();

                GameObjectIns.Instance.BG_Image0.SetActive(true);
                GameObjectIns.Instance.BG_Image1.SetActive(false);
                Invoke("CDGAniUpOver", 10f);
            }
            else if (other.transform.name == "StartRecharge_Btn")
            {
                if (GameObjectIns.Instance.IsShouSha && GameObjectIns.Instance.DangWei_N)
                {
                    GameObjectIns.Instance.vr_cartoon_hand_prefab_Left.gameObject.SetActive(true);
                    GameObjectIns.Instance.vr_cartoon_hand_prefab_Left.transform.parent.GetComponent<MeshRenderer>().enabled = false;

                    GameObjectIns.Instance.PlayerHint_Canvas.SetActive(false);
                    print("9通信连接过程中，请稍后，等待通信连接完成");
                    //PlayerAudio.Instance.PlayAudioClipTest(9);
                    GameObjectIns.Instance.SetC2ChildFalse();
                    GameObjectIns.Instance.TwoRecharge_Img.SetActive(true);
                    GameObjectIns.Instance.TwoRecharge_Img.transform.GetChild(0).gameObject.SetActive(false);
                    GameObjectIns.Instance.TwoRecharge_Img.transform.GetChild(1).gameObject.SetActive(true);
                    GameObjectIns.Instance.BG_Image0.SetActive(true);
                    GameObjectIns.Instance.BG_Image1.SetActive(false);
                    Invoke("StartRecharge_Btn_Event", 2f);
                }
                else
                {
                    print("手刹挡位未关闭 测验失败");
                    TestOver();
                }
            }
            else if (other.transform.name == "AKeyToStartFail_Btn")
            {
                print("开始手动模式");
                if (GameObjectIns.Instance.CDGIsDown == false)
                {
                    SetAKeyToStartFail_Btn();
                }
                else
                {
                    print("2正在充电");
                    GameObjectIns.Instance.SetC2ChildFalse();

                    GameObjectIns.Instance.OneRecharge_Btn.gameObject.SetActive(true);

                    GameObjectIns.Instance.TwoRecharge_Btn.gameObject.SetActive(true);
                    GameObjectIns.Instance.TwoRecharge_Btn.transform.GetChild(1).transform.gameObject.SetActive(false);
                    GameObjectIns.Instance.TwoRecharge_Btn.transform.GetChild(2).transform.gameObject.SetActive(true);

                    GameObjectIns.Instance.ThreeRecharge_Btn.gameObject.SetActive(true);
                    GameObjectIns.Instance.FourRecharge_Btn.gameObject.SetActive(true);
                }
            }
            else if (other.transform.name == "OverRecharge_Btn")
            {
                GameObjectIns.Instance.PlayerHint_Canvas.SetActive(true);
                GameObjectIns.Instance.PlayerHint_Text.text = "按左手手柄Y键可以下车查看，X键可以回到车内";



                GameObjectIns.Instance.vr_cartoon_hand_prefab_Left.gameObject.SetActive(false);
                GameObjectIns.Instance.vr_cartoon_hand_prefab_Left.transform.parent.GetComponent<MeshRenderer>().enabled = true;



                print("13充电结束后，充电弓自动上升，可通过监控或者下车查看充电弓是否上升成功");
                //PlayerAudio.Instance.PlayAudioClipTest(13);

                GameObjectIns.Instance.IsRecharge = false;
                GameObjectIns.Instance.CDGAniUP();

                GameObjectIns.Instance.CDGJL_arrow.SetActive(true);


                print("充电弓正在上升 请稍后...");
                //PlayerAudio.Instance.PlayAudioClip(4);
                GameObjectIns.Instance.SetC2ChildFalse();
                GameObjectIns.Instance.BG_Image0.SetActive(true);
                GameObjectIns.Instance.BG_Image1.SetActive(false);


                GameObjectIns.Instance.RechargeOver_Image.gameObject.SetActive(true);

                GameObjectIns.Instance.RechargeOver_Image.transform.GetChild(0).gameObject.SetActive(true);
                GameObjectIns.Instance.RechargeOver_Image.transform.GetChild(1).gameObject.SetActive(false);


                GameObjectIns.Instance.Invoke("CDGAniUpOver1", 8f);
            }
            else if (other.transform.name == "Homepage_Btn")
            {
                GameObjectIns.Instance.OneRecharge_Btn.transform.GetComponent<BoxCollider>().enabled = false;
                Invoke("SetOneBtn", 1f);
                other.transform.GetChild(0).gameObject.SetActive(false);
                GameObjectIns.Instance.BG_Image0.SetActive(true);
                GameObjectIns.Instance.BG_Image1.SetActive(false);
                GameObjectIns.Instance.TwoRecharge_Img.transform.GetChild(0).gameObject.SetActive(true);
                GameObjectIns.Instance.TwoRecharge_Img.transform.GetChild(1).gameObject.SetActive(false);
                if (GameObjectIns.Instance.CDGIsDown == false)
                {
                    print("都没有充电");
                    GameObjectIns.Instance.SetC2ChildFalse();
                    GameObjectIns.Instance.OneRecharge_Btn.gameObject.SetActive(true);
                    GameObjectIns.Instance.OneRecharge_Btn.transform.GetChild(1).transform.gameObject.SetActive(true);
                    GameObjectIns.Instance.OneRecharge_Btn.transform.GetChild(2).transform.gameObject.SetActive(false);
                    GameObjectIns.Instance.TwoRecharge_Btn.gameObject.SetActive(true);
                    GameObjectIns.Instance.TwoRecharge_Btn.transform.GetChild(1).transform.gameObject.SetActive(true);
                    GameObjectIns.Instance.TwoRecharge_Btn.transform.GetChild(2).transform.gameObject.SetActive(false);

                    GameObjectIns.Instance.ThreeRecharge_Btn.gameObject.SetActive(true);
                    GameObjectIns.Instance.ThreeRecharge_Btn.transform.GetChild(1).transform.gameObject.SetActive(true);
                    GameObjectIns.Instance.ThreeRecharge_Btn.transform.GetChild(2).transform.gameObject.SetActive(false);
                    GameObjectIns.Instance.FourRecharge_Btn.gameObject.SetActive(true);
                    GameObjectIns.Instance.FourRecharge_Btn.transform.GetChild(1).transform.gameObject.SetActive(true);
                    GameObjectIns.Instance.FourRecharge_Btn.transform.GetChild(2).transform.gameObject.SetActive(false);
                }
                else
                {
                    GameObjectIns.Instance.IsRecharge30 = true;
                    print("2正在充电");
                    //PlayerAudio.Instance.PlayAudioClipTest(11);
                    GameObjectIns.Instance.SetC2ChildFalse();

                    GameObjectIns.Instance.OneRecharge_Btn.gameObject.SetActive(true);
                    GameObjectIns.Instance.OneRecharge_Btn.transform.GetChild(1).transform.gameObject.SetActive(true);
                    GameObjectIns.Instance.OneRecharge_Btn.transform.GetChild(2).transform.gameObject.SetActive(false);
                    GameObjectIns.Instance.TwoRecharge_Btn.gameObject.SetActive(true);
                    GameObjectIns.Instance.TwoRecharge_Btn.transform.GetChild(1).transform.gameObject.SetActive(false);
                    GameObjectIns.Instance.TwoRecharge_Btn.transform.GetChild(2).transform.gameObject.SetActive(true);

                    GameObjectIns.Instance.ThreeRecharge_Btn.gameObject.SetActive(true);
                    GameObjectIns.Instance.ThreeRecharge_Btn.transform.GetChild(1).transform.gameObject.SetActive(true);
                    GameObjectIns.Instance.ThreeRecharge_Btn.transform.GetChild(2).transform.gameObject.SetActive(false);
                    GameObjectIns.Instance.FourRecharge_Btn.gameObject.SetActive(true);
                    GameObjectIns.Instance.FourRecharge_Btn.transform.GetChild(1).transform.gameObject.SetActive(true);
                    GameObjectIns.Instance.FourRecharge_Btn.transform.GetChild(2).transform.gameObject.SetActive(false);
                }
            }
            else if (other.name == "IsMoveVehicleNo_Btn")
            {
                GameObjectIns.Instance.IsRecharge = true;
                if (GameObjectIns.Instance.IsRecharge30)
                {
                    PlayerAudio.Instance.PlayBtnAudio();
                    GameObjectIns.Instance.VibateController(1, 100, 1);
                    GameObjectIns.Instance.SetC2ChildFalse();
                    GameObjectIns.Instance.Homepage_Btn.gameObject.SetActive(true);
                    GameObjectIns.Instance.RechargeState_Image.gameObject.SetActive(true);
                    GameObjectIns.Instance.OverRecharge_Btn.gameObject.SetActive(true);
                    GameObjectIns.Instance.BG_Image0.gameObject.SetActive(false);
                    GameObjectIns.Instance.BG_Image1.gameObject.SetActive(true);
                }
                else
                {
                    PlayerAudio.Instance.PlayBtnAudio();
                    GameObjectIns.Instance.VibateController(1, 100, 1);
                    GameObjectIns.Instance.SetC2ChildFalse();
                    GameObjectIns.Instance.RechargeYes_Image.SetActive(true);
                    GameObjectIns.Instance.Homepage_Btn.gameObject.SetActive(true);
                }

            }
            else if (other.name == "IsMoveVehicleYes_Btn")
            {
                PlayerAudio.Instance.PlayBtnAudio();
                GameObjectIns.Instance.VibateController(1, 100, 1);
                print("充电过程中不能移车 充电弓变形冒火花 测验失败");
                GameObjectIns.Instance.IsMoveVehicle_Img.SetActive(false);
                GameObjectIns.Instance.Urgency_Button.gameObject.SetActive(false);
                CancelInvoke();

                print("50充电弓挂弓");
                PlayerAudio.Instance.PlayCDGGG();
                print("47滋滋的电流声音效_爱给网_aigei_com");
                PlayerAudio.Instance.PlayCDGDL();

                CDQBX_HuoHua();
            }
        }
        else
        {
            print("正在播放声音 稍后操作");
        }
    }

    private void SetOneBtn()
    {
        GameObjectIns.Instance.OneRecharge_Btn.transform.GetComponent<BoxCollider>().enabled = true;
    }

    private void CDQBX_HuoHua()
    {
        
        CancelInvoke();
        GameObjectIns.Instance.IsRecharge = false;

        GameObjectIns.Instance.IsTestFalse = true;

        GameObjectIns.Instance.CDGAniBreak();

        GameObjectIns.Instance.LavaSparks03_coll.Play();

        GameObjectIns.Instance.DOTweenMove(GameObjectIns.Instance.CityBuswithInterior, PlayerPosition.CityBuswithInteriorCDGYiChePos, 2f);
        GameObjectIns.Instance.DOTweenMove(GameObjectIns.Instance.Player,PlayerPosition.PlayerCDGYiChePos, 2f);

        Invoke("TestOver", 10f);
    }

    private void SetTwoCDGArrow()
    {
        GameObjectIns.Instance.CDG_arrow.SetActive(false);
        print("19核对编号完成后点击屏幕上对应的编号");
        //PlayerAudio.Instance.PlayAudioClipTest(19);
        GameObjectIns.Instance.TwoRecharge_Btn.transform.GetChild(3).gameObject.SetActive(true);
    }

    private void StartRecharge_Btn_Event()
    {
        GameObjectIns.Instance.SetC2ChildFalse();
        //PlayerAudio.Instance.PlayAudioClip(5);
        //PlayerAudio.Instance.PlayAudioClipTest(10);
        GameObjectIns.Instance.RechargeYes_Image.SetActive(true);
        GameObjectIns.Instance.Recharge30Time = 30f;
        GameObjectIns.Instance.IsRecharge = true;


        Invoke("JJDD", 2f);
        //GameObjectIns.Instance.Homepage_Btn.gameObject.SetActive(true);
    }

    //紧急调度信息
    private void JJDD()
    {
        GameObjectIns.Instance.IsRecharge = false;

        GameObjectIns.Instance.SetC2ChildFalse();
        GameObjectIns.Instance.IsMoveVehicle_Img.SetActive(true);
        GameObjectIns.Instance.Urgency_Button.gameObject.SetActive(true);
        GameObjectIns.Instance.RechargeYes_Image.SetActive(true);
    }

    private void TwoRechargeEvent()
    {
        print("配音：点击“充电弓降落”按钮，并通过监控或下车确认充电弓是否已降落");
        //PlayerAudio.Instance.PlayAudioClipTest(6);


        //PlayerAudio.Instance.PlayAudioClip(11);
        GameObjectIns.Instance.CDGIsDown = true;
        GameObjectIns.Instance.SetC2ChildFalse();
        GameObjectIns.Instance.RechargeDown_Btn.transform.gameObject.SetActive(true);
        GameObjectIns.Instance.RechargeUp_Btn.transform.gameObject.SetActive(true);
    }

    private void CDGAniDownOver()
    {
        print("8请点击屏幕“启动充电”按钮");
        //PlayerAudio.Instance.PlayAudioClipTest(8);

        GameObjectIns.Instance.CDGJL_arrow.SetActive(false);

        GameObjectIns.Instance.BG_Image0.SetActive(false);
        GameObjectIns.Instance.BG_Image1.SetActive(true);

        GameObjectIns.Instance.SetC2ChildFalse();
        GameObjectIns.Instance.RechargeDownYes_Image.SetActive(true);
        GameObjectIns.Instance.RechargeUp_Btn1.gameObject.SetActive(true);
        GameObjectIns.Instance.StartRecharge_Btn.transform.gameObject.SetActive(true);
        //Invoke("CDGAniUpOver()", 10f);
    }

    private void CDGAniUpOver()
    {
        print("充电弓上升结束");
        GameObjectIns.Instance.SetC2ChildFalse();
        //GameObjectIns.Instance.RechargeUPOver();
        GameObjectIns.Instance.CDGIsDown = false;
        GameObjectIns.Instance.RechargeOver_Image.gameObject.SetActive(true);
        GameObjectIns.Instance.RechargeOver_Image.transform.GetChild(0).gameObject.SetActive(false);
        GameObjectIns.Instance.RechargeOver_Image.transform.GetChild(1).gameObject.SetActive(true);
        GameObjectIns.Instance.CDGJL_arrow.SetActive(false);
        //GameObjectIns.Instance.RechargeUPOver();
        Invoke("SetAKeyToStartFail_Btn", 5f);
    }

    private void SetAKeyToStartFail_Btn()
    {
        //print("4通过地面标识或者充电弓体标识核对充电指定区域的充电弓编号");
        //PlayerAudio.Instance.PlayAudioClipTest(5);
        //PlayerAudio.Instance.PlayAudioClip(21);
        GameObjectIns.Instance.CDG_arrow.SetActive(true);

        //GameObjectIns.Instance.SetHight(GameObjectIns.Instance.CDG_arrow, true);

        GameObjectIns.Instance.SetC2ChildFalse();
        GameObjectIns.Instance.OneRecharge_Btn.gameObject.SetActive(true);

        GameObjectIns.Instance.TwoRecharge_Btn.gameObject.SetActive(true);
        GameObjectIns.Instance.TwoRecharge_Btn.transform.GetChild(1).transform.gameObject.SetActive(true);
        GameObjectIns.Instance.TwoRecharge_Btn.transform.GetChild(2).transform.gameObject.SetActive(false);

        GameObjectIns.Instance.TwoRecharge_Btn.transform.Find("Two_CDGJL_arrow").gameObject.SetActive(false);
        Invoke("SetTwoCDGArrow", 8f);

        GameObjectIns.Instance.ThreeRecharge_Btn.gameObject.SetActive(true);
        GameObjectIns.Instance.FourRecharge_Btn.gameObject.SetActive(true);
    }
  
    private void SetOverRecharge_Btn()
    {
        //GameObjectIns.Instance.AKeyToStart_Button.gameObject.SetActive(true);

        GameObjectIns.Instance.SetC2ChildFalse();
        GameObjectIns.Instance.RechargeOverHint_Image.gameObject.SetActive(true);
        if (GameObjectIns.Instance.RechargeTime > 100f)
        {
            GameObjectIns.Instance.RechargeTime = 100f;
        }
        GameObjectIns.Instance.RechargeOverHint_Image.transform.GetChild(0).GetComponent<Text>().text = ((int)GameObjectIns.Instance.RechargeTime).ToString() + "%";
        Invoke("GameOverHint1", 10f);
    }

    private void GameOverHint1()
    {
        GameObjectIns.Instance.CDGGameOverHint();
    }

    private void TestOver()
    {
        PlayerAudio.Instance.StopDL();
        CancelInvoke();
        GameObjectIns.Instance.IsTestFalse = true;
        print("40操作错误！请重新培训后再进行考试");
        PlayerAudio.Instance.PlayAudioClipTest(40);
        GameObjectIns.Instance.PlayerHint_Canvas.SetActive(true);
        GameObjectIns.Instance.PlayerHint_Text.text = "操作错误！请重新培训后再进行考试";
        Invoke("TestOverZC", 5f);
    }
    /// <summary>
    /// 测验失败转场动画
    /// </summary>
    private void TestOverZC()
    {
        GameObjectIns.Instance.SetCutTo(0f, 1f, 2f);
        Invoke("TestOverToTraining", 2f);
    }

    private void TestOverToTraining()
    {
        PlayerAudio.Instance.StopAudio();
        GameObjectIns.Instance.LavaSparks03_coll.Stop();
        GameObjectIns.Instance.CDGAniUP();

        GameObjectIns.Instance.SetCDGValue();
        GameObjectIns.Instance.PlayerHint_Canvas.SetActive(false);
        GameObjectIns.Instance.vr_cartoon_hand_prefab_Left.AddComponent<LeftShouShaTrigger>();
        GameObjectIns.Instance.vr_cartoon_hand_prefab_Right.AddComponent<RightYaoShiTrigger>();

        GameObjectIns.Instance.SetRawImage(false);
        Destroy(GameObjectIns.Instance.vr_cartoon_hand_prefab_Left.transform.GetComponent<LeftShouCdgTrigger>());
        Destroy(GameObjectIns.Instance.vr_cartoon_hand_prefab_Right.transform.GetComponent<RightShouCdgTrigger>());
    }
}

