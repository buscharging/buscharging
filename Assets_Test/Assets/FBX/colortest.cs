﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class colortest : MonoBehaviour
{
    public Material LeftYellowLight;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            LeftYellowLight.EnableKeyword("_EMISSION");
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            LeftYellowLight.DisableKeyword("_EMISSION");
        }
    }
}
