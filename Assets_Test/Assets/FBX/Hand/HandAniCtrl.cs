﻿using UnityEngine;

public class HandAniCtrl : MonoBehaviour
{
    public Animator A;
    public GameObject A_L;
    public GameObject A_R;
    public GameObject VR_L_Hand;
    public GameObject VR_R_Hand;

    public GameObject L_HandPos;
    public GameObject R_HandPos;
    public GameObject QiangPos;

    public GameObject XuanBi_GunButtoono;

    [SerializeField]
    private HandAniCtrl handAniCtrl;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        AnimatorStateInfo info = GameObjectIns.Instance.B_HandVR.transform.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0);
        AnimatorStateInfo info1 = GameObjectIns.Instance.A_HandVR.transform.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0);
        if (info.normalizedTime >=1.0f && info1.normalizedTime >= 1.0f)
        {
            GameObjectIns.Instance.IsPlayAni = true;
        }
        else
        {
            GameObjectIns.Instance.IsPlayAni = false;
        }


        //Distance_Hand();
    }

    /// <summary>
    /// 显示手模型和动画
    /// </summary>
    private void Distance_Hand()
    {
        if (Vector3.Distance(R_HandPos.transform.position, QiangPos.transform.position) < GameObjectIns.Instance.Test_JvLi1 || Vector3.Distance(L_HandPos.transform.position, QiangPos.transform.position) < GameObjectIns.Instance.Test_JvLi1)
        {
            A.SetBool("IsHold", true);
            A.SetBool("IsPine", false);
            A_L.SetActive(true);
            A_R.SetActive(true);
            VR_L_Hand.SetActive(false);
            VR_R_Hand.SetActive(false);
            XuanBi_GunButtoono.transform.localEulerAngles = new Vector3(90f, 0f, 0f);
            handAniCtrl.enabled = false;
        }
        else
        {
            A.SetBool("IsHold", false);
            A.SetBool("IsPine", true);
            A_L.SetActive(false);
            A_R.SetActive(false);
            VR_L_Hand.SetActive(true);
            VR_R_Hand.SetActive(true);
            XuanBi_GunButtoono.transform.localEulerAngles = new Vector3(89f, 0f, 0f);

            handAniCtrl.enabled = true;
        }
    }
}