﻿using HighlightPlus;
using UnityEngine;

public class MyTest : MonoBehaviour
{
    private HighlightEffect HighlightEffect;

    // Start is called before the first frame update
    void Start()
    {
        HighlightEffect = transform.GetComponent<HighlightEffect>();
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.A))
        {
            HighlightEffect.highlighted = true;

        }
        if (Input.GetKeyUp(KeyCode.A))
        {
            HighlightEffect.highlighted = false;
        }

    }
}
