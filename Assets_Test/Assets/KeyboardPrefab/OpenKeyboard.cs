﻿/// <summary>
/// 作者：张振飞
/// 开启键盘
/// 时间：2019.10.16
/// </summary>
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/// <summary>
/// 开启键盘
/// </summary>
public class OpenKeyboard : MonoBehaviour
{
    private Button btn_OnTheKeyboard;
    // Start is called before the first frame update
    void Start()
    {
        btn_OnTheKeyboard = transform.Find("Img_ActivateThePanel/Btn_OnTheKeyboard").GetComponent<Button>();
        btn_OnTheKeyboard.onClick.AddListener(OpenKeyboard_Btn);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OpenKeyboard_Btn()
    {
        transform.Find("Img_Keyboard").gameObject.SetActive(true);
    }
}
